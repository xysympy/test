package net.mqtt;

import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.util.DigestUtils;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author Administrator
 * @Date 2021/9/16 14:45
 */
public class TestMqtt {
    public static void main(String[] args) throws InterruptedException {
        String pubTopic = "v1/device/capture/xmeye";
        String subTopic = "v1/device/capture/xmeye/result";
        int qos = 2;
        String broker = "ssl://116.63.193.154:8000";
        String clientId = "xmeye";
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            MqttClient client = new MqttClient(broker, clientId, persistence);

            // MQTT 连接选项
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setUserName("xmeye-username");
            String token = getToken();
            JSONObject jsonObject = JSONUtil.parseObj(token);
            String password = jsonObject.getStr("token");
            System.out.println("password : " + password);
            connOpts.setPassword(password.toCharArray());
            // 保留会话
            connOpts.setCleanSession(true);
            String content = "{\n" +
                    "    \"sn\":\"a8e81d1365f5f714\",\n" +
                    "    \"content\":{\n" +
                    "        \"Name\":\"OPSNAP\",\n" +
                    "        \"OPSNAP\":{\n" +
                    "            \"Channel\":0\n" +
                    "        }\n" +
                    "    }\n" +
                    "}";
            // 设置回调
            client.setCallback(new OnMessageCallback());

            // 建立连接
            System.out.println("Connecting to broker: " + broker);
            client.connect(connOpts);

            System.out.println("Connected");
            System.out.println("Publishing message: " + content);

            // 订阅
            client.subscribe(subTopic, 2);

            // 消息发布所需参数
            MqttMessage message = new MqttMessage();
            message.setPayload(content.getBytes());
            message.setQos(qos);
            client.publish(pubTopic, message);
            Thread.sleep(5000);
            System.out.println("Message published");

            client.disconnect();
            System.out.println("Disconnected");
            client.close();
            System.exit(0);
        } catch (MqttException me) {
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
    }
    private static String getToken(){
        String url = "https://pub-token.xmeye.net/reqToken";
        String SN     = "a8e81d1365f5f714";
        String SEVICE = "MQTT";
        String USER   = "admin";
        String PASS   = "";
        String UUID   = "xmeye";
        String APPkEY = "e97833dc8b1f47339aaf728f4607971a";
        String SECkEY = "a1d45b7889204367affbfd719d1b147e";
        Integer MC     = 2;
        String tm = getTimMillis();
        System.out.println("tm : " + tm);
        String sign = getEncryptStr(UUID, APPkEY, SECkEY, tm, MC);
        System.out.println("sign : " + sign);
        JSONObject jsonObject = new JSONObject();
        jsonObject.putOnce("sn", SN);
        jsonObject.putOnce("service", SEVICE);
        jsonObject.putOnce("uuid", UUID);
        jsonObject.putOnce("appkey", APPkEY);
        jsonObject.putOnce("tm", tm);
        jsonObject.putOnce("sign", sign);
        jsonObject.putOnce("user", USER);
        jsonObject.putOnce("pass", PASS);
        System.out.println(jsonObject);
        String password = HttpRequest.post(url).header(Header.CONTENT_TYPE, "application/json")
                .body(jsonObject.toString()).execute().body();
        return password;
    }

    private static String getEncryptStr(String uuid, String appKey, String appSecret, String timeMillis, int movedCard) {
        String encryptStr = uuid + appKey + appSecret + timeMillis;
        byte[] encryptByte = encryptStr.getBytes(StandardCharsets.ISO_8859_1);
        byte[] changeByte = change(encryptStr, movedCard);
        byte[] mergeByte = mergeByte(encryptByte, changeByte);
        return DigestUtils.md5DigestAsHex(mergeByte);
    }
    /**
     * 简单移位
     */
    private static byte[] change(String encryptStr, int moveCard) {
        byte[] encryptByte = encryptStr.getBytes(StandardCharsets.ISO_8859_1);
        int encryptLength = encryptByte.length;
        byte temp;
        for (int i = 0; i < encryptLength; i++) {
            temp = ((i % moveCard) > ((encryptLength - i) % moveCard)) ? encryptByte[i] : encryptByte[encryptLength - (i + 1)];
            encryptByte[i] = encryptByte[encryptLength - (i + 1)];
            encryptByte[encryptLength - (i + 1)] = temp;
        }
        return encryptByte;
    }

    /**
     * 合并
     *
     * @param encryptByte
     * @param changeByte
     * @return
     */
    private static byte[] mergeByte(byte[] encryptByte, byte[] changeByte) {
        int encryptLength = encryptByte.length;
        int encryptLength2 = encryptLength * 2;
        byte[] temp = new byte[encryptLength2];
        for (int i = 0; i < encryptByte.length; i++) {
            temp[i] = encryptByte[i];
            temp[encryptLength2 - 1 - i] = changeByte[i];
        }
        return temp;
    }

    private static long timMillis;//时间戳
    private static long counter = 0L;//计数器
    /**
     * 获取计数器
     *
     * @return
     */
    private static synchronized String getCounter(){
        ++counter;
        if (counter < 10L)
            return "000000" + String.valueOf(counter);
        else if (counter < 100L)
            return "00000" + String.valueOf(counter);
        else if (counter < 1000L)
            return "0000" + String.valueOf(counter);
        else if (counter < 10000L)
            return "000" + String.valueOf(counter);
        else if (counter < 100000L)
            return "00" + String.valueOf(counter);
        else if (counter < 1000000L)
            return "0" + String.valueOf(counter);
        else if (counter < 10000000L)
            return String.valueOf(counter);
        else {
            counter = 1L;
            return "000000" + String.valueOf(counter);
        }
    }
    /**
     * 获取组合时间戳
     *
     * @return
     */
    public static String getTimMillis() {
        timMillis = System.currentTimeMillis();
        return getCounter() + String.valueOf(timMillis);
    }
}
