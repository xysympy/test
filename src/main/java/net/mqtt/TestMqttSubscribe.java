package net.mqtt;

import cn.hutool.core.text.PasswdStrength;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.DigestUtils;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author Administrator
 * @Date 2021/9/16 10:33
 */
public class TestMqttSubscribe {
    public static void main(String[] args) {
        String subTopic = "v1/status/query/xmeye/result";
        String broker = "ssl://mqtt_test.bcloud365.net:8000";
        String clientId = "xmeyesubscribe";
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            MqttClient client = new MqttClient(broker, clientId, persistence);

            // MQTT 连接选项
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setUserName("username-developer-subscribe");
            String token = getToken();
            JSONObject jsonObject = JSONUtil.parseObj(token);
            String password = jsonObject.getStr("token");
            System.out.println("password : " + password);
            connOpts.setPassword(password.toCharArray());
            // 保留会话
            connOpts.setCleanSession(true);

            // 设置回调
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    // 连接丢失后，一般在这里面进行重连
                    System.out.println("连接断开，可以做重连");
                }

                @Override
                public void messageArrived(String topic, MqttMessage message){
                    // subscribe后得到的消息会执行到这里面
                    System.out.println("接收消息主题:" + topic);
                    System.out.println("接收消息Qos:" + message.getQos());
                    System.out.println("接收消息内容:" + new String(message.getPayload()));
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    System.out.println("deliveryComplete---------" + token.isComplete());
                }
            });

            // 建立连接
            System.out.println("Connecting to broker: " + broker);
            client.connect(connOpts);

            // 订阅
            client.subscribe(subTopic);
        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
    }

    private static String getToken(){
        String url = "https://pub-token.xmeye.net/reqToken";
        String SN     = "df88035b912db7c1";
        String SEVICE = "MQTT";
        String USER   = "admin";
        String PASS   = "";
        String UUID   = "xmeye";
        String APPkEY = "e97833dc8b1f47339aaf728f4607971a";
        String SECkEY = "a1d45b7889204367affbfd719d1b147e";
        Integer MC     = 2;
        String tm = System.currentTimeMillis() + "";
        System.out.println("tm : " + tm);
        String sign = getEncryptStr(UUID, APPkEY, SECkEY, tm, MC);
        System.out.println("sign : " + sign);
        JSONObject jsonObject = new JSONObject();
        jsonObject.putOnce("sn", SN);
        jsonObject.putOnce("service", SEVICE);
        jsonObject.putOnce("uuid", UUID);
        jsonObject.putOnce("appkey", APPkEY);
        jsonObject.putOnce("tm", tm);
        jsonObject.putOnce("sign", sign);
        jsonObject.putOnce("user", USER);
        jsonObject.putOnce("pass", PASS);
        System.out.println(jsonObject);
        String password = HttpRequest.post(url).header(Header.CONTENT_TYPE, "application/json")
                .body(jsonObject.toString()).execute().body();
        return password;
    }

    private static String getEncryptStr(String uuid, String appKey, String appSecret, String timeMillis, int movedCard) {
        String encryptStr = uuid + appKey + appSecret + timeMillis;
        byte[] encryptByte = encryptStr.getBytes(StandardCharsets.ISO_8859_1);
        byte[] changeByte = change(encryptStr, movedCard);
        byte[] mergeByte = mergeByte(encryptByte, changeByte);
        return DigestUtils.md5DigestAsHex(mergeByte);
    }
    /**
     * 简单移位
     */
    private static byte[] change(String encryptStr, int moveCard) {
        byte[] encryptByte = encryptStr.getBytes(StandardCharsets.ISO_8859_1);
        int encryptLength = encryptByte.length;
        byte temp;
        for (int i = 0; i < encryptLength; i++) {
            temp = ((i % moveCard) > ((encryptLength - i) % moveCard)) ? encryptByte[i] : encryptByte[encryptLength - (i + 1)];
            encryptByte[i] = encryptByte[encryptLength - (i + 1)];
            encryptByte[encryptLength - (i + 1)] = temp;
        }
        return encryptByte;
    }

    /**
     * 合并
     *
     * @param encryptByte
     * @param changeByte
     * @return
     */
    private static byte[] mergeByte(byte[] encryptByte, byte[] changeByte) {
        int encryptLength = encryptByte.length;
        int encryptLength2 = encryptLength * 2;
        byte[] temp = new byte[encryptLength2];
        for (int i = 0; i < encryptByte.length; i++) {
            temp[i] = encryptByte[i];
            temp[encryptLength2 - 1 - i] = changeByte[i];
        }
        return temp;
    }
}
