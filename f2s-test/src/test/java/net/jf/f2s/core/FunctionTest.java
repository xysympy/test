package net.jf.f2s.core;

import net.jf.f2s.core.api.quartzcustom.entity.QrtzCustomScheduleJob;
import net.jf.f2s.core.api.quartzcustom.function.TryCatchFunction;
import net.jf.f2s.core.api.quartzcustom.function.TryCatchUtil;
import net.jf.f2s.core.api.quartzcustom.service.QrtzCustomScheduleJobService;
import net.project.init.starter.code.DefaultBusinessCode;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2022/4/12 11:29
 */
@SpringBootTest
public class FunctionTest {

    @Resource
    private QrtzCustomScheduleJobService qrtzCustomScheduleJobService;

    @Test
    public void functionTest() {
        TryCatchUtil tryCatchUtil = new TryCatchUtil();
        tryCatchUtil.tryCatch( o -> qrtzCustomScheduleJobService.save((QrtzCustomScheduleJob) o), new Object(), DefaultBusinessCode.FAILED);

    }
}
