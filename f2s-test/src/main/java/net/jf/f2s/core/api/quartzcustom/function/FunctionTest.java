package net.jf.f2s.core.api.quartzcustom.function;

/**
 * @author yxm
 * @description: 测试Function函数式接口的使用
 * @date 2022/4/13 10:22
 */
public class FunctionTest {

    private boolean functionTest(FunctionMethodTest test, Object object) {
        return test.functionTest(object).equals(11);
    }

    public static void main(String[] args) {
        FunctionTest functionTest = new FunctionTest();
        FunctionMethodTest functionMethodTest = new FunctionMethodTestImpl();
        Integer integer = functionMethodTest.functionTest(11);
        // 其实主要就是为了测试在<T> T test(T t)能不能直接使用lambda，在不进行强制转换类型的基础上
        // 显然的，是不行的，还是直接参考TryCatchFunction和TryCatchUtil的使用
    }

    private static boolean compare(Integer temp) {
        return temp.equals(11);
    }

    private static Integer make(Integer temp) {
        return 11;
    }
}

@FunctionalInterface
interface FunctionMethodTest {

    /**
     * 功能测试
     *
     * @param t t
     * @return boolean
     */
    <T> T functionTest(T t);
}

class FunctionMethodTestImpl implements FunctionMethodTest {

    @Override
    public <T> T functionTest(T t) {
        return t;
    }
}