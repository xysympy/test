package net.jf.f2s.core.api.quartzcustom.function;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import net.jf.f2s.core.api.quartzcustom.entity.QrtzCustomScheduleJob;
import net.jf.f2s.core.api.quartzcustom.service.QrtzCustomScheduleJobService;
import net.jf.f2s.core.api.quartzcustom.service.impl.QrtzCustomScheduleJobServiceImpl;
import net.project.init.starter.code.BusinessCode;
import net.project.init.starter.code.DefaultBusinessCode;
import net.project.init.starter.exception.BusinessException;
import org.bouncycastle.util.io.Streams;
import quartz.net.project.init.starter.service.QuartzService;

import java.util.stream.Stream;

/**
 * @author yxm
 * @description: 具体的实现
 * @date 2022/4/12 10:38
 */
public class TryCatchUtil {
    public <T>void tryCatch(TryCatchFunction<T> function, T t, BusinessCode code) {
        try {
            if (!function.tryCatch(t)) {
                throw new BusinessException(code);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        /*new TryCatchUtil().tryCatch(o -> {
            QrtzCustomScheduleJobService qrtzCustomScheduleJobService = new QrtzCustomScheduleJobServiceImpl();
            return qrtzCustomScheduleJobService.save(o);
        }, 1, DefaultBusinessCode.BUSINESS_VERIFICATION_ERROR);*/
        QrtzCustomScheduleJobService qrtzCustomScheduleJobService = new QrtzCustomScheduleJobServiceImpl();
        LambdaQueryWrapper<QrtzCustomScheduleJob> queryWrapper = new LambdaQueryWrapper<>();
        Stream.of(1, 2, 3).forEach(System.out::print);
    }
}
