package net.jf.f2s.core.api.time.service;

import org.springframework.stereotype.Component;

/**
 * @author yxm
 * @description: 定时服务接口
 * @date 2022/4/7 16:58
 */
@Component
public class ExecuteService{

    public ExecuteService() {
        System.out.println("ExecuteService");
    }
    public void run() {
        System.out.println("ExecuteService");
    }
}
