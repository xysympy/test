package net.jf.f2s.core.api.quartzcustom.service;

import com.baomidou.mybatisplus.extension.service.IService;
import net.jf.f2s.core.api.quartzcustom.entity.QrtzCustomScheduleJob;

/**
 * 自定义定时任务调度(QrtzCustomScheduleJob)表服务接口
 *
 * @author yxm
 * @since 2022-04-09 09:15:33
 */
public interface QrtzCustomScheduleJobService extends IService<QrtzCustomScheduleJob> {

}

