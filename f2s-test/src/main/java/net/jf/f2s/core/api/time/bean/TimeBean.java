package net.jf.f2s.core.api.time.bean;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import net.jf.f2s.core.api.quartzcustom.entity.QrtzCustomScheduleJob;
import net.jf.f2s.core.api.quartzcustom.service.QrtzCustomScheduleJobService;
import net.jf.f2s.core.api.time.service.ExecuteService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author yxm
 * @description: 任务类
 * @date 2022/4/7 17:18
 */
public class TimeBean extends QuartzJobBean {

    private final ExecuteService executeService;
    private final QrtzCustomScheduleJobService qrtzCustomScheduleJobService;

    public TimeBean(ExecuteService executeService, QrtzCustomScheduleJobService qrtzCustomScheduleJobService) {
        this.executeService = executeService;
        this.qrtzCustomScheduleJobService = qrtzCustomScheduleJobService;
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        executeService.run();
        LambdaQueryWrapper<QrtzCustomScheduleJob> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(QrtzCustomScheduleJob::getIsNowRun, 1)
                .eq(QrtzCustomScheduleJob::getDeleteState, 0);
        QrtzCustomScheduleJob one = qrtzCustomScheduleJobService.getOne(queryWrapper);
        System.out.println(one);
    }
}
