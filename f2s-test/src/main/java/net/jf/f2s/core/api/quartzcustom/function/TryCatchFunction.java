package net.jf.f2s.core.api.quartzcustom.function;

/**
 * @author yxm
 * @description: 统一使用try-catch
 * @date 2022/4/12 10:33
 */
@FunctionalInterface
public interface TryCatchFunction<T> {

    /**
     * 统一try-catch方法
     * {@link FunctionTest}
     * @param t t
     * @return boolean
     */
    public boolean tryCatch(T t);
}
