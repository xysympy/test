package net.jf.f2s.core;

import net.jf.f2s.core.api.time.bean.TimeBean;
import net.jf.f2s.core.api.time.service.ExecuteService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.ResolvableType;

import java.lang.reflect.Constructor;
import java.util.Objects;

/**
 * f2核心应用程序
 *
 * @author mrava
 * @date 2022/04/06
 */
@SpringBootApplication
public class F2sCoreApplication {

    public static void main(String[] args) throws ClassNotFoundException {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(F2sCoreApplication.class, args);
        // Constructor<?> declaredConstructor = Class.forName("net.jf.f2s.core.api.time.bean.TimeBean").getDeclaredConstructors()[0];
        Constructor<?> declaredConstructor = ClassLoader.getSystemClassLoader().loadClass("net.jf.f2s.core.api.time.bean.TimeBean").getDeclaredConstructors()[0];
        Class<?>[] parameterTypes = declaredConstructor.getParameterTypes();
        ExecuteService executeService = applicationContext.getBean("executeService", ExecuteService.class);
        Class<? extends ExecuteService> aClass = executeService.getClass();
        ResolvableType resolvableType = ResolvableType.forRawClass(parameterTypes[0]);
        System.out.println(Objects.requireNonNull(resolvableType.getRawClass()).isAssignableFrom(aClass));
    }

}
