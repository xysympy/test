package net.jf.f2s.core.api.quartzcustom.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.jf.f2s.core.api.quartzcustom.dao.QrtzCustomScheduleJobDao;
import net.jf.f2s.core.api.quartzcustom.entity.QrtzCustomScheduleJob;
import net.jf.f2s.core.api.quartzcustom.service.QrtzCustomScheduleJobService;
import org.springframework.stereotype.Service;

/**
 * 自定义定时任务调度(QrtzCustomScheduleJob)表服务接口实现类
 *
 * @author yxm
 * @since 2022-04-09 09:15:33
 */
@Service("qrtzCustomScheduleJobService")
public class QrtzCustomScheduleJobServiceImpl extends ServiceImpl<QrtzCustomScheduleJobDao, QrtzCustomScheduleJob> implements QrtzCustomScheduleJobService {

}

