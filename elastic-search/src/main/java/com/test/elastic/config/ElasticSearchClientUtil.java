package com.test.elastic.config;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.nodes.Http;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.ssl.SSLContexts;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author yxm
 * @description: 使用客户端而非SpringBoot整合的
 * @date 2022/3/1 13:07
 */
@Component
public class ElasticSearchClientUtil {
    /**
     * 客户端
     */
    private final static ElasticsearchClient CLIENT;
    private final static String username = "elastic";
    private final static String password = "PLV2NlOWMmR5dcDfUuZK";
    private final static String crtFileAddress = "D:\\Program Files (x86)\\elasticsearch-8.0.0\\config\\certs\\http_ca.crt";
    private final static String keyStoreType = "PKCS12";
    private final static List<String> uris = Arrays.asList("https://localhost:9200", "https://localhost:9201", "https://localhost:9202");

    static {
        SSLContext sslContext = null;
        try {
            CertificateFactory factory = CertificateFactory.getInstance("X.509");
            Certificate certificate = factory.generateCertificate(Files.newInputStream(Paths.get(crtFileAddress)));
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", certificate);
            sslContext = SSLContexts.custom()
                    .loadTrustMaterial(keyStore, null).build();
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | KeyManagementException e) {
            e.printStackTrace();
        }
        List<HttpHost> list = new ArrayList<>();
        for (String s : uris) {
            list.add(HttpHost.create(s));
        }
        HttpHost[] httpHosts = new HttpHost[list.size()];
        list.toArray(httpHosts);
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));
        SSLContext finalSslContext = sslContext;
        RestClient restClient = RestClient.builder(httpHosts)
                .setHttpClientConfigCallback(httpClientBuilder -> {
                    httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                    httpClientBuilder.setSSLContext(finalSslContext);
                    return httpClientBuilder;
                }).build();
        ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
        CLIENT = new ElasticsearchClient(transport);
    }

    public static ElasticsearchClient getClient() {
        return CLIENT;
    }
}
