package com.test.elastic.config;


import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.json.jsonb.JsonbJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.ssl.SSLContexts;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yxm
 * @description: 配置ElasticSearch连接
 * @date 2022/2/22 19:03
 */
@Configuration
public class ElasticSearchConfig {

    private final Logger log = LoggerFactory.getLogger(ElasticSearchConfig.class);
    @Resource
    private ElasticSearchProperties esProperties;

    @Bean
    public ElasticsearchClient getElasticSearchClient() {
        RestClient restClient = getRestClient();
        ElasticsearchTransport transport = new RestClientTransport(restClient, new JacksonJsonpMapper());
        return new ElasticsearchClient(transport);
    }

    private RestClient getRestClient() {
        List<HttpHost> list = new ArrayList<>();
        for (String uri : esProperties.getUris()) {
            list.add(HttpHost.create(uri));
        }
        HttpHost[] httpHosts = new HttpHost[list.size()];
        list.toArray(httpHosts);
        RestClientBuilder restClientBuilder = RestClient.builder(httpHosts);
        if (Boolean.TRUE.equals(esProperties.getUsessl())) {
            SSLContext sslContext = null;
            try {
                CertificateFactory factory = CertificateFactory.getInstance("X.509");
                Certificate certificate = factory.generateCertificate(Files.newInputStream(Paths.get(esProperties.getCrtFileAddress())));
                KeyStore keyStore = KeyStore.getInstance("PKCS12");
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", certificate);
                sslContext = SSLContexts.custom()
                        .loadTrustMaterial(keyStore, null).build();
            } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | KeyManagementException e) {
                log.error(e.getMessage());
                // 抛出异常
            }
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(esProperties.getUsername(), esProperties.getPassword()));
            SSLContext finalSslContext = sslContext;
            restClientBuilder.setHttpClientConfigCallback(httpClientBuilder -> {
                httpClientBuilder.setSSLContext(finalSslContext);
                httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                return httpClientBuilder;
            });
        }
        return restClientBuilder.build();
    }

}
