package com.test.elastic.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author yxm
 * @description: es参数设置
 * @date 2022/3/4 14:58
 */
@ConfigurationProperties(prefix = "elasticsearch")
@Configuration
public class ElasticSearchProperties {
    private List<String> uris;
    private Boolean usessl;
    private String username;
    private String password;
    private String crtFileAddress;

    public void setUris(List<String> uris) {
        this.uris = uris;
    }

    public void setUsessl(Boolean usessl) {
        this.usessl = usessl;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCrtFileAddress(String crtFileAddress) {
        this.crtFileAddress = crtFileAddress;
    }

    public List<String> getUris() {
        return uris;
    }

    public Boolean getUsessl() {
        return usessl;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getCrtFileAddress() {
        return crtFileAddress;
    }
}
