package com.test.elastic.api.service;

import org.springframework.stereotype.Service;

/**
 * @author yxm
 * @description: 测试api
 * @date 2022/2/22 19:23
 */
@Service
public class ElasticService {

    /*private final RestHighLevelClient restHighLevelClient;

    public ElasticService(RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }*/

    /*
     * 客户端
    private ElasticsearchClient client = ElasticSearchConfig.getClient();;

    /**
     * 创建索引
     *
     * @param index 索引名称
     * @return boolean
     * @throws IOException ioexception

    public boolean createIndex(String index) throws IOException {
        CreateIndexResponse response = this.client.indices().create(c -> c.index(index));
        System.out.println(response.toString());
        return response.acknowledged() == null ? false : response.acknowledged();
    }
    */

    /*因为ElasticSearch7.0之后都没有type了，只有一个_doc，所以就不必去做这个了
    public boolean createType(String type) throws IOException {

    }*/


    /*public boolean createIndex(String index) throws IOException {
        IndexRequest indexRequest = Requests.indexRequest(index);
        indexRequest.id("test");
        Person person = new Person();
        person.setAge(18);
        person.setName("xxx");
        person.setGender("man");
        indexRequest.type("_doc");
        indexRequest.source(JSONUtil.toJsonStr(person), XContentType.JSON);
        IndexResponse indexResponse = this.restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println(indexResponse.getIndex());
        System.out.println(indexResponse.getResult().getLowercase());
        return true;
    }*/

}
