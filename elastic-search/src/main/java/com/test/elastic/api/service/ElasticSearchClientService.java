package com.test.elastic.api.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.indices.CreateIndexRequest;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import com.test.elastic.api.domain.entity.Person;
import com.test.elastic.config.ElasticSearchClientUtil;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yxm
 * @description: 测试使用Java 原生es客户端
 * @date 2022/3/1 13:22
 */
@Service
public class ElasticSearchClientService {

    /*private RestHighLevelClient restHighLevelClient = RestHighLevelClientUtil.getClient();

    public boolean createIndex(String index) {
        CreateIndexRequest.Builder builder = new CreateIndexRequest.Builder();
        builder.index("index-test");
        CreateIndexRequest createIndexRequest = builder.build();
        restHighLevelClient.indices().create(createIndexRequest)
    }*/

    private ElasticsearchClient elasticsearchClient = ElasticSearchClientUtil.getClient();

    /**
     * 创建索引
     *
     * @param index 指数
     * @return boolean
     * @throws IOException ioexception
     */
    public boolean createIndex(String index) throws IOException {
        CreateIndexRequest.Builder builder = new CreateIndexRequest.Builder();
        builder.index(index);
        CreateIndexRequest createIndexRequest = builder.build();
        CreateIndexResponse indexResponse = elasticsearchClient.indices().create(createIndexRequest);
        return Boolean.TRUE.equals(indexResponse.acknowledged());
    }

    /**
     * 创建doc文档，放到指定的索引
     *
     * @param index  指数
     * @param person 人
     * @return boolean
     * @throws IOException ioexception
     */
    public boolean createByIndex(String index, Person person) throws IOException {
        IndexRequest.Builder<Person> builder = new IndexRequest.Builder<>();
        IndexRequest<Person> indexRequest = builder.id(person.getName())
                .document(person).index(index).build();
        IndexResponse response = elasticsearchClient.index(indexRequest);
        return response.result().jsonValue() != null;
    }

    /**
     * 搜索
     *
     * @param index  指数
     * @param person 人
     * @return {@link Object}
     */
    public List<Person> search(String index, Person person) throws IOException, IllegalAccessException {
        SearchRequest.Builder builder = new SearchRequest.Builder();
        builder.index(index);
        if (person != null) {
           // builder(person, builder);
            builder.query(q -> q.match(m -> m.field("name").query(v -> v.stringValue(person.getName()))))
                    .query(q -> q.match(m -> m.field("gender").query(v -> v.stringValue(person.getGender()))));
        }
        SearchRequest searchRequest = builder.build();
        SearchResponse<Person> searchResponse = elasticsearchClient.search(searchRequest, Person.class);
        List<Person> list = new ArrayList<>();
        if (searchResponse.hits() != null) {
            searchResponse.hits().hits().forEach(h -> list.add(h.source()));
        }
        return list;
    }

    private static void builder(Object object, SearchRequest.Builder builder) throws IllegalAccessException {
        Class cla = object.getClass();
        Field[] declaredFields = cla.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            if (declaredField.get(object) != null) {
                builder.query(q -> q.match(p -> p.field(declaredField.getName()).query(v -> {
                    try {
                        return v.stringValue(String.valueOf(declaredField.get(object)));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    return v;
                })));
            }
        }
    }

    public static void main(String[] args) throws IllegalAccessException, IOException {
        SearchRequest.Builder builder = new SearchRequest.Builder();
        builder.index("index-test");
        Person person = new Person();
        person.setName("ceshi");
        person.setAge(10);
        person.setGender("woman");
        //builder(person, builder);
       // System.out.println(builder);
        new ElasticSearchClientService().search("index-test", person);
    }

}
