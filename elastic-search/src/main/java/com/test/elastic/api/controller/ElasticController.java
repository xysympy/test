package com.test.elastic.api.controller;

import com.test.elastic.api.domain.entity.Person;
import com.test.elastic.api.service.ElasticSearchClientService;
import com.test.elastic.api.service.ElasticService;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * @author yxm
 * @description: elasticSearch 测试
 * @date 2022/2/22 19:33
 */
@RestController
@RequestMapping("/es")
public class ElasticController {
    private final ElasticService elasticService;
    private final ElasticSearchClientService elasticSearchClientService;

    public ElasticController(ElasticService elasticService, ElasticSearchClientService elasticSearchClientService) {
        this.elasticService = elasticService;
        this.elasticSearchClientService = elasticSearchClientService;
    }

    @GetMapping("/{index}")
    private boolean createIndex(@PathVariable("index") String index) throws IOException {
        // return elasticService.createIndex(index);
        // return elasticService.createIndex(index);
        return elasticSearchClientService.createIndex(index);
    }

    @PostMapping("/{index}/doc")
    public boolean createDoc(@RequestBody Person person, @PathVariable("index") String index) throws IOException {
        return elasticSearchClientService.createByIndex(index, person);
    }

    @PostMapping("/{index}/search")
    public List<Person> searchDoc(@RequestBody Person person, @PathVariable("index") String index) throws IOException, IllegalAccessException {
        return elasticSearchClientService.search(index, person);
    }
}
