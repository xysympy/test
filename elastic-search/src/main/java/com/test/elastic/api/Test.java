package com.test.elastic.api;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.test.elastic.api.domain.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.rmi.runtime.Log;

import java.util.StringJoiner;

/**
 * @author yxm
 * @description: 测试使用elasticSearch api
 * @date 2022/2/22 14:39
 */
public class Test {

    private static final Logger LOGGER = LoggerFactory.getLogger(Test.class);
    private static final String address = "http://localhost:9200";

    public static void main(String[] args) {
        // 测试创建索引
        /*Person person = new Person();
        person.setName("张三");
        person.setGender("man");
        person.setAge(1);
        System.out.println(createDoc("es", "person", "1", JSONUtil.toJsonStr(person)));
        person = new Person();
        person.setName("李四");
        person.setGender("man");
        person.setAge(2);
        System.out.println(createDoc("es", "person", "2", JSONUtil.toJsonStr(person)));
        person = new Person();
        person.setName("wangwu");
        person.setGender("man");
        person.setAge(3);
        System.out.println(createDoc("es", "person", "3", JSONUtil.toJsonStr(person)));*/
        // 更新也是上使用createIndex方法即可，只要index，type，id一致的时候，使用的就是覆盖或者说叫更新
        // 搜索
        System.out.println(queryById("es", "person", "1", null));
        System.out.println(queryById("es", "person", "", "name:张三"));
        System.out.println(queryById("es", "person1", "1", null));
        System.out.println(queryById("es", "person", " ", null));
        System.out.println(queryById("es", null, null, null));
        Person person = new Person();
        person.setName("张三");
        person.setGender("man");
        person.setAge(1);
        System.out.println(createDoc("es", "person1", "1", JSONUtil.toJsonStr(person)));
    }

    /**
     * 创建索引
     *
     * @param index 指数
     * @param type  类型
     * @param id    id
     * @return {@link String}
     */
    private static String createDoc(String index, String type, String id, String data) {
        StringJoiner stringJoiner = new StringJoiner("/");
        stringJoiner.add(address).add(index).add(type).add(id);
        LOGGER.info("url: {}", stringJoiner.toString());
        String body = HttpRequest.put(stringJoiner.toString())
                .body(data).execute().body();
        LOGGER.info("body: {}", body);
        return body;
    }

    /**
     * 查询通过id
     *
     * @param index 指数
     * @param type  类型
     * @param id    id
     * @param param 请求参数（精确查找）
     * @return {@link String}
     */
    private static String queryById(String index, String type, String id, String param) {
        if (id == null || "".equals(id.trim())) {
            id = "_search";
        }
        StringJoiner url = new StringJoiner("/");
        url.add(address).add(index).add(type).add(id);
        LOGGER.info("url: {}", url.toString());
        String body;
        if (param == null || "".equals(param.trim())) {
            body = HttpRequest.get(url.toString()).execute().body();
        }else {
            body = HttpRequest.get(url.toString()).form("p", param).execute().body();
        }
        LOGGER.info("body: {}", body);
        return body;
    }
}
