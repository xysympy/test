package com.test.elastic;

import cn.hutool.core.util.IdUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import com.test.elastic.api.domain.entity.Person;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.io.*;

/**
 * @author yxm
 * @description: 测试接口
 * @date 2022/3/1 14:36
 */
@SpringBootTest(classes = {ElasticApplication.class})
public class ElasticSearchTest {

    @Test
    public void testCreateIndex() {
        String index = "";
        String body = HttpRequest.get("localhost:9201/es/" + index)
                .execute().body();
        System.out.println(body);
    }

    @Test
    void testCreateDoc() {
        String index = "index-test";
        Person person = new Person();
        person.setName("1");
        person.setGender("woman");
        person.setAge(100);
        String body = JSONUtil.toJsonStr(person);
        String result = HttpRequest.post("localhost:9199/es/" + index + "/doc")
                .body(body).execute().body();
        System.out.println(result);
    }

    @Test
    void testSearchDoc() {
        String index = "index-test";
        Person person = new Person();
        person.setName("test");
        String body = JSONUtil.toJsonStr(person);
        String result = HttpRequest.post("localhost:9199/es/" + index + "/search")
                .body(body).execute().body();
        System.out.println(result);
    }

    @Resource
    private ElasticsearchClient client;

    @Test
    public void testClient() throws IOException {
        Person person = new Person();
        person.setName("xxxxxxx");
        person.setAge(1);
        person.setGender("man");
        IndexRequest<Person> indexRequest = new IndexRequest.Builder().index("index-test")
                .id(IdUtil.fastSimpleUUID()).document(person).build();
        IndexResponse result = client.index(indexRequest);
        System.out.println(result.result().jsonValue());
    }


    @Test
    void testMd5() throws IOException {
        String src = "1111";
        /*try (InputStream inputStream = new FileInputStream(new File("E:\\mfd1.zip"))) {
            ByteArrayOutputStream baos = cloneInputStream(inputStream);
            byte[] dataBytes = baos.toByteArray();
            baos.close();
            String md5 = getMd5(dataBytes);
            System.out.println(md5);
        }

        try (InputStream inputStream = new FileInputStream(new File("E:\\mfd.zip"))) {
            ByteArrayOutputStream baos = cloneInputStream(inputStream);
            byte[] dataBytes = baos.toByteArray();
            baos.close();
            String md5 = getMd5(dataBytes);
            System.out.println(md5);
        }*/

        InputStream inputStream = new FileInputStream(new File("E:\\mfd1.zip"));
        System.out.println(DigestUtils.md5DigestAsHex(inputStream));
        // 记住：文件流是只能读取一次的，所以直接这样测试的话，结果肯定是不一样的
        System.out.println(DigestUtils.md5DigestAsHex(inputStream));

        // System.out.println(new String(DigestUtils.md5Digest(src.getBytes())));
    }

    private static ByteArrayOutputStream cloneInputStream(InputStream input) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        while ((len = input.read(buffer)) > -1) {
            baos.write(buffer, 0, len);
        }
        baos.flush();
        return baos;
    }

    private String getMd5(byte[] bytes) throws IOException {
        return DigestUtils.md5DigestAsHex(bytes);
    }
}
