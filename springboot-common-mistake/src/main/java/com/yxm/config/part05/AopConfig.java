package com.yxm.config.part05;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class AopConfig {
    @Around("execution(* com.yxm.service.part05.ElectricService.pay()) ")
    public void recordPayPerformance(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        // 这个是有返回值的，所以idea会一直的提示返回值没有接收
        joinPoint.proceed();
        long end = System.currentTimeMillis();
        log.info("Pay method time cost（ms）: " + (end - start));
    }
}
