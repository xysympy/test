package com.yxm.service.part05;

import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ElectricService {
    public void charge() throws Exception {
        log.info("Electric Charging.....");
        // 知道是因为基于代理而实现的aop，所以this的调用实际上是调用了真实对象，所以，around就会失效了
        // this.pay();
        // 相当于是将proxy和threadLocal绑定在一起
        ElectricService electric = ((ElectricService) AopContext.currentProxy());
        electric.pay();
    }

    public void pay() throws Exception {
        log.info("Pay with alipay....");
        TimeUnit.SECONDS.sleep(1);
    }
}
