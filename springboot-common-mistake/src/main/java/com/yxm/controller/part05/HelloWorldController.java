package com.yxm.controller.part05;

import com.yxm.service.part05.ElectricService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class HelloWorldController {
    private final ElectricService electricService;

    public HelloWorldController(ElectricService electricService) {
        this.electricService = electricService;
    }

    @GetMapping(path = "charge")
    public void charge() throws Exception {
        electricService.charge();
    }
} 