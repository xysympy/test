package com.yxm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy = true)
public class CommonMistakeApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommonMistakeApplication.class);
    }
}
