package com.yxm.task;

import com.yxm.utils.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Component
public class NoticeTask {

    private SpringUtils springUtils;

    @Autowired
    public void setSpringUtils(SpringUtils springUtils) {
        this.springUtils = springUtils;
    }

    @PostConstruct
    public void testNotice() {
        System.out.println("notice start");
        // 首先，因为懒加载的缘故，我们前面直接使用springUtils.getBean的时候，就是没有加载springUtils，会一直等到使用的时候才会加载，所以...
        // 然后像现在的代码，就会出现循环依赖了，springboot貌似版本到了2.5之后就禁止循环依赖了
        // 7.27 号写个博客记录下这个东西吧
        SpringUtils.getBean(NoticeTask.class).notice();
        System.out.println("notice end");
    }

    @Transactional
    public void notice() {
        System.out.println("notice");
    }
}
