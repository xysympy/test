package com.yxm.mono;

import reactor.core.publisher.Mono;

import java.sql.Time;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ThenDeferTest {
    public static void main(String[] args) throws InterruptedException {
        List<TestDefer> list1 = IntStream.range(1, 6).mapToObj(i -> new TestDefer(i, "test" + i)).collect(Collectors.toList());
        List<TestDefer> list2 = IntStream.range(1, 6).mapToObj(i -> new TestDefer(i, "test" + i)).collect(Collectors.toList());

        // 虽然是then，但是也是会在构建链的时候就会执行start，而不是下面的defer起到延时的作用
        list1.stream().map(testDefer -> close(testDefer).then(start(testDefer))).forEach(Mono::subscribe);
        list2.stream().map(testDefer -> close(testDefer).then(Mono.defer(() -> start(testDefer)))).forEach(Mono::subscribe);

        TimeUnit.SECONDS.sleep(5);

        System.out.println(list1);
        System.out.println(list2);
    }

    private static Mono<Void> close(TestDefer testDefer) {
        return Mono.fromFuture(doClose(testDefer));
    }

    private static CompletableFuture<Void> doClose(TestDefer testDefer) {
        return CompletableFuture.runAsync(() -> {
            testDefer.name = "close";
            testDefer.value = 0;
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private static Mono<Void> start(TestDefer testDefer) {
        testDefer.name = "start";
        testDefer.value = 1;
        return Mono.just(testDefer).then();
    }

    static class TestDefer {
        private int value;

        private String name;

        public TestDefer(int value, String name) {
            this.value = value;
            this.name = name;
        }

        @Override
        public String toString() {
            return "TestDefer{" +
                    "value=" + value +
                    ", name='" + name + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TestDefer testDefer = (TestDefer) o;
            return value == testDefer.value && Objects.equals(name, testDefer.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value, name);
        }
    }
}
