package com.yxm.websocket;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yxm
 * @description: 获取设备token
 * @date 2022/2/14 15:08
 */
public class TokenGet {
    private final static String url = "https://tks.xmeye.net/v2/device/token";
    //时间戳
    private static long timMillis;
    //计数器
    private static long counter = 0L;

    public static String getToken(String uuid, String appKey, String[] sns, String userId, String appSecret, int movedCard) throws Exception {
        Map<String, String> headers = new HashMap<>(2);
        headers.put("uuid", uuid);
        headers.put("appKey", appKey);
        JSONObject body = new JSONObject();
        body.putOnce("userId", userId);
        body.putOnce("sns", sns);
        System.out.println("请求参数: " + body.toString());
        String timMillis = getTimMillis();
        String result = HttpRequest
                .post(url + "/" + timMillis + "/" + getEncryptStr(uuid, appKey, appSecret, timMillis, movedCard) + ".rs")
                .body(body.toString())
                .headerMap(headers, false)
                .execute().body();
        System.out.println(result);
        return JSONUtil.parseObj(result).getJSONArray("data").getJSONObject(0).getStr("token");
    }

    public static void main(String[] args) {
        try {
            getToken("e0534f3240274897821a126be19b6d46", "0621ef206a1d4cafbe0c5545c3882ea8"
                    , new String[]{"0621ef206a1d4cafbe0c5545c3882ea8"}, ""
                    , "90f8bc17be2a425db6068c749dee4f5d", 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取计数器
     *
     * @return
     */
    private static synchronized String getCounter(){
        ++counter;
        if (counter < 10L) {
            return "000000" + String.valueOf(counter);
        }
        else if (counter < 100L) {
            return "00000" + String.valueOf(counter);
        }
        else if (counter < 1000L) {
            return "0000" + String.valueOf(counter);
        }
        else if (counter < 10000L) {
            return "000" + String.valueOf(counter);
        }
        else if (counter < 100000L) {
            return "00" + String.valueOf(counter);
        }
        else if (counter < 1000000L) {
            return "0" + String.valueOf(counter);
        }
        else if (counter < 10000000L) {
            return String.valueOf(counter);
        }
        else {
            counter = 1L;
            return "000000" + String.valueOf(counter);
        }
    }

    /**
     * 获取组合时间戳
     *
     * @return
     */

    public static String getTimMillis() {
        timMillis = System.currentTimeMillis();
        return getCounter() + String.valueOf(timMillis);
    }
    /**
     * 获取签名字符串
     *
     * @param uuid       客户唯一标识
     * @param appKey     应用key
     * @param appSecret  应用密钥
     * @param timeMillis 时间戳
     * @param movedCard  移动取模基数
     * @return
     * @throws Exception
     */
    public static String getEncryptStr(String uuid, String appKey, String appSecret, String timeMillis, int movedCard) throws Exception {
        String encryptStr = uuid + appKey + appSecret + timeMillis;
        byte[] encryptByte = encryptStr.getBytes("iso-8859-1");
        byte[] changeByte = change(encryptStr, movedCard);
        byte[] mergeByte = mergeByte(encryptByte, changeByte);
        return SecureUtil.md5().digestHex(mergeByte);
    }

    /**
     * 简单移位
     */
    private static byte[] change(String encryptStr, int moveCard) throws UnsupportedEncodingException {
        byte[] encryptByte = encryptStr.getBytes("iso-8859-1");
        int encryptLength = encryptByte.length;
        byte temp;
        for (int i = 0; i < encryptLength; i++) {
            temp = ((i % moveCard) > ((encryptLength - i) % moveCard)) ? encryptByte[i] : encryptByte[encryptLength - (i + 1)];
            encryptByte[i] = encryptByte[encryptLength - (i + 1)];
            encryptByte[encryptLength - (i + 1)] = temp;
        }
        return encryptByte;
    }

    /**
     * 合并
     *
     * @param encryptByte
     * @param changeByte
     * @return
     */
    private static byte[] mergeByte(byte[] encryptByte, byte[] changeByte) {
        int encryptLength = encryptByte.length;
        int encryptLength2 = encryptLength * 2;
        byte[] temp = new byte[encryptLength2];
        for (int i = 0; i < encryptByte.length; i++) {
            temp[i] = encryptByte[i];
            temp[encryptLength2 - 1 - i] = changeByte[i];
        }
        return temp;
    }
}
