package com.yxm.websocket;

import lombok.extern.slf4j.Slf4j;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Map;

/**
 * @author yxm
 * @description: 自定义实现连接websocket
 * @date 2022/2/14 14:12
 */
public class MyMsgWebSocket extends WebSocketClient {

    private static Logger logger = LoggerFactory.getLogger(WebsocketClient.class);

    public MyMsgWebSocket(URI serverUri, Map<String, String> httpHeaders) {
        super(serverUri, httpHeaders);
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        logger.info("握手成功");
    }

    @Override
    public void onMessage(String msg) {
        logger.info("收到消息==========" + msg);
        if (msg.equals("over")) {
            this.close();
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        logger.info("链接已关闭" + ",原因" + s);
    }

    @Override
    public void onError(Exception e) {
        e.printStackTrace();
        logger.info("发生错误已关闭");
    }
}
