package com.yxm.websocket;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.enums.ReadyState;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class WebsocketClient {

    private static Logger logger = LoggerFactory.getLogger(WebsocketClient.class);
    public static WebSocketClient client;

    public static void main(String[] args) throws Exception {
        try {
            Map<String, String> headers = new HashMap<>(1);
            // headers.put("Sec-WebSocket-Extensions", "permessage-deflate; client_max_window_bits");
            headers.put("Origin", "http://10.10.18.55");
            client = new MyMsgWebSocket(new URI("ws://rds-test.bcloud365.net/v1/ws"), headers);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        client.connect();
        //判断连接状态，0为请求中  1为已建立  其它值都是建立失败
        while(client.getReadyState().ordinal() != ReadyState.OPEN.ordinal()) {
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
                logger.warn("延迟操作出现问题，但并不影响功能");
            }
            logger.info("连接中。。。");
        }
        String token = TokenGet.getToken("e0534f3240274897821a126be19b6d46", "0621ef206a1d4cafbe0c5545c3882ea8"
                , new String[]{"7ab9efc748e55db8"}, ""
                , "90f8bc17be2a425db6068c749dee4f5d", 2);
        JSONObject content = new JSONObject();

        client.send(getData(WebSocketMethod.DEVICE_STATUS.getMethod() + token, ""));
        client.send(getData(WebSocketMethod.DEVICE_WAKEUP.getMethod() + token, ""));
        // 获取设备直播地址
        content.putOnce("mediaType", "hls");
        content.putOnce("channel", "0");
        content.putOnce("stream", "0");
        content.putOnce("protocol", "ts");
        content.putOnce("username", "admin");
        content.putOnce("devPwd", "1qaz2wsx3e");
        client.send(getData(WebSocketMethod.DEVICE_BROADCAST_ADDRESS.getMethod() + token, content.toString()));
        /*token = TokenGet.getToken("e0534f3240274897821a126be19b6d46", "0621ef206a1d4cafbe0c5545c3882ea8"
                , new String[]{"b11ffe8d6b812473"}, ""
                , "90f8bc17be2a425db6068c749dee4f5d", 2);*/
        //登录设备
        content.clear();
        content.putOnce("EncryptType", "MD5");
        content.putOnce("LoginType", "DVRIP-Web");
        content.putOnce("PassWord", "1qaz2wsx3e");
        content.putOnce("UserName", "admin");
        content.putOnce("Name", "generalinfo");
        client.send(getData(WebSocketMethod.DEVICE_LOGIN.getMethod() + token, content.toString()));
        // 获取设备信息
        content.clear();
        content.putOnce("Name", "SystemInfo");
        client.send(getData(WebSocketMethod.DEVICE_INFO.getMethod() + token, content.toString()));
        // 获取设备能力
        content.replace("Name", "SystemFunction");
        client.send(getData(WebSocketMethod.DEVICE_ABILITY.getMethod() + token, content.toString()));
        // 设备保活
        content.replace("Name", "KeepAlive");
        client.send(getData(WebSocketMethod.DEVICE_KEEP_ALIVE.getMethod() + token, content.toString()));
        // 获取通道名称
        content.replace("Name", "ChannelTitle");
        client.send(getData(WebSocketMethod.DEVICE_GET_CONFIG.getMethod() + token, content.toString()));
        // 外部报警设置
        content.replace("Name", "Alarm.LocalAlarm");
        client.send(getData(WebSocketMethod.DEVICE_GET_CONFIG.getMethod() + token, content.toString()));
        // 网络基础设置
        content.replace("Name", "NetWork.NetCommon");
        client.send(getData(WebSocketMethod.DEVICE_GET_CONFIG.getMethod() + token, content.toString()));
        // 本地化设置
        content.replace("Name", "General.Location");
        client.send(getData(WebSocketMethod.DEVICE_GET_CONFIG.getMethod() + token, content.toString()));
        // 设置设备配置
        content.clear();
        content.putOnce("Name", "ChannelTitle");
        JSONArray channelTitle = new JSONArray();
        channelTitle.put("cha");
        content.putOnce("ChannelTitle", channelTitle);
        client.send(getData(WebSocketMethod.DEVICE_SET_CONFIG.getMethod() + token, content.toString()));
        // 设备保活
        content.clear();
        content.putOnce("Name", "KeepAlive");
        client.send(getData(WebSocketMethod.DEVICE_KEEP_ALIVE.getMethod() + token, content.toString()));
        //设备操作
        content.clear();
        content.putOnce("Name", "OPVersionList");
        client.send(getData(WebSocketMethod.DEVICE_OPERATION.getMethod() + token, content.toString()));
        // 设备抓图
        content.clear();
        content.putOnce("Name", "OPSNAP");
        JSONObject opSnap = new JSONObject();
        opSnap.putOnce("Channel", 0);
        opSnap.putOnce("PicType", 0);
        content.putOnce("OPSNAP", opSnap);
        client.send(getData(WebSocketMethod.DEVICE_SNAPSHOOT.getMethod() + token, content.toString()));
        // 设备用户相关
        content.clear();
        content.putOnce("Name", "USERS");
        client.send(getData(WebSocketMethod.DEVICE_USER.getMethod() + token, content.toString()));
    }

    private static String getData(String method, String data) {
        JSONObject body = new JSONObject();
        if (CharSequenceUtil.startWith(data, '{')) {
            body.putOnce("body", JSONUtil.parseObj(data));
        }else {
            body.putOnce("body", data);
        }
        body.putOnce("method", method);
        logger.info(body.toString());
        return body.toString();
    }

}