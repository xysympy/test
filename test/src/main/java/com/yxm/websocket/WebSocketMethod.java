package com.yxm.websocket;

public enum WebSocketMethod {
    DEVICE_STATUS("/v2/rtc/status/query/", "get device status")
    , DEVICE_WAKEUP("/v2/rtc/device/wakeup/", "low-power device wakeup")
    , DEVICE_BROADCAST_ADDRESS("/v2/rtc/device/livestream/", "get device live broadcast address")
    , DEVICE_INFO("/v2/rtc/device/getinfo/", "get device information")
    , DEVICE_LOGIN("/v2/rtc/device/login/", "device login")
    , DEVICE_ABILITY("/v2/rtc/device/getability/", "get device capability set")
    , DEVICE_KEEP_ALIVE("/v2/rtc/device/keepalive/", "device keep alive")
    , DEVICE_GET_CONFIG("/v2/rtc/device/getconfig/", "device get config")
    , DEVICE_SET_CONFIG("/v2/rtc/device/setconfig/", "device set config")
    , DEVICE_OPERATION("/v2/rtc/device/opdev/", "device operation")
    , DEVICE_SNAPSHOOT("/v2/rtc/device/capture/", "device snapshoot")
    , DEVICE_USER("/v2/rtc/device/usermanage/", "device user related");

    private final String method;
    private final String desc;

    WebSocketMethod(String method, String desc) {
        this.method = method;
        this.desc = desc;
    }

    public String getMethod() {
        return method;
    }

    public String getDesc() {
        return desc;
    }
}