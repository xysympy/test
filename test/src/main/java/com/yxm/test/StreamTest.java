package com.yxm.test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yxm
 * @description: 测试流
 * @date 2021/11/4 16:58
 */
public class StreamTest {
    public static void main(String[] args) {
        // 将["hello","world"]变成["h","e","l","o","w","r","d"]
        List<String> source = Arrays.asList("hello", "world");
        List<String[]> collect = source.stream().map(world -> world.split("")).distinct()
                .collect(Collectors.toList());
        System.out.println(collect);
        source.stream().flatMap(world->Arrays.stream(world.split("")))
                .distinct().forEach(System.out::print);
        source.stream().forEach(System.out::print);
    }
}
