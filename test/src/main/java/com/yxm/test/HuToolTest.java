package com.yxm.test;

import cn.hutool.crypto.symmetric.AES;
import cn.hutool.json.JSONConfig;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;

/**
 * @author yxm
 * @description: 测试hutool工具类
 * @date 2021/12/13 19:13
 */
public class HuToolTest {
    public static void main(String[] args) {
        AES aes = new AES();
        SecretKeySpec secretKey = (SecretKeySpec) aes.getSecretKey();
        System.out.println(Arrays.toString(secretKey.getEncoded()));
        String encodeToString = Base64.getEncoder().encodeToString(secretKey.getEncoded());
        // 将秘钥加密成base64字符串
        System.out.println(encodeToString);
        System.out.println(Arrays.toString(cn.hutool.core.codec.Base64.decode(encodeToString)));
        // 测试使用hutool的JSON转换问题
        HutoolClass hutoolClass = new HutoolClass();
        hutoolClass.setName("hutool");
        hutoolClass.setAge(11);
        hutoolClass.setGender("woman");
        System.out.println(JSONUtil.toJsonStr(hutoolClass));
        hutoolClass.setGender(null);
        System.out.println(JSONUtil.toJsonStr(hutoolClass));
        // 实现null值也能转换为gender="null"这样的
        String toString = JSONUtil.wrap(hutoolClass, new JSONConfig().setIgnoreNullValue(false)).toString();
        System.out.println(toString);
        System.out.println(JSONUtil.toBean(toString, HutoolClass.class));
        /*String res = "{\"Header\":{\"MessageType\":\"DEV_VIP_CONFIG_ON_REQ\"},\"Body\":{\"SerialNumber\":\"c142dd39f8222e1d\",\"HardWare\":\"RM50H20L_8188EU_S38\",\"DevOemId\":\"C14\",\"ServiceList\":null}}";
        System.out.println(JSONUtil.toBean(res, CfgProtocolDto.class));
        JSONConfig jsonConfig = new JSONConfig();
        jsonConfig.setIgnoreCase(true);
        JSONObject jsonObject = new JSONObject(res, jsonConfig);
        CfgProtocolDto cfgProtocolDto = jsonObject.toBean(CfgProtocolDto.class);
        System.out.println(cfgProtocolDto);
        // 没有办法做到序列化成上面的字符串
        System.out.println(JSONUtil.wrap(cfgProtocolDto, jsonConfig).toString());*/
    }
}

class HutoolClass {
    private String name;
    private Integer age;
    private String gender;

    public HutoolClass() {
    }

    public HutoolClass(String name, Integer age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "HutoolClass{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HutoolClass that = (HutoolClass) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(age, that.age) &&
                Objects.equals(gender, that.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, gender);
    }

}