package com.yxm.test.function;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * @ClassName: OptionalTest
 * @Description: 测试下使用optional 嵌套校验不为null
 * @Author: yxm
 * @Date: 2022/7/6 21:12
 * @Version: 1.0
 **/
public class OptionalTest {
    public static void main(String[] args) {
        Outer outer = new Outer();
        Optional.of(outer)
                .map(Outer::getNested)
                .map(Nested::getInner)
                .map(Inner::getFoo)
                .ifPresent(System.out::println);
       /* resolve(() -> Optional.ofNullable(outer.getNested().getInner().getFoo())).ifPresent(System.out::println);*/
    }

    private static Optional<String> resolve(Supplier<Optional<String>> supplier) {
        return supplier.get();
    }
}

class Outer {
    Nested nested;

    Nested getNested() {
        return nested;
    }
}

class Nested {
    Inner inner;

    Inner getInner() {
        return inner;
    }
}

class Inner {
    String foo;

    String getFoo() {
        return foo;
    }
}