package com.yxm.test.function;

import java.util.function.Function;

/**
 * @author yxm
 * @description: 柯里化意为：将一个多参数的函数，转换为一系列单参数函数。
 * @date 2022/3/24 14:59
 */
public class CurryFunctionTest {
    public static void main(String[] args) {
        // 使用Function普通实现
        Function<String, String> function1 = s -> s.toLowerCase();
        // Function<T, R>类似于将T转换为R的这种格式
        Function<String, Function<String, String>> function2 =
                a -> b -> a + b;
        // 转换成匿名内部类的实现
        Function<String, Function<String, String>> function3 =
                new Function<String, Function<String, String>>() {
                    @Override
                    public Function<String, String> apply(String a) {
                        return new Function<String, String>() {
                            @Override
                            public String apply(String b) {
                                return a + b;
                            }
                        };
                    }
                };
        // 应用
        Function<String, String> apply = function2.apply("1");
        System.out.println(apply.apply("2"));
    }
}
