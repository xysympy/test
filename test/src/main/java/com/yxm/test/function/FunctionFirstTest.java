package com.yxm.test.function;

/**
 * @author yxm
 * @description: 简单尝试下函数式编程
 * @date 2022/3/23 16:20
 */
public class FunctionFirstTest {
    Strategy strategy;
    String msg;
    FunctionFirstTest(String msg) {
        strategy = new Soft(); // [1]
        this.msg = msg;
    }
    void communicate() {
        System.out.println(strategy.approach(msg));
    }
    void changeStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
    public static void main(String[] args) {
        Strategy[] strategies = {
                new Strategy() { // [2]
                    @Override
                    public String approach(String msg) {
                        return msg.toUpperCase() + "!";
                    }
                },
                // 可以理解为这下面的两个都是匿名内部类，不过前面一个用的是lambda，后面一个是方法引用
                msg -> msg.substring(0, 5), // [3]
                Unrelated::twice // [4]
        };
        FunctionFirstTest s = new FunctionFirstTest("Hello there");
        s.communicate();
        for(Strategy newStrategy : strategies) {
            s.changeStrategy(newStrategy); // [5]
            s.communicate(); // [6]
        }
    }
}
// functional/Strategize.java
interface Strategy {
    String approach(String msg);
}
class Soft implements Strategy {
    @Override
    public String approach(String msg) {
        return msg.toLowerCase() + "?";
    }
}
class Unrelated {
    static String twice(String msg) {
        return msg + " " + msg;
    }
}