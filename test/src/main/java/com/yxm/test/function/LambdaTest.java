package com.yxm.test.function;

import java.util.function.Function;

/**
 * @author yxm
 * @description: 测试一下lambda表达式作为方法参数
 * @date 2021/12/16 15:12
 */
public class LambdaTest {

    public String lambda(FunctionTest function, String str) {
        return function.handle(str);
    }

    public static void main(String[] args) {
        LambdaTest test = new LambdaTest();
        String string = "str";
        // 这样的写法，前提是对应的那个接口只有一个方法，不然就会出现，不知道你调用哪一个
        // 有例外，就是如果你在接口中重写了Object的方法的时候是没有问题的，它还是函数式接口
        // 调用的还是自己自定义的那个。为什么能重写Object的方法，因为Java标准
        // 参考:https://www.cnblogs.com/softnovo/articles/4546418.html
        System.out.println(test.lambda(str -> str.toUpperCase(), string));

        // 测试使用Function
        Function<String, String> function = String::toUpperCase;
        // compose是先执行传入进来的function andThen是先执行原来的function，然后执行传入进来的function
        System.out.println(function.compose(s -> s + "compose").andThen(s -> s + "andThen")
                .apply("function test"));
        System.out.println(function.apply("function test"));
    }
}
