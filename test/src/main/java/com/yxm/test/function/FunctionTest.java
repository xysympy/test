package com.yxm.test.function;

/**
 * @author yxm
 * @description: 测试下lambda表达式作为参数传递
 * @date 2021/12/16 15:11
 */
public interface FunctionTest{
    /**
     * 处理
     *
     * @param str str
     * @return {@link String}
     */
    String handle(String str);

    @Override
    boolean equals(Object t);

}
