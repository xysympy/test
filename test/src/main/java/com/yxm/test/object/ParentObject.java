package com.yxm.test.object;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/12/10 9:54
 */
public class ParentObject {
    public static void main(String[] args) {
        ParentObject parentObject = new SonObject();
        parentObject = (ParentObject) parentObject;
        System.out.println(parentObject.getClass());
        System.out.println(getObject());
    }

    private static ParentObject getObject(){
        return (ParentObject) new SonObject();
    }
}

class SonObject extends ParentObject{}
