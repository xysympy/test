package com.yxm.test.string;

/**
 * @ClassName: FnTostring
 * @Description: 递归toString方法
 * @Author: yxm
 * @Date: 2022/4/5 15:23
 * @Version: 1.0
 **/
public class FnTostring {
    public static void main(String[] args) {

    }
}

class ToString {
    @Override
    public String toString() {
        // 这个会导致toString循环
        // 也就是这个this会自动的被编译器去调用toString方法，然后递归了
        return "ToString test: " + this;
    }
}