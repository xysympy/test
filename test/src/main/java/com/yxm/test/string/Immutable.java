package com.yxm.test.string;

/**
 * @author yxm
 * @description:
 * @date 2022/3/29 16:52
 */
public class Immutable {

    private static String upCase(String str) {
        return str.toUpperCase();
    }

    private static StringBuffer append(StringBuffer sb) {
        sb.append("append");
        return sb;
    }

    public static void main(String[] args) {
        // 原本的想法是因为String是对象，所以如果形参是对象，那么传入的参数就是引用的复制
        // 所以应该是里面改了，外面也会改，但是，这个是基于可变对象的，实际上，但是String是不可变的
        // 下面的StringBuffer就能看到，如果传入的是可变的对象，那么对应的就会发生改变
        String p = "string";
        System.out.println(p);
        String q = upCase(p);
        System.out.println(q);
        System.out.println(p);
        StringBuffer sb = new StringBuffer();
        sb.append("sb");
        System.out.println(sb);
        StringBuffer append = append(sb);
        System.out.println(append);
        System.out.println(sb);
    }
}
