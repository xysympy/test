package com.yxm.test.time;

import java.util.Calendar;
import java.util.Date;

/**
 * @author yxm
 * @description: 测试使用Calender
 * @date 2022/4/13 11:29
 */
public class CalenderTest {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        System.out.println(calendar.getTime().toString());
        calendar.clear();
        System.out.println(calendar.getTime());
    }
}
