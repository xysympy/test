package com.yxm.test;

/**
 * @author Administrator
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/19 9:53
 */
public class IsAssignableFromTest {
    public static void main(String[] args) {

        ExtendsFormTest formTest = new ExtendsFormTest();
        IsAssignableFromTest isAssignableFromTest = new IsAssignableFromTest();
        System.out.println(IsAssignableFromTest.class.isAssignableFrom(formTest.getClass()));
        System.out.println(IsAssignableFromTest.class.isAssignableFrom(isAssignableFromTest.getClass()));
        System.out.println(ExtendsFormTest.class.isAssignableFrom(formTest.getClass()));
        System.out.println(ExtendsFormTest.class.isAssignableFrom(isAssignableFromTest.getClass()));
    }
}

class ExtendsFormTest extends IsAssignableFromTest{

}
