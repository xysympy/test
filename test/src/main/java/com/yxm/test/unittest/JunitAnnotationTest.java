package com.yxm.test.unittest;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yxm
 * @description: 使用下Junit5 的部分注解
 * 对于直接对Junit测试类直接执行的话，那么下面的注解的执行顺序就是
 * BeforeAll->BeforeEach->Test->AfterEach->BeforeEach->Test->AfterEach->AfterAll
 * 如果是单个测试的话
 * BeforeAll->BeforeEach->Test->AfterEach->AfterAll
 * 之所以得用static的方法来标注@BeforeAll和@AfterAll是因为Junit5在调用@Test对应的方法的时候
 * 实际上是直接new了一个单元测试类，然后进行调用对应的方法的。可以通过在单元测试类上面添加无参构造
 * 器，然后打断点看看执行过程即可
 * @TestInstance(TestInstance.Lifecycle.PER_CLASS)如果添加了这个的话，对应的，实际上
 * 单元测试执行的时候，是只创建了一个对象，然后执行所有的方法，那么这个时候BeforeAll和AfterAll
 * 注解就完全是可以使用非静态方法的
 * @date 2022/3/26 16:38
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JunitAnnotationTest {

    private static JunitAnnotationList junitAnnotationList;

    public JunitAnnotationTest() {
    }

    @BeforeAll
    public void beforeAll() {
        junitAnnotationList = new JunitAnnotationList(10);
    }

    @BeforeEach
    public void beforeEach() {
        junitAnnotationList = new JunitAnnotationList();
    }

    @Test
    public void add() {
        junitAnnotationList.add("1111");
        junitAnnotationList.add("2222");
        junitAnnotationList.add("3333");
    }

    @Test
    public void remove() {
        try {
            junitAnnotationList.remove("11111");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @AfterAll
    public void afterAll() {
        System.out.println("afterAll");
    }

    @AfterEach
    public void afterEach() {
        System.out.println("afterEach");
    }

}

class JunitAnnotationList{

    private List<String> list;

    public JunitAnnotationList(int initialSize) {
        System.out.println("JunitAnnotationList(int initialSize)");
        this.list = new ArrayList<>(initialSize);
    }

    public JunitAnnotationList() {
        System.out.println("JunitAnnotationList()");
        this.list = new ArrayList<>();
    }

    public boolean add(String value) {
        return this.list.add(value);
    }

    public boolean add(int index, String value) {
        if (index >= this.list.size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        try {
            this.list.add(index, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String remove(int index) throws IndexOutOfBoundsException{
        if (index >= this.list.size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        return this.list.remove(index);
    }

    public boolean remove(String value) throws NoSuchFieldException{
        if (!this.list.contains(value)) {
            throw new NoSuchFieldException();
        }
        return this.list.remove(value);
    }

    public String get(int index) {
        if (index >= this.list.size() || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        return this.list.get(index);
    }

}