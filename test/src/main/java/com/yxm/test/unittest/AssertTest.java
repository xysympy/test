package com.yxm.test.unittest;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yxm
 * @description: 测试使用断言机制
 * assert boolean-expression: information-expression;
 * 类似于三目运算，assert后面的表达式是false的时候，就是抛出异常，打印异常信息
 * 关于if-else和断言机制的区别
 * 一般if-else是对于流程的把控的，也就是if....else....而断言是类似于if throw new Exception这样的
 * 还有就是断言的information-expression只是输出，不能有语句，只能是类似于变量的那种
 * assert false : "false"/1...;
 * @date 2022/3/28 9:59
 */
public class AssertTest {

    private static AssertClass assertClass;
    private final static Logger log = LoggerFactory.getLogger(AssertTest.class);

    public static void main(String[] args) {
        // 前提是你得开启断言机制
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        // 这样是没有效果的，因为这个时候已经是加载了这个类了。这个设置是对类起作用的，
        // 这个类已经加载到classLoader里面了，那么这个设置就不会对它起作用了
        // 但是如果是通过添加jvm参数-ea的话，还是能起作用的，应该是因为这个类还没有加载吧
        // 可以通过-da关闭断言  -ea -da
        // new AssertTest().assertTest();
        // 这个才有用(针对的是代码开启断言机制)
        try {
            new AssertPublicClass().assertTest();
        } catch (AssertionError e) {
            e.printStackTrace();
        }
        System.out.println(1);
        System.out.println("finally print");
        try {
            new AssertPublicClass().assertTest();
        } catch (AssertionError e) {
            e.printStackTrace();
        }
        assertClass = new AssertClass();
        assertClass.print();
        log.info("assertClass.print()");
        try {
            assertClass.throwException();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        assertClass.assertTest();
        // 之所以测试这些，就是发现即使输出语句是在异常的前面，输出的是时候还是会在后面
        // 所以是为了测试看看是不是所有的异常都是这样，然后发现，异常之间有顺序，但所有的异常先输出
        // 有个前提：异常在输出语句之前输出。如果输出语句先输出的，那么就变成输出语句先输出，不论顺序
    }
    public void assertTest() {
        assert false : "AssertTest false";
    }
}

class AssertClass {
    public void assertTest() {
        assert false : "AssertClass false";
    }

    public void print() {
        System.out.println("AssertClass print()");
    }

    public void throwException() throws RuntimeException {
        System.out.println("before throw exception");
        try {
            throw new RuntimeException("custom exception");
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("AssertClass throwException()");
    }

    public static void main(String[] args) {
        Preconditions.checkElementIndex(1, 3);
        Preconditions.checkPositionIndex(1, 6);
    }
}
