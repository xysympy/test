// validating/CircularQueue.java
// Demonstration of Design by Contract (DbC)
package com.yxm.test.unittest;

import java.util.Arrays;

public class CircularQueue {
    private Object[] data;
    // Next available storage space
    private int in = 0,
    // Next gettable object
    out = 0;
    // Has it wrapped around the circular queue?
    private boolean wrapped = false;

    public CircularQueue(int size) {
        data = new Object[size];
        // Must be true after construction:
        assert invariant();
    }

    public boolean empty() {
        return !wrapped && in == out;
    }

    public boolean full() {
        return wrapped && in == out;
    }

    public boolean isWrapped() {
        return wrapped;
    }

    public void put(Object item) {
        precondition(item != null, "put() null item");
        precondition(!full(),
                "put() into full CircularQueue");
        assert invariant();
        data[in++] = item;
        if (in >= data.length) {
            // 数组满了的前提是in指针至少会走完一遍数组
            in = 0;
            wrapped = true;
        }
        assert invariant();
    }

    public Object get() {
        precondition(!empty(),
                "get() from empty CircularQueue");
        assert invariant();
        Object returnVal = data[out];
        data[out] = null;
        out++;
        if (out >= data.length) {
            // 因为数组为空的前提就是你至少out会走完一遍数组
            out = 0;
            wrapped = false;
        }
        assert postcondition(
                returnVal != null,
                "Null item in CircularQueue");
        assert invariant();
        return returnVal;
    }

    // Design-by-contract support methods:
    private static void precondition(boolean cond, String msg) {
        if (!cond) {
            throw new CircularQueueException(msg);
        }
    }

    private static boolean postcondition(boolean cond, String msg) {
        if (!cond) {
            throw new CircularQueueException(msg);
        }
        return true;
    }

    private boolean invariant() {
        // Guarantee that no null values are in the
        // region of 'data' that holds objects:
        for (int i = out; i != in; i = (i + 1) % data.length) {
            if (data[i] == null) {
                throw new CircularQueueException("null in CircularQueue");
            }
        }
        // Guarantee that only null values are outside the
        // region of 'data' that holds objects:
        if (full()) {
            return true;
        }
        for (int i = in; i != out; i = (i + 1) % data.length) {
            if (data[i] != null) {
                throw new CircularQueueException(
                        "non-null outside of CircularQueue range: " + dump());
            }
        }
        return true;
    }

    public String dump() {
        return "in = " + in +
                ", out = " + out +
                ", full() = " + full() +
                ", empty() = " + empty() +
                ", CircularQueue = " + Arrays.asList(data);
    }

    public static void main(String[] args) {
        CircularQueue circularQueue = new CircularQueue(10);
        circularQueue.put(1);
        circularQueue.put(2);
        circularQueue.put(3);
        circularQueue.put(4);
        circularQueue.put(5);
        circularQueue.put(6);
        circularQueue.put(7);
        circularQueue.put(8);
        circularQueue.put(9);
        circularQueue.put(10);
        circularQueue.get();
        circularQueue.get();
        System.out.println(circularQueue.isWrapped());
        System.out.println(circularQueue.empty());
        circularQueue.get();
        System.out.println(circularQueue.empty());
        System.out.println(circularQueue.dump());
        circularQueue.put(11);
        circularQueue.put(12);
        circularQueue.put(13);
        System.out.println(circularQueue.isWrapped());
        System.out.println(circularQueue.dump());
    }
}

// validating/CircularQueueException.java
class CircularQueueException extends RuntimeException {
    public CircularQueueException(String why) {
        super(why);
    }
}