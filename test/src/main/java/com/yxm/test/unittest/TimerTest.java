package com.yxm.test.unittest;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author yxm
 * @description: 自定义Timer
 * @date 2022/3/28 15:08
 */
public class TimerTest {

    private long start = System.nanoTime();

    private long duration() {
        return TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);
    }

    public static long duration(Runnable test) {
        TimerTest timer = new TimerTest();
        test.run();
        return timer.duration();
    }
}

class MainTest {

    public static void main(String[] args) {
        int[] ints = new int[25_000_000_0];
        // 串行
        System.out.println("" + TimerTest.duration(() -> Arrays.setAll(ints, n -> (n * 2 + 1) * 10 / 10 + 1)));
        // 并行
        System.out.println("" + TimerTest.duration(() -> Arrays.parallelSetAll(ints, n -> (n * 2 + 1) * 10 / 10 + 1)));
        // 有的时候会产生并行操作的时间高于串行，应该是因为并行相较于串行有额外的消耗
        // 当额外的消耗是大于代码的执行时，就会出现并行低于串行，但是小于的时候就不会
        System.out.println("" + TimerTest.duration(() -> Arrays.setAll(ints, n -> new Random().nextInt())));
        System.out.println("" + TimerTest.duration(() -> Arrays.parallelSetAll(ints, n -> new Random().nextInt())));
    }
}