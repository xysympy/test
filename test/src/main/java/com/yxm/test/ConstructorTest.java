package com.yxm.test;

import org.w3c.dom.NameList;

/**
 * @Description: 测试子类调用父类构造器的情况
 * 测试过后，显示子类的构造器会默认的调用一个无参构造器（如果不是显示的使用super调用父类的构造器的话）
 * @Author yxm
 * @Date 2021/10/15 10:03
 */
public class ConstructorTest {
    public static void main(String[] args) {
        Student student = new Student("student", 2);
    }
}

/**
 * 人
 *
 * @author yxm
 * @date 2021/10/15
 */
class Person{
    /**
     * 名字
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;

    public Person(String name, Integer age){
        this.name = name;
        this.age = age;
        System.out.println("Person has param");
    }

    Person() {
        System.out.println("Person");
    }

    public Person getPerson(){
        return new Person();
    }
}

/**
 * 学生
 *
 * @author Administrator
 * @date 2021/10/15
 */
class Student extends Person{
    /**
     * 名字
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;

    public Student(String name, Integer age) {
        this.age = age;
        this.name = name;
    }

    @Override
    public Student getPerson() {
        return new Student("",1);
    }
}