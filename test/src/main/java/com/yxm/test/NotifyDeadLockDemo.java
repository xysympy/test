package com.yxm.test;

public class NotifyDeadLockDemo {

    public static void main(String[] args) {
        final OutTurn outTurn = new OutTurn();
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                for (int j = 0; j < 5; j++) {
                    outTurn.sub();
                }

            }).start();

            new Thread(() -> {
                for (int j = 0; j < 5; j++) {
                    outTurn.main();
                }
            }).start();
        }
    }

}

class OutTurn {
    private boolean isSub = true;
    private int count = 0;

    public synchronized void sub() {
        try {
            while (!isSub) {
                this.wait();
            }
            System.out.println("sub --- " + count);
            isSub = false;
            this.notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count++;
    }

    public synchronized void main() {
        try {
            while (isSub) {
                this.wait();
            }
            System.out.println("main --- " + count);
            isSub = true;
            this.notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count++;
    }
}