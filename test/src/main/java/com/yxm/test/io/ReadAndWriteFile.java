package com.yxm.test.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author yxm
 * @description: 文件读写
 * @date 2022/3/29 15:50
 */
public class ReadAndWriteFile {
    public static void main(String[] args) {
        try (Stream<String> stream = Files.lines(Paths.get("E:", "我的", "user.sql"));
             BufferedWriter bufferedWriter = Files.newBufferedWriter(
                     Paths.get("E:", "我的", "my-application.log"), StandardOpenOption.APPEND);) {
            List<String> stringList = stream
                    .skip(12)
                    .limit(new Random().nextInt(30))
                    .collect(Collectors.toList());
            for (String str : stringList) {
                bufferedWriter.write(str + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 简单的读取文件
        try {
            Files.readAllLines(Paths.get("E:", "我的", "my-application.log"))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 简单的写文件
        /*可以执行，但是对于下面的测试不太友好，因为这个是随机生成的byte数组，所以会乱码，然后就会导致读取的时候报错
        Random random = new Random();
        byte[] bytes = new byte[1024];
        random.nextBytes(bytes);
        try {
            Files.write(Paths.get("E:", "我的", "my-application.log"), bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        // 转换为流读取
        try (Stream<String> stream = Files.lines(Paths.get("E:", "我的", "my-application.log"));
             Stream<String> stream1 = Files.lines(Paths.get("E:", "我的", "my-application.log"))) {
            stream
                    .skip(1)
                    .findFirst()
                    .ifPresent(System.out::println);
            stream1
                    .skip(1)
                    .findAny()
                    .ifPresent(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 流的读取和写入相结合
        try (Stream<String> stream = Files.lines(Paths.get("E:", "我的", "my-application.log"));
             PrintWriter print = new PrintWriter("E://我的//ReadAndWriteFile.txt");) {
            stream.map(String::toUpperCase).forEach(print::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
