package com.yxm.test.io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * 删除文件中指定行之后的数据
 */
public class DeleteFileByLines {
    public static void main(String[] args) throws IOException {
        String testShPath = "E:\\ideaProject\\test\\test\\sh\\test.sh";
        deleteFilesByLine(testShPath, -4);
    }

    private static void deleteFilesByLine(String path, int index) throws IOException {
        Path path1 = Path.of(path);
        List<String> strings = Files.readAllLines(path1, StandardCharsets.UTF_8);
        List<String> subList = strings.subList(0, strings.size() + index);
        Files.write(path1, subList, StandardCharsets.UTF_8);
    }
}
