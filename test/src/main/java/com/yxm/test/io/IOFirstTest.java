package com.yxm.test.io;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author yxm
 * @description: IO代码
 * @date 2022/3/29 13:31
 */
public class IOFirstTest {
    public static void main(String[] args) {
        PathInfo pathInfo = new PathInfo();
        System.out.println(System.getProperty("os.name"));
        pathInfo.info(Paths.get("C:", "我的", "user.sql"));
        // Java中的使用相对路径是相对于user.dir这个变量的
        System.out.println(System.getProperty("user.dir"));
        Path path = Paths.get("test", "IOFirstTest.java");
        pathInfo.info(path);
        pathInfo.info(path.toAbsolutePath());
        pathInfo.info(path.toAbsolutePath().getParent());
        try {
            Path realPath = Paths.get("E:", "我的", "user.sql");
            pathInfo.info(realPath.toRealPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        URI uri = path.toUri();
        System.out.println("URI: " + uri);
        Path path1 = Paths.get(uri);
        System.out.println(Files.exists(path1));
        try {
            System.out.println(Files.probeContentType(path1));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class PathInfo {

    private void show(String id, Object info) {
        System.out.println(id + ": " + info);
    }

    public void info(Path path) {
        show("toString", path);
        show("exists", Files.exists(path));
        show("regularFile", Files.isRegularFile(path));
        show("directory", Files.isDirectory(path));
        show("absolute", path.isAbsolute());
        show("fileName", path.getFileName());
        show("parent", path.getParent());
        show("Root", path.getRoot());
        System.out.println("-------------------分界线--------------------");
    }
}