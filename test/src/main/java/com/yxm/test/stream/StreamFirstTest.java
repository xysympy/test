package com.yxm.test.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.stream.Stream;

/**
 * @author yxm
 * @description: 假如你要随机展示 5 至 20 之间不重复的整数并进行排序(7个数字)
 * @date 2022/3/24 15:16
 */
public class StreamFirstTest {
    private static int x = 1;
    public static void main(String[] args) {
        // 添加种子是为了让之后测试的随机数都是一样的
        new Random(47)
                .ints(5, 20)
                .distinct()
                .limit(7)
                .sorted()
                .forEach(System.out::println);
        System.out.printf("123");
        System.out.println();
        /*try {
            Files.readAllLines(Paths.get(""));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        // 斐波那契数列生成
        // stream.iter方法第一个参数是种子，作为传递个后面第二个参数，然后将返回值存储到种子，重复
        Stream.iterate(0, i -> {
            // x记录的是前一个的值，result作为当前的值
            // 方法结束后就是x为x1，result为x2作为往后传的值
           int result = x + i;
           x = i;
           return result;
        });
        Stream.Builder<Object> add = Stream.builder().add("1");
        add.build().map(x -> x + " ").map(x -> x + "?").forEach(System.out::print);
        // add.add("2"); 会报错
    }
}
