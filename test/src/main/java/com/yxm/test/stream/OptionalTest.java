package com.yxm.test.stream;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.ObjIntConsumer;
import java.util.stream.Collectors;

/**
 * @author yxm
 * @description: 测试使用optional
 * @date 2022/3/25 9:46
 */
public class OptionalTest {
    public static void main(String[] args) {
        Optional<String> optional = Optional.empty();
        System.out.println(optional);
        optional = Optional.of("null");
        System.out.println(optional);
        // optional.of必须不为null，不然就会抛出异常
        // optional = Optional.of(null);
        System.out.println(optional);
        optional = Optional.ofNullable(null);
        System.out.println(optional);
        optional = Optional.ofNullable("null");
        System.out.println(optional);
        optional.orElse("1111");
        System.out.println(optional);
        // 和上面的ofElse的区别就是前者是返回一个Object对象，后者是一个Supplier对象
        optional.orElseGet(() -> {return "11111";});
        System.out.println(optional);
        // 测试map和flatmap：下面的输出可以看到差别，map更多的是使用optional包装里面的转换后的元素
        // 但是flatmap则是直接返回，并没有进行包装处理
        System.out.println("---------------------map/flatmap---------------------");
        Optional<String> optionalS = Optional.of("123");
        // 输出：Optional[Optional[22222]]
        System.out.println(optionalS.map(s -> Optional.of("22222")));
        // optionalS = Optional.of("123");
        // 输出Optional[1231111]
        System.out.println(optionalS.map(s -> s + "1111"));
        // optionalS = Optional.of("123");
        // Optional[333333]
        System.out.println(optionalS.flatMap(s -> Optional.of("333333")));
        // 测试使用.collect()方法
        System.out.println("------------------------collect--------------------------");
        int[] ints = new Random(14).ints(0, 30).limit(14).toArray();
        System.out.println(Arrays.toString(ints));
        System.out.println(Arrays.stream(ints).collect(Option::new, Option::new, Option::add));
    }
}
class Option {
    private int value;

    public Option(int value) {
        this.value = value;
    }
    public Option() {
    }

    public Option(Option option, int i) {
        option.value = i;
    }

    public void add(Option option) {
        this.value = option.value;
    }

    @Override
    public String toString() {
        return "Option{" +
                "value=" + value +
                '}';
    }
}