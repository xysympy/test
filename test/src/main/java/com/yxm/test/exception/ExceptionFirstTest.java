package com.yxm.test.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author yxm
 * @description: 测试Exception的使用
 * 实际上Java的异常分为error和Exception
 * error：就是程序没有办法处理，一般正常的程序都不应该出现的错误，一般是jvm出现了问题
 * Exception：
 *      RuntimeException：Java 虚拟机正常运行的时候可能会出现的错误
 *      CheckedException：受检查的异常，需要被显式捕获或者是显式声明
 * 对于throw和throws，我认为Throws是一个声明，也就是表示当前方法会出现这个异常
 * 但是仅仅只是声明并没有用，因为没有实例，那么就没有办法捕获到这个声明对应的异常
 * 实例，所以，我认为throw是类似于实例化，让你的捕获得到对象的那种，throws则是
 * 声明，就像声明一个变量那样，实际上是没有对应的值的。所以，一般是两者结合着使用的
 * @date 2022/3/25 14:20
 */
public class ExceptionFirstTest {

    private static Logger log = LoggerFactory.getLogger(ExceptionFirstTest.class);

    private void test(String str) {
        if ("null".equals(str)) {
            throw new NullPointerException("input null");
        }
    }

    private void printExceptionInfo(Exception e) {
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        log.info(stringWriter.toString());
    }

    public static void main(String[] args) {
        /*try {
            new ExceptionFirstTest().test("null");
        } catch (Exception e) {
            // 使用日志来记录异常输出
            // new ExceptionFirstTest().printExceptionInfo(e);
            throw e;
        }*/
        /*System.out.println("---------------------");
        try {
            new ExceptionFirstTest().test("null");
        } catch (NullPointerException e) {
            // 使用日志来记录异常输出
            // new ExceptionFirstTest().printExceptionInfo(e);
            // 这个方式是将异常的定位变成这里，而不是定位到原来的抛出异常的地方
            // 受检查异常必须声明出来，但是RuntimeException可以不用
            throw (NullPointerException) e.fillInStackTrace();
        }*/
        /*System.out.println("initCause");
        try {
            new ExceptionFirstTest().test("null");
        } catch (NullPointerException e) {
            // 使用日志来记录异常输出
            // new ExceptionFirstTest().printExceptionInfo(e);
            // 类似于抛出了自定义的异常，而原来的异常也没有丢失
            NumberFormatException numberFormatException = new NumberFormatException();
            numberFormatException.initCause(e);
            throw numberFormatException;
        }*/
        // 关于使用finally导致的异常抛出消失
        System.out.println("finally");
        ExceptionFirstTest exceptionFirstTest = new ExceptionFirstTest();
        try {
            // 如下，你就会发现，前面的f1方法抛出的异常会被后面的f2方法抛出的异常覆盖
            // 最后的打印就是打印出f2方法对应抛出的异常
            try {
                exceptionFirstTest.f1();
            } finally {
                exceptionFirstTest.f2();
            }
        } catch (Exception e) {
            exceptionFirstTest.printExceptionInfo(e);
        }
    }

    private void f1() {
        throw new NumberFormatException();
    }

    private void f2() {
        throw new NullPointerException();
    }
}
