package com.yxm.test.exception;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * @author yxm
 * @description: 测试使用try-catch-resources
 * 这个只是帮你调用了close方法，但是异常的捕获还是得自己来
 * @date 2022/3/26 14:09
 */
public class AutoCloseableTest {
    public static void main(String[] args) {
        // 只有实现了AutoCloseable接口的类才能这样
        try (MyAutoClose1 close1 = new MyAutoClose1();
             MyAutoClose2 close2 = new MyAutoClose2()
            // 关闭的顺序就是先关闭close2然后再是close1的，因为防止close2可能会依赖于close1
        ){
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (Stream<String> in = Files.lines(Paths.get("AutoCloseableTest.java"))){
            in.skip(19).limit(1).map(String::toLowerCase)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class MyAutoClose implements AutoCloseable {

    public MyAutoClose() throws Exception {
        System.out.println("MyAutoClose Implements AutoCloseable");
    }

    @Override
    public void close() {
        // 可以在实现这个接口的时候选择不声明异常，这样就不用进行catch了，当然你可以选择
        // catch，只是编译器不再强制而已
        System.out.println("MyAutoClose----close");
    }
}

class MyAutoClose1 extends MyAutoClose{
    public MyAutoClose1() throws Exception {
    }
}
class MyAutoClose2 extends MyAutoClose{
    public MyAutoClose2() throws Exception {
    }
}