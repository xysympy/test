package com.yxm.test.exception;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author yxm
 * @description: 将被检查异常转换为运行时异常
 * @date 2022/3/26 15:31
 */
public class CheckedToRuntimeException {

    public static void main(String[] args) {
        WrappedException wrappedException = new WrappedException();
        for (int i = 0; i < 4; i++) {
            try {
                wrappedException.throwRunTimeException(i);
            } catch (RuntimeException e) {
                try {
                    // 这个就是将包装在RuntimeException里面的异常提取出来了
                    throw e.getCause();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }
    }
}


class WrappedException {

    public void throwRunTimeException(int type) {
        // 方法的作用就算是将检查异常包装成运行时异常
        // 这么做的原因大概就是检查异常是必须捕获的，但是运行异常则不用
        try {
            switch (type) {
                case 0:
                    throw new FileNotFoundException();
                case 1:
                    throw new IOException();
                case 2:
                    throw new RuntimeException();
                default: return;
            }
        } catch (RuntimeException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}