package com.yxm.test.thread;

import java.io.IOException;

/**
 * @author yxm
 * @description: 异步测试
 * @date 2022/6/1 14:34
 */
public class AsyncTest {
    public static void main(String[] args) throws IOException {
        System.out.println(test());
        // 会先打印出test方法的return的值，然后直接main方法执行完成
        // test方法里面的线程开启的还是会继续执行
        // 实际上不确定的，可能是下面的main方法执行完成先，也可能是test方法
        // 里面的线程开启的内容执行完成先
        System.out.println("finish");
    }

    private static int test() {
        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(i);
            }
        }).start();
        return 101;
    }
}
