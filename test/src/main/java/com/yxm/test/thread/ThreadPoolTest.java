package com.yxm.test.thread;

import cn.hutool.core.thread.ThreadFactoryBuilder;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.*;

/**
 * @author yxm
 * @description: 测试下使用线程池
 * @date 2021/11/6 11:30
 */
public class ThreadPoolTest {
    public static void main(String[] args) throws Exception {        //用于获取到本java进程，进而获取总线程数
        RuntimeMXBean runtimeBean = ManagementFactory.getRuntimeMXBean();
        String jvmName = runtimeBean.getName();
        System.out.println("JVM Name = " + jvmName);
        long pid = Long.valueOf(jvmName.split("@")[0]);
        System.out.println("JVM PID  = " + pid);
        ThreadMXBean bean = ManagementFactory.getThreadMXBean();
        int n = 1000;
        for (int i = 0; i < n; i++) {
            ThreadPoolExecutor executor = new ThreadPoolExecutor(10, 20, 1000, TimeUnit.SECONDS, new LinkedBlockingDeque<>());
            for (int j = 0; j < 5; j++) {
                executor.execute(() -> {
                    System.out.println("当前线程总数为：" + bean.getThreadCount());
                });
            }
            // executor.shutdown();
        }
        Thread.sleep(10000);
        System.out.println("线程总数为 = " + bean.getThreadCount());
        ThreadFactory factory = ThreadFactoryBuilder.create().setNamePrefix("Thread-Pool-Test-").build();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(10, 20, 0L, TimeUnit.SECONDS
                , new LinkedBlockingQueue<>(), factory);
        ThreadPoolTest test = new ThreadPoolTest();
        for (int i = 0; i < 20; i++) {
            final int temp = i;
            executor.execute(() -> test.poolTest(temp));
        }
        executor.shutdown();
    }

    void poolTest(int i) {
        if (i == 1) {
            int j = 1 / 0;
        }
        System.out.println(i);
    }
}
