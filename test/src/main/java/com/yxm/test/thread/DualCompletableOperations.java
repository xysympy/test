package com.yxm.test.thread;// concurrent/DualCompletableOperations.java

import java.util.concurrent.CompletableFuture;

public class DualCompletableOperations {
    static CompletableFuture<Workable> cfA, cfB;

    static void init() {
        cfA = Workable.make("A", 0.15);
        cfB = Workable.make("B", 0.10); // Always wins
    }

    static void join() {
        cfA.join();
        cfB.join();
        System.out.println("*****************");
    }

    public static void main(String[] args) {
        init();
        CompletableOperations.CompletableUtilities.voidr(cfA.runAfterEitherAsync(cfB, () ->
                System.out.println("runAfterEither")));
        join();
        init();
        CompletableOperations.CompletableUtilities.voidr(cfA.runAfterBothAsync(cfB, () ->
                System.out.println("runAfterBoth")));
        join();
        init();
        CompletableOperations.CompletableUtilities.showr(cfA.applyToEitherAsync(cfB, w -> {
            System.out.println("applyToEither: " + w);
            return w;
        }));
        join();
        init();
        CompletableOperations.CompletableUtilities.voidr(cfA.acceptEitherAsync(cfB, w -> {
            System.out.println("acceptEither: " + w);
        }));
        join();
        init();
        CompletableOperations.CompletableUtilities.voidr(cfA.thenAcceptBothAsync(cfB, (w1, w2) -> {
            System.out.println("thenAcceptBoth: "
                    + w1 + ", " + w2);
        }));
        join();
        init();
        CompletableOperations.CompletableUtilities.showr(cfA.thenCombineAsync(cfB, (w1, w2) -> {
            System.out.println("thenCombine: "
                    + w1 + ", " + w2);
            return w1;
        }));
        join();
        init();
        CompletableFuture<Workable>
                cfC = Workable.make("C", 0.08),
                cfD = Workable.make("D", 0.09);
        CompletableFuture.anyOf(cfA, cfB, cfC, cfD)
                .thenRunAsync(() ->
                        System.out.println("anyOf"));
        join();
        init();
        cfC = Workable.make("C", 0.08);
        cfD = Workable.make("D", 0.09);
        CompletableFuture.allOf(cfA, cfB, cfC, cfD)
                .thenRunAsync(() ->
                        System.out.println("allOf"));
        join();
    }

     static class Workable {
        String id;
        final double duration;

        public Workable(String id, double duration) {
            this.id = id;
            this.duration = duration;
        }

        @Override
        public String toString() {
            return "Workable[" + id + "]";
        }

        public static void test(){

        }
        public static Workable work(Workable tt) {
            new Nap(tt.duration); // Seconds
            tt.id = tt.id + "W";
            System.out.println(tt);
            return tt;
        }

        public static CompletableFuture<Workable> make(String id, double duration) {
            return CompletableFuture
                    .completedFuture(
                            new Workable(id, duration)
                    )
                    .thenApplyAsync(Workable::work);
        }
    }
}