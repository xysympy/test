package com.yxm.test.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

/**
 * 单线程执行器
 *
 * @author Administrator
 * @date 2022/05/10
 */
public class SingleThreadExecutor {
    public static void main(String[] args) {
        // 这个SingleThreadExecutor是单线程执行的，也就是你交给这个线程池的任务它都是一个一个
        // 执行的，所以即使是个线程不安全的，交给它执行，也是没有问题的
        ExecutorService exec = Executors.newSingleThreadExecutor();
        IntStream.range(0, 10)
                .mapToObj(NapTask::new)
                .forEach(exec::execute);
        System.out.println("All tasks submitted");
        exec.shutdown();
        // 测试线程池是否还存活
        // shutdown方法会让线程池中的任务都完成时才会关闭线程池
        // shutdownNow方法则会让线程池立即停止，不管任务还在不在运行
        // 当然，两个方法都不会再允许提交新的任务了
        while (!exec.isTerminated()) {
            System.out.println(
                    Thread.currentThread().getName() +
                            " awaiting termination");
            new Nap(0.1);
        }

        // SingleThreadExecutor的好处
        exec = Executors.newSingleThreadExecutor();
        IntStream.range(0, 10)
                .mapToObj(InterferingTask::new)
                .forEach(exec::execute);
        exec.shutdown();
        System.out.println(LongStream.rangeClosed(3, 3).reduce(1, Long::sum));
    }
}

class InterferingTask implements Runnable {
    final int id;
    private static Integer val = 0;

    public InterferingTask(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++)
            val++;
        System.out.println(id + " " +
                Thread.currentThread().getName() + " " + val);
    }
}