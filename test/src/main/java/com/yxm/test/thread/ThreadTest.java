package com.yxm.test.thread;

import lombok.SneakyThrows;

/**
 * @Description: 线程测试
 * @Author yxm
 * @Date 2021/10/28 10:01
 */
public class ThreadTest {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread() + "线程开始运行");
        new MyThread().start();
        MyThreadTest myThreadTest = new MyThreadTest();
        myThreadTest.setDaemon(true);
        myThreadTest.start();
        System.out.println(Thread.currentThread() + "线程运行结束");
    }
}

class MyThread extends Thread{
    @SneakyThrows
    @Override
    public void run() {
        System.out.println(Thread.currentThread() + "线程开始运行");
        Thread.sleep(5000);
        System.out.println(Thread.currentThread() + "线程运行结束");
    }
}
class MyThreadTest extends Thread{
    @SneakyThrows
    @Override
    public void run() {
        System.out.println(Thread.currentThread() + "线程开始运行");
        Thread.sleep(1000);
        System.out.println(Thread.currentThread() + "线程运行结束");
    }
}
