package com.yxm.test.thread;

import java.util.concurrent.TimeUnit;

/**
 * 午睡
 *
 * @author yxm
 * @date 2022/05/10
 */
public class Nap {
    public Nap(double t) { // Seconds
        try {
            TimeUnit.MILLISECONDS.sleep((int)(1000 * t));
        } catch(InterruptedException e){
            throw new RuntimeException(e);
        }
    }
    public Nap(double t, String msg) {
        this(t);
        System.out.println(msg);
    }
}