package com.yxm.test.thread;

import cn.hutool.core.thread.ThreadFactoryBuilder;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author yxm
 * @description: 静态变量线程池
 * @date 2022/5/20 13:18
 */
public class StaticVariableThreadPool {
    public static void main(String[] args) {
        QuartzJob job1 = new QuartzJob();
        job1.init();
        job1.exec();
        QuartzJob job2 = new QuartzJob();
        job2.init();
        job2.exec();
    }
}

class QuartzJob {
    private static ThreadPoolExecutor threadPoolExecutor;

    public QuartzJob() {
    }

    public void init() {
        if (threadPoolExecutor != null) {
            return;
        }
        ThreadFactory factory = ThreadFactoryBuilder.create()
                .setNamePrefix("Quartz-Test-Job-Thread-").build();
        threadPoolExecutor = new ThreadPoolExecutor(100, 100, 0L, TimeUnit.SECONDS
                , new LinkedBlockingQueue<>(), factory);
    }

    public void exec() {
        for (int i = 0; i < 100; i++) {
            final int temp = i;
            threadPoolExecutor.execute(() -> System.out.println(Thread.currentThread().getName() + ": " + temp));
        }
    }
}
