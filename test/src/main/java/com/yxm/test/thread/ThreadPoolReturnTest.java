package com.yxm.test.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author yxm
 * @description: 线程池结合return
 * @date 2022/6/24 14:25
 */
public class ThreadPoolReturnTest {

    private final static ExecutorService poolExecutor = Executors.newCachedThreadPool();

    public static void main(String[] args) {
        System.out.println(test());
        System.out.println(test2());
        // 类似于异步的效果，也就是会先return出来，然后线程池里面的还是会继续处理的，除非是线程池强制关闭了
        System.out.println("main");
    }

    private static String test() {
        for (int i = 0; i < 100; i++) {
            final int temp = i;
            poolExecutor.execute(() -> {
                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + temp);
            });
        }
        return "test";
    }

    private static String test2() {
        for (int i = 0; i < 100; i++) {
            final int temp = i;
            new Thread(() -> {
                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + temp);
            }).start();
        }
        return "test2";
    }

}
