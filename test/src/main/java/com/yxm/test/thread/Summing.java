package com.yxm.test.thread;

import java.util.Arrays;
import java.util.function.LongSupplier;
import java.util.stream.LongStream;

/**
 * @author yxm
 * @description: 求和
 * @date 2022/5/10 14:44
 */
public class Summing {
    private static void timeTest(String id, long checkValue, LongSupplier longSupplier) {
        System.out.println("id: " + id);
        long start = System.currentTimeMillis();
        long result = longSupplier.getAsLong();
        long end = System.currentTimeMillis();
        if (result == checkValue) {
            System.out.println("time: " + (end - start));
        } else {
            System.out.format("result: %d%ncheckValue: %d%n", result, checkValue);
        }
    }

    private static final int SZ = 100_000_000;
    private static final long CHECK_VALUE = SZ * ((long) SZ + 1) / 2;

    public static void main(String[] args) {
        //测试(使用迭代器或者是range的形式来处理)
        timeTest("sum stream (range)", CHECK_VALUE, () -> LongStream.rangeClosed(0, SZ)
                .sum());
        timeTest("sum parallel stream (range)", CHECK_VALUE, () -> LongStream.rangeClosed(0, SZ)
                .parallel().sum());
        timeTest("sum stream (iterator)", CHECK_VALUE, () -> LongStream.iterate(0, i -> i + 1)
                .limit(SZ + 1).sum());
        timeTest("sum parallel stream (iterator)", CHECK_VALUE, () -> LongStream.iterate(0, i -> i + 1)
                .parallel().limit(SZ + 1).sum());
        System.out.println(LongStream.iterate(1, i -> i + 1)
                .parallel().limit(SZ + 1).min());
        // 尝试使用数组的形式
        long[] la = new long[SZ + 1];
        // [0, SZ]
        Arrays.parallelSetAll(la, i -> i);
        Summing.timeTest("Array Stream Sum", CHECK_VALUE, () ->
                Arrays.stream(la).sum());
        Summing.timeTest("Parallel", CHECK_VALUE, () ->
                Arrays.stream(la).parallel().sum());
        // Destructive summation:
        Summing.timeTest("Basic Sum", CHECK_VALUE, () ->
                basicSum(la));
        Summing.timeTest("parallelPrefix", CHECK_VALUE, () -> {
            Arrays.parallelPrefix(la, Long::sum);
            return la[la.length - 1];
        });
        System.out.println(la[la.length - 1]);
    }

    private static long basicSum(long[] la) {
        long sum = 0;
        for (long l : la) {
            sum += l;
        }
        return sum;
    }
}
