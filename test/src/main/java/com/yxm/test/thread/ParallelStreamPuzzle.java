package com.yxm.test.thread;

import java.io.IOException;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 平行流难题
 *
 * @author yxm
 * @date 2022/05/10
 */
public class ParallelStreamPuzzle {
    /**
     * int发电机
     *
     * @author Administrator
     * @date 2022/05/10
     */
    static class IntGenerator implements Supplier<Integer> {
        private int current = 0;

        @Override
        public Integer get() {
            return current++;
        }
    }

    public static void main(String[] args) throws IOException {
        List<Integer> x = Stream.generate(new IntGenerator())
                .limit(10)
                .parallel()  // [1]
                .collect(Collectors.toList());
        // 这里的运行结果并不像我们想象中的那样，0-9，去掉那个parallel可以，但是加上就是不行了
        /**
         * 包括后面的{@link ParallelStreamPuzzle2}也是不行的
         */
        System.out.println(x);
        // 最好的就是直接使用intStream
        List<Integer> collect = IntStream.range(0, 30)
                .limit(10)
                .boxed()
                .collect(Collectors.toList());
        System.out.println(collect);
    }
}