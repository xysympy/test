package com.yxm.test.thread;

/**
 * @author yxm
 * @description: 测试使用final关键字
 * @date 2022/5/27 11:28
 */
public class FinalTest {
    private static FinalTestClass finalTestClass;

    public static void main(String[] args) {
        /*new Thread(() -> finalTestClass = new FinalTestClass()).start();
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                if (finalTestClass != null) {
                    System.out.println(finalTestClass.obj.x == 0);
                }
            }).start();
        }*/
        new FinalTestClass();
    }
}

class FinalTestClass {
    public int x;
    public int y;
    public FinalTestClass obj;

    public FinalTestClass() {
        // this逸出代码
        // 指的是在还没有初始化完成实例的时候，就将this供给其它线程调用，这个时候可能会出现成员变量的数据问题
        obj = this;
        for (int i = 0; i < 100; i++) {

        }
        //x = 2;
        y = 1;
    }
}