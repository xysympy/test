package com.yxm.test.thread;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author yxm
 * @description: 测试使用Future
 * @date 2021/11/5 16:56
 */
public class FutureTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Instant startTime = Instant.now();
        /*// 模拟的问题是网购厨具，同时买菜做饭
        // 首先使用多线程，看看能不能够做到
        OnShopping onShopping = new OnShopping();
        // 开启线程
        onShopping.start();
        // 防止买菜在厨具购买之前，所以需要确保onShopping线程先运行
        onShopping.join();

        //购买食物
        System.out.println("去超市");
        System.out.println("购买中");
        Thread.sleep(2000);
        System.out.println("购买完成");
        System.out.println("做饭");
        System.out.println("总共耗时：" + Duration.between(startTime, Instant.now()).toMillis());*/
        // 显然的，这个就没有等厨具的同时去买菜的感觉，感觉不合理

        // 使用Future修改逻辑
        Callable<Object> callable = new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("下单购买，支付完成");
                System.out.println("开始快递");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("快递到达");
                return null;
            }
        };
        // 将这个线程添加到Future中
        FutureTask<Object> futureTask = new FutureTask<>(callable);
        // 开启异步
        new Thread(futureTask).start();
        //购买食物
        System.out.println("去超市");
        System.out.println("购买中");
        Thread.sleep(2000);
        System.out.println("购买完成");
        // 是否完成异步的任务
        if(!futureTask.isDone()){
            System.out.println("厨具还未到，不做饭");
        }
        // get方法会阻塞，直到上面的异步任务完成
        Object o = futureTask.get();
        System.out.println("做饭");
        System.out.println("总共耗时：" + Duration.between(startTime, Instant.now()).toMillis());
    }
}

class OnShopping extends Thread{
    @Override
    public void run() {
        System.out.println("下单购买，支付完成");
        System.out.println("开始快递");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("快递到达");
    }
}
