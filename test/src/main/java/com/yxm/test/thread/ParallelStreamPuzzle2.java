package com.yxm.test.thread;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 平行流puzzle2
 *
 * @author yxm
 * @date 2022/05/10
 */
public class ParallelStreamPuzzle2 {
    public static final Deque<String> TRACE =
            new ConcurrentLinkedDeque<>();

    static class IntGenerator implements Supplier<Integer> {
        private final AtomicInteger current = new AtomicInteger();

        @Override
        public Integer get() {
            TRACE.add(current.get() + ": " + Thread.currentThread().getName());
            return current.getAndIncrement();
        }

        public AtomicInteger getCurrent() {
            return current;
        }
    }

    public static void main(String[] args) throws Exception {
        IntGenerator intGenerator = new IntGenerator();
        List<Integer> x = Stream.generate(intGenerator)
                .limit(10)
                .parallel()
                .collect(Collectors.toList());
        System.out.println(x);
        Files.write(Paths.get("PSP2.txt"), TRACE);
        System.out.println(intGenerator.getCurrent().get());
    }
}