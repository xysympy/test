package com.yxm.test.thread;

/**
 * 午睡任务
 *
 * @author yxm
 * @date 2022/05/10
 */
public class NapTask implements Runnable {
    final int id;
    public NapTask(int id) {
        this.id = id;
        }
    @Override
    public void run() {
        // Seconds
        new Nap(0.1);
        System.out.println(this + " "+
            Thread.currentThread().getName());
        }
    @Override
    public String toString() {
        return"NapTask[" + id + "]";
    }
}