package com.yxm.test.thread;

import cn.hutool.core.thread.ThreadFactoryBuilder;

import java.util.Random;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author yxm
 * @description: 线程池测试使用2
 * @date 2022/4/20 15:53
 */
public class ThreadPollTest2 {

    private final ThreadPoolExecutor threadPoolExecutor;

    public ThreadPollTest2() {
        int coreSize = Math.max(1, Runtime.getRuntime().availableProcessors()) * 2;
        this.threadPoolExecutor = new ThreadPoolExecutor(coreSize, coreSize, 0L, TimeUnit.SECONDS
                , new LinkedBlockingQueue<>(), ThreadFactoryBuilder.create().setNamePrefix("thread-test-").build());
    }

    public static void main(String[] args) {
        ThreadPollTest2 threadPollTest2 = new ThreadPollTest2();
        for (int i = 1; i <= 100; i++) {
            System.out.println(i + "准备进入线程池");
            threadPollTest2.poolExecute(new Random().nextInt(i));
            System.out.println(i + "线程池出来了");
        }
    }

    public void poolExecute(int nums) {
        for (int i = 0; i < nums; i++) {
            final int index = i;
            this.threadPoolExecutor.execute(() -> {
                System.out.println(Thread.currentThread().getName() + "测试输出" + index);
            });
        }
    }
}
