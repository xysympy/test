package com.yxm.test.thread;

import java.util.concurrent.TimeUnit;

/**
 * @author yxm
 * @description: 测试synchronized是否有可见性
 * @date 2022/5/26 9:31
 */
public class SynchronizedTest {

    private int count = 0;

    public static void main(String[] args) throws InterruptedException {
        SynchronizedTest test = new SynchronizedTest();
       /* for (int i = 0; i < 1000; i++) {
            // 所以个人觉得synchronized是能够保证共享变量的原子性的
            // 会在synchronized结束的时候将数据刷会主内存
            // synchronized不能保证线程不被中断，也就是即使用了synchronized
            // 也只是保证它同一时间只有一个线程执行，或者说拿到这个锁，但是如果此时
            // 线程中断，cpu去调度另一个线程，这个synchronized是没有办法的
            // 所以在DCL的单例模式中voliate关键字就是为了禁止编译优化，保证你拿到instance不为null的的时候
            // 它对应的内存地址是有对象的
            new Thread(() -> test.add()).start();
            new Thread(() -> System.out.println(test.getCount())).start();
        }
        TimeUnit.SECONDS.sleep(1L);
        System.out.println("--------------------------------");
        System.out.println(test.getCount());*/

        // 这里主要是为了证明，synchronized是能保证可见性的，但是管程规则不行，也就是Happen-Before
        // 但是synchronized不能保证中途的线程切换导致你还没有加完，就被读取数据了，而数据是老的数据
        new Thread(() -> {
            try {
                test.add();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("thread add " + test.getCount());
        }).start();
        TimeUnit.MILLISECONDS.sleep(1);
        new Thread(() -> System.out.println("thread get " + test.getCount())).start();
    }

    public synchronized void add() throws InterruptedException {
        count += 1;
    }

    public int getCount() {
        return count;
    }
}