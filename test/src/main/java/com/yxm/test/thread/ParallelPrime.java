package com.yxm.test.thread;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Timer;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * 平行'
 *
 * @author yxm
 * @description: 判断一个数字是否为质数
 * @date 2022/5/10 14:21
 */
public class ParallelPrime {

    private static boolean isPrime(long n) {
        return LongStream.rangeClosed(2, (long) Math.sqrt(n)).noneMatch(i -> n % i == 0);
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        List<String> primes = LongStream.iterate(2, i -> i + 1)
                // 注释掉下面的这个parallel之后，发现速度还是会慢点，大概相差三倍
                .parallel()
                .filter(ParallelPrime::isPrime)
                .limit(100_000)
                .mapToObj(Long::toString)
                .collect(Collectors.toList());
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        System.out.println(primes);
    }
}
