package com.yxm.test.enums;

import java.util.Random;

/**
 * @author yxm
 * @description: 随机输出枚举类的值
 * @date 2022/4/27 16:42
 */
public class Enums {
    /**
     * 这个之所以不能像下面的random那样，是因为这个必须是一个枚举类
     * 不然会报错
     *
     * @param clz clz
     * @return {@link T}
     */
    public static <T extends Enum<T>> T random(Class<T> clz) {
        return random(clz.getEnumConstants());
    }

    /**
     * 这个方法只是一个简单的对数组进行随机
     *
     * @param values 值
     * @return {@link T}
     */
    public static <T> T random(T[] values) {
        return values[new Random().nextInt(values.length)];
    }

    public static void main(String[] args) {
        // random(Enums.class);
        for (int i = 0; i < 20; i++) {
            System.out.print(random(Activity.class) + "\t");
        }
    }
}

enum Activity { SITTING, LYING, STANDING, HOPPING,
    RUNNING, DODGING, JUMPING, FALLING, FLYING }
