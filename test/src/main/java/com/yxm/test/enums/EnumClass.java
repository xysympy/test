package com.yxm.test.enums;

/**
 * @author yxm
 * @description: 测试枚举类的基本使用
 * @date 2022/4/27 13:55
 */
public class EnumClass {
    public static void main(String[] args) {
        for (Shrubby value : Shrubby.values()) {
            // ordinal 就是顺序问题
            System.out.println("ordinal 方法" + value.ordinal());
            // 实际上枚举类是默认的继承了一个Enum的类，而这个类是实现了Comparable接口和Serializable
            System.out.println("compareTo 方法" + value.compareTo(Shrubby.CRAWLING));
            // == 是使用equals和hashCode进行比较的
            System.out.println(value == Shrubby.GROUND);
            // 直接输出的是枚举的名字
            System.out.println(value.name());
            // 枚举类
            System.out.println(value.getDeclaringClass());
            // 使用valueOf方法时，如果是非枚举类里面的枚举会报错
            // System.out.println(Shrubby.valueOf("test"));
            // System.out.println(Enum.valueOf(Shrubby.class, "test"));
            System.out.println("==========================================");
        }
    }
}

enum Shrubby {
    GROUND, CRAWLING, HANGING
}
