package com.yxm.test.enums;

/**
 * @author yxm
 * @description: 在enum枚举类中使用main方法
 *               也就是你可以把枚举类当做一个普通的类
 * @date 2022/4/27 14:04
 */
public enum OzWitch {
    WEST("Miss Gulch, aka the Wicked Witch of the West"),
    NORTH("Glinda, the Good Witch of the North"),
    EAST("Wicked Witch of the East, wearer of the Ruby " +
            "Slippers, crushed by Dorothy's house"),
    SOUTH("Good by inference, but missing");

    private final String desc;

    OzWitch(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public static void main(String[] args) {
        for (OzWitch ozWitch : OzWitch.values()) {
            // toString方法打印出来的和枚举的name方法差不多
            // 所以实际上你可以重写toString方法
            System.out.println(ozWitch);
            System.out.println(ozWitch.getDesc());
            System.out.println("================");
        }
    }
}
