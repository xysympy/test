package com.yxm.test.enums;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author yxm
 * @description: 测试使用EnumMap
 * @date 2022/4/29 14:58
 */
public class EnumMapTest {
    public static void main(String[] args) {
        // enumMap实际上和enumSet类似，都是在创建的时候就已经确定了枚举的大小了，所以所有的枚举都是作为key值存在里面了
        // EnumMap里面实现是数组实现的，因为是enum，大小已经确定了，而且不需要类似于hashmap的机制来存储
        // 没有必要，因为不会重复 enum.ordinal()
        EnumMap<ActualCommand, Command> enumMap = new EnumMap<>(ActualCommand.class);
        enumMap.put(ActualCommand.GO, () -> System.out.println("go"));
        enumMap.put(ActualCommand.STOP, () -> System.out.println("stop"));
        // 测试下put会不会覆盖
        enumMap.put(ActualCommand.GO, () -> System.out.println("go 2"));
        for (Map.Entry<ActualCommand, Command> commandEntry : enumMap.entrySet()) {
            System.out.println(commandEntry.getKey());
            commandEntry.getValue().action();
            System.out.println("============================");
        }
    }
}

enum ActualCommand {
    GO, STOP, SIT;
}

interface Command {
    /**
     * 行动
     */
    public void action();
}