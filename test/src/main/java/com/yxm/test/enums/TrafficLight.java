package com.yxm.test.enums;

/**
 * @author yxm
 * @description: 枚举类来使用Switch语句，来实现一个简单的红绿灯的变色
 * @date 2022/4/27 14:13
 */
public class TrafficLight {

    Signal color = Signal.RED;

    private void change() {
        // 这里可以看到编译器没有叫你添加default，因为case已经把所有的情况都给覆盖了
        switch (color) {
            case RED:
                color = Signal.GREEN;
                break;
            case GREEN:
                color = Signal.YELLOW;
                break;
            case YELLOW:
                color = Signal.RED;
                break;
        }
    }

    @Override
    public String toString() {
        return "TrafficLight{" +
                "color=" + color +
                '}';
    }

    public static void main(String[] args) {
        TrafficLight t = new TrafficLight();
        for (int i = 0; i < 7; i++) {
            System.out.println(t);
            t.change();
        }
    }
}

enum Signal {
    RED, GREEN, YELLOW;
}