package com.yxm.test.enums;

import java.util.EnumSet;

/**
 * @author yxm
 * @description: EnumSet类测试使用
 * @date 2022/4/27 17:09
 */
public class EnumSetTest {

    public static void main(String[] args) {
        // 常规的存储长度是64个元素，也就是long类型的位数
        EnumSet<AlarmPoints> enumSet = EnumSet.noneOf(AlarmPoints.class);
        System.out.println(enumSet);
        enumSet.add(AlarmPoints.OFFICE1);
        System.out.println(enumSet);
        enumSet.addAll(EnumSet.of(AlarmPoints.OFFICE2, AlarmPoints.OFFICE3, AlarmPoints.OFFICE4));
        System.out.println(enumSet);
        enumSet = EnumSet.allOf(AlarmPoints.class);
        System.out.println(enumSet);
        enumSet.removeAll(EnumSet.of(AlarmPoints.OFFICE4, AlarmPoints.OFFICE3, AlarmPoints.OFFICE2));
        System.out.println(enumSet);
        // 应该是全部减去enumSet中包含的
        enumSet = EnumSet.complementOf(enumSet);
        System.out.println(enumSet);
        // 这个的存储机制应该是使用long类型存储，但是是使用bit来表示这个枚举存不存在在这个EnumSet中的
        // 也就是并不存实际的数据，感觉更应该是像一种向量，然后根据这个向量能知道它对应的值
        EnumSet<AlarmPoints2> enumSet1 = EnumSet.noneOf(AlarmPoints2.class);
    }

}

enum AlarmPoints {
    STAIR1, STAIR2, LOBBY, OFFICE1, OFFICE2, OFFICE3,
    OFFICE4, BATHROOM, UTILITY, KITCHEN
}

enum AlarmPoints2 {
    A0, A1, A2, A3, A4, A5, A6, A7, A8, A9,
    A10, A11, A12, A13, A14, A15, A16, A17, A18, A19,
    A20, A21, A22, A23, A24, A25, A26, A27, A28, A29,
    A30, A31, A32, A33, A34, A35, A36, A37, A38, A39,
    A40, A41, A42, A43, A44, A45, A46, A47, A48, A49,
    A50, A51, A52, A53, A54, A55, A56, A57, A58, A59,
    A60, A61, A62, A63, A64, A65, A66, A67, A68, A69,
    A70, A71, A72, A73, A74, A75
}