package com.yxm.test.enums;

import javax.management.RuntimeErrorException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author yxm
 * @description: 实际上在Enum类中是没有values方法的，那么为什么enum类就有呢？
 * 使用反射来查看下
 * 实际上是编译器为你加上了values方法，valueOf(xx)也是它加的
 * Enum类是有valueOf(x, x)
 * 请记住，我们写的枚举类实际上是继承了Enum的，所以不能再继承了
 * @date 2022/4/27 14:22
 */
public class Reflection {

    /**
     * 分析
     *
     * @param enumClass 枚举类(Class<?> 是为了表示这个参数是支持泛型的，而不是使用原生的Class对象)
     * @return {@link Set}<{@link String}>
     */
    private Set<String> analyze(Class<?> enumClass) {
        System.out.println("analyze class: " + enumClass);
        System.out.println("实现的接口，使用Class<?>直接输出: ");
        for (Class<?> anInterface : enumClass.getInterfaces()) {
            System.out.println(anInterface);
        }
        System.out.println("实现的接口，使用Type直接输出: ");
        for (Type type : enumClass.getGenericInterfaces()) {
            System.out.println(type);
        }
        System.out.println("实现的父类: " + enumClass.getSuperclass());
        Set<String> methods = new TreeSet<>();
        for (Method method : enumClass.getMethods()) {
            methods.add(method.getName());
        }
        System.out.println("方法: " + methods);
        return methods;
    }

    public static void main(String[] args) {
        Reflection reflection = new Reflection();
        Set<String> exploreAnalyze = reflection.analyze(Explore.class);
        Set<String> enumAnalyze = reflection.analyze(Enum.class);
        System.out.println("explore containsAll enum : " + exploreAnalyze.containsAll(enumAnalyze));
        System.out.println("explore removeAll enum");
        exploreAnalyze.removeAll(enumAnalyze);
        System.out.println(exploreAnalyze);
        OSExecute.command("javap -cp test/target/classes/com/yxm/test/enums Explore");

        // 另一种获取Enum所有枚举的方法
        Enum e =Explore.HERE;
        for (Enum constant : e.getClass().getEnumConstants()) {
            System.out.println(constant);
        }
    }
}

enum Explore {
    HERE, THERE;
}

class OSExecute {

    public static void command(String command) {
        boolean err = false;
        try {
            Process process = new ProcessBuilder(command.split(" ")).start();
            try (
                    BufferedReader results = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    BufferedReader errors = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            ) {
                results.lines().forEach(System.out::println);
                // err = errors.lines().peek(System.out::println).count() > 0;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (err) {
            throw new RuntimeException("error exception");
        }
    }
}