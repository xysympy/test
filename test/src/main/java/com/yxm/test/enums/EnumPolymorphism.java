package com.yxm.test.enums;

import java.util.EnumSet;

/**
 * @author yxm
 * @description: 枚举实现类似于多态的机制
 * @date 2022/4/29 15:23
 */
public class EnumPolymorphism {
    public static void main(String[] args) {
        EnumSet<CarWash> enumSet = EnumSet.of(CarWash.FLUSH, CarWash.FOAM);
        for (CarWash carWash : enumSet) {
            carWash.action();
        }
        enumSet.add(CarWash.FLUSH);
        enumSet.add(CarWash.DRYING);
        for (CarWash carWash : enumSet) {
            carWash.action();
        }
    }
}

/**
 * 大概就是实现一个多态的机制，但是不算是多态
 * 虽然看着CarWash是超类的感觉，然后里面的枚举实例是具体的实现，
 * 但是里面的枚举实例实际上是变量，并不算是class
 * 因为void fn(CarWash.Flush flush)这样的方法是不能被声明的
 *
 * @author yxm
 * @date 2022/04/29
 */
enum CarWash {

    FLUSH {
        @Override
        void action() {
            System.out.println("flush");
        }
    },
    FOAM {
        @Override
        void action() {
            System.out.println("foam");
        }
    },
    DRYING {
        @Override
        void action() {
            System.out.println("drying");
        }
    };


    abstract void action();
    }