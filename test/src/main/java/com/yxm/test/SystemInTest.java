package com.yxm.test;

import java.io.IOException;
import java.util.Scanner;

/**
 * @ClassName: SytemInTest
 * @Description: 测试System.in
 * @Author: yxm
 * @Date: 2022/7/16 11:45
 * @Version: 1.0
 **/
public class SystemInTest {
    public static void main(String[] args) throws IOException {
        // read是读取一个字节
        int input = System.in.read();
        System.out.println(input);
        // 会包含一个回车键，也就是换行符
        int length = System.in.available();
        System.out.println(length);
        if(length > 2) {
            byte[] bts = new byte[length - 1];
            int flag = System.in.read(bts);
            System.out.println(new String(bts));
            if(flag!= -1) {
                byte[] tmp = new byte[bts.length];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = bts[tmp.length - i - 1];
                }
                System.out.println(new String(tmp));
            }
        }
        /*Scanner scanner = new Scanner(System.in);
        scanner.nextLine();*/

    }
}
