package com.yxm.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author yxm
 * @description: 测试SimpleDate类
 * @date 2022/4/6 20:20
 */
public class SimpleDateTest {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        System.out.println(dateFormat.toString());
        new SimpleDateTest().simpleDate(dateFormat);
        System.out.println(dateFormat.toString());
    }

    private void simpleDate(SimpleDateFormat dateFormat) throws ParseException {
        System.out.println(dateFormat.toString());
        String format = dateFormat.format(new Date());
        System.out.println(format);
        dateFormat.applyPattern("yyyy-MM-dd");
        System.out.println(dateFormat.toString());
    }
}