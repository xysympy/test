package com.yxm.test.socket;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;

/**
 * @author yxm
 * @description: socket的服务端
 * @date 2021/12/4 15:34
 */
public class SocketServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(9527);
        serverSocket.setSoTimeout(10000);
        System.out.println("等待远程连接，端口号为：" + serverSocket.getLocalPort() + "...");
        Socket server = serverSocket.accept();
        System.out.println("远程主机地址：" + server.getRemoteSocketAddress());
        DataInputStream in = new DataInputStream(server.getInputStream());
        System.out.println(in.readUTF());
        DataOutputStream out = new DataOutputStream(server.getOutputStream());
        out.writeUTF("谢谢连接我：" + server.getLocalSocketAddress() + "\nGoodbye!");
        server.close();
    }

    /**
     * 服务器
     *
     * @param port       知识产权
     */
    private static void server(String port) throws IOException {
        // 监听指定的端口
        ServerSocket server = new ServerSocket(Integer.parseInt(port));

        // server将一直等待连接的到来
        System.out.println("server将一直等待连接的到来");
        Socket socket = server.accept();
        // 建立好连接后，从socket中获取输入流，并建立缓冲区进行读取
        InputStream inputStream = socket.getInputStream();
        byte[] bytes = new byte[1024];
        int len;
        StringBuilder sb = new StringBuilder();
        while ((len = inputStream.read(bytes)) != -1) {
            //注意指定编码格式，发送方和接收方一定要统一，建议使用UTF-8
            sb.append(new String(bytes, 0, len,"UTF-8"));
        }
        System.out.println("get message from client: " + sb);
        String message="你好  yiwangzhibujian";
        socket.getOutputStream().write(message.getBytes("UTF-8"));
    }
}
