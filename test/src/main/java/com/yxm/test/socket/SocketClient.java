package com.yxm.test.socket;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;

/**
 * @author yxm
 * @description: 测试使用socket(TCP连接)
 * @date 2021/12/4 15:14
 */
public class SocketClient {
    public static void main(String[] args) throws IOException {
       // String serverName = args[0];
        //int port = Integer.parseInt(args[1]);
        try
        {
            //System.out.println("连接到主机：" + serverName + " ，端口号：" + port);
            Socket client = new Socket("10.10.18.55", 9527);
            System.out.println("远程主机地址：" + client.getRemoteSocketAddress());
            OutputStream outToServer = client.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToServer);

            out.writeUTF("Hello from " + client.getLocalSocketAddress());
            InputStream inFromServer = client.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);
            // 可以这样，但是直接用InputStream的read方法，用while循环读取的时候就不行（估计是因为只要对方通道不关，就
            // 不会去读）
            System.out.println("服务器响应： " + in.readUTF());
            client.close();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 客户端
     *
     * @param hostname   要连接到的服务器地址
     * @param port 要连接的服务器地址的端口
     */
    private static void client(String hostname, String port) throws IOException {
        // 要连接的服务端IP地址和端口
        // 与服务端建立连接
        Socket socket = new Socket(hostname, Integer.parseInt(port));
        // 建立连接后获得输出流
        String message="你好  yiwangzhibujian";
        socket.getOutputStream().write(message.getBytes("UTF-8"));
        InputStream inputStream = socket.getInputStream();
        byte[] bytes = new byte[1024];
        int len;
        StringBuilder sb = new StringBuilder();
        while ((len = inputStream.read(bytes)) != -1) {
            //注意指定编码格式，发送方和接收方一定要统一，建议使用UTF-8
            sb.append(new String(bytes, 0, len,"UTF-8"));
        }
        System.out.println("get message from client: " + sb);
    }
}
