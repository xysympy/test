package com.yxm.test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yxm
 * @description: 通过反射绕过泛型
 * @date 2021/11/5 9:36
 */
public class Generic {
    public static void main(String[] args) throws Exception {
        Map<String, String> map = new HashMap<>();
        String key = "key";
        Integer val = new Integer(1); //备注：此方法在Java9后标注为过期了，建议使用valueOf，使用缓存来提高效率
        Method m = HashMap.class.getDeclaredMethod("put", new Class[]{Object.class, Object.class});
        m.invoke(map, key, val);

        System.out.println(map); //{key=1}
        //但是下面的输出会报错
        System.out.println(map.get(key)); // java.lang.ClassCastException: java.lang.Integer cannot be cast to java.lang.String

        List<Integer> list = new ArrayList<>();
        list.add(1);
        //list.add("a"); // 这样直接添加肯定是不允许的

        //下面通过java的反射，绕过泛型  来给添加字符串
        Method add = list.getClass().getMethod("add", Object.class);
        add.invoke(list,"a");

        System.out.println(list); //[1, a] 输出没有没问题
        System.out.println(list.get(1)); //a
        Integer integer = list.get(1); // 这个还是会报错，报强转错误
        // 比较神奇的是如果把上面的Integer换成String，然后添加的是数字1的时候，输出就有问题了
    }

}
