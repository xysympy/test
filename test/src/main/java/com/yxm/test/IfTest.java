package com.yxm.test;

import java.util.Arrays;
import java.util.Random;

/**
 * @Description: 测试下if预测
 * @Author yxm
 * @Date 2021/9/30 10:14
 */
public class IfTest {
    public static void main(String[] args) {
        int len = 9999;
        int[] arr = new int[len];
        for (int i = 0; i < len; i++){
            arr[i] = new Random().nextInt();
        }
        long start = System.currentTimeMillis();
        for (int i : arr) {
            if (i > 5) {
                // 解决 git文件冲突测试
                System.out.println("测试: " + i);
                // 测试
                System.out.println("git commit message: " + i);
                // 再次测试
                // 提交测试
            }
        }
        long end = System.currentTimeMillis();
        long time = end - start;

        Arrays.sort(arr);
        start = System.currentTimeMillis();
        for (int i : arr) {
            if (i > 5) {
                System.out.println(i);
            }
        }
        end = System.currentTimeMillis();
        System.out.println("时间：" + time);
        System.out.println("时间：" + (end - start));
    }
}
