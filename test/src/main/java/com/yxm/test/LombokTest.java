package com.yxm.test;

import lombok.Data;
import lombok.Value;

/**
 * @author yxm
 * @description: lombok测试
 * @date 2021/11/6 17:24
 */
@Value
public class LombokTest {
    private String name;
}
