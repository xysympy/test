package com.yxm.test.mybatis.plus.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户表(User)实体类
 *
 * @author yxm
 * @since 2022-05-06 09:04:02
 */
@SuppressWarnings("serial")
@Data
public class User extends Model<User> {

    /**
     * 用户id
     */
    @TableId(type = IdType.AUTO)
    private String userId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    private Integer deleteState;

    private Date deleteTime;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.userId;
    }
}

