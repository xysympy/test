package com.yxm.test.mybatis.plus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxm.test.mybatis.plus.entity.User;

/**
 * 用户表(User)表服务接口
 *
 * @author yxm
 * @since 2022-05-06 09:04:02
 */
public interface UserService extends IService<User> {

    /**
     * 测试
     */
    void test();

    /**
     * mybatis测试
     */
    void mybatisTest();
}

