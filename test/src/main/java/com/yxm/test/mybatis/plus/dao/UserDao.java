package com.yxm.test.mybatis.plus.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxm.test.mybatis.plus.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户表(User)表数据库访问层
 *
 * @author yxm
 * @since 2022-05-06 09:04:02
 */
@Mapper
public interface UserDao extends BaseMapper<User> {

    /**
     * 用户列表
     *
     * @param deleteState 删除状态
     * @return {@link List}<{@link User}>
     */
    public List<User> listUser(Integer deleteState);
}

