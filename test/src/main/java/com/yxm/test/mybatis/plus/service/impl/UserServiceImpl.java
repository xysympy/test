package com.yxm.test.mybatis.plus.service.impl;

import cn.hutool.core.thread.ThreadFactoryBuilder;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxm.test.mybatis.plus.dao.UserDao;
import com.yxm.test.mybatis.plus.entity.User;
import com.yxm.test.mybatis.plus.service.UserService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 用户表(User)表服务接口实现类
 *
 * @author yxm
 * @since 2022-05-06 09:04:02
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

    private static final ThreadPoolExecutor THREAD_POOL_EXECUTOR;

    static {
        ThreadFactory threadFactory = ThreadFactoryBuilder.create()
                .setNamePrefix("Statistics-Datasource-thread").build();
        THREAD_POOL_EXECUTOR = new ThreadPoolExecutor(100, 100, 0L, TimeUnit.SECONDS
                , new LinkedBlockingQueue<>(), threadFactory);
    }

    @Override
    public void test() {
        final LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq( User::getDeleteState, false);
        // 之所以会产生部分sql语句出现没有使用到queryWrapper的过滤条件
        // queryWrapper应该主要是使用getSqlSegment对应的生成的sql语句来进行拼接到下面的list的
        // 在没有用到的地方是不会进行加载这个SQL语句的，所以得先做这块的处理
        // LocalDateTime.now();
        // 只是为了测试是否对mybatis的时区有影响
        // 答案是有影响，尽管在jdbc连接的时候使用了serverTimeZone=GMT+8但是如果在代码里面有设置的话，就会覆盖
        /*TimeZone.setDefault(TimeZone.getTimeZone("GMT+10"));
        System.out.println(queryWrapper.getSqlSegment());
        for (int i = 0; i < 100; i++) {
            final int temp = i;
            THREAD_POOL_EXECUTOR.execute(() -> {
                Map<String, Object> map = new HashMap<>(1);
                map.put("num", temp);
                // System.out.println(queryWrapper.getExpression().getSqlSegment());
                List<User> list = this.list(queryWrapper);
                if (!list.isEmpty()) {
                    System.out.println(list);
                }
            });
        }*/
        // mysql的datetime类型，mybatis是使用LocalDateTime来接收转换的，所以和下面的时区很有关系
        List<User> list = this.list(queryWrapper);
        if (!list.isEmpty()) {
            System.out.println(list);
        }
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+10"));
    }

    @Override
    public void mybatisTest() {
        for (int i = 0; i < 100; i++) {
            final int temp = i;
            THREAD_POOL_EXECUTOR.execute(() -> {
                Map<String, Object> map = new HashMap<>(1);
                map.put("num", temp);
                List<User> list = this.getBaseMapper().listUser(0);
                System.out.println(list.isEmpty());
            });
        }
    }
}

