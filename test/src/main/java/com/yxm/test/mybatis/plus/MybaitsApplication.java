package com.yxm.test.mybatis.plus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2022/5/6 9:26
 */
@SpringBootApplication
public class MybaitsApplication {
    public static void main(String[] args) {
        SpringApplication.run(MybaitsApplication.class, args);
    }
}
