package com.yxm.test.mybatis.plus.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yxm.test.mybatis.plus.entity.User;
import com.yxm.test.mybatis.plus.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 用户表(User)表控制层
 *
 * @author yxm
 * @since 2022-05-06 09:04:02
 */
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 服务对象
     */
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    /**
     * 测试
     */
    @RequestMapping("/test")
    public void test() {
        this.userService.test();
    }

    @RequestMapping("/mybatis/test")
    public void mybatisTest() {
        this.userService.mybatisTest();
    }

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param user 查询实体
     * @return 所有数据
     */
    @PostMapping("list")
    public List<User> selectAll(Page<User> page, @RequestBody User user) {
        return this.userService.page(page, new QueryWrapper<>(user)).getRecords();
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public User selectOne(@PathVariable Serializable id) {
        return this.userService.getById(id);
    }

    /**
     * 新增数据
     *
     * @param user 实体对象
     * @return 新增结果
     */
    @PostMapping("save")
    public Boolean insert(@RequestBody User user) {
        return this.userService.save(user);
    }

    /**
     * 修改数据
     *
     * @param user 实体对象
     * @return 修改结果
     */
    @PostMapping("update")
    public Boolean update(@RequestBody User user) {
        return this.userService.updateById(user);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("del")
    public Boolean delete(@RequestParam("idList") List<Long> idList) {
        return this.userService.removeByIds(idList);
    }
}

