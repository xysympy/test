package com.yxm.test.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: HashMapTest
 * @Description: map 的get方法的参数为什么是object
 * 为了符合map的设计思想，能够通过equals来获取对应的数据
 * @Author: yxm
 * @Date: 2022/7/28 10:22
 * @Version: 1.0
 **/
public class HashMapTest {
    public static void main(String[] args) {
        TestMap<Integer, Integer> testMap = new TestMap<>();
        System.out.println(testMap.get(1));
        // get方法的参数设计成Object应该是为了这样也能取出数据吧
        Map<List<Number>, Integer> map = new HashMap<>();
        List<Number> list = new ArrayList<>();
        list.add(1);
        map.put(list, 1);
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        System.out.println(map.get(list1));
    }

    private static class TestMap<K, V> {
        public V get(K key) {
            return null;
        }
    }
}
