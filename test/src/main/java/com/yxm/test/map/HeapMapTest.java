package com.yxm.test.map;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @ClassName: HeapMapTest
 * @Description: 测试下新建出来的map会不会删除原本的内容
 * @Author: yxm
 * @Date: 2022/2/13 18:50
 * @Version: 1.0
 **/
public class HeapMapTest {
    public static void main(String[] args) {
        ConcurrentSkipListMap<Long, String> map = new ConcurrentSkipListMap<>();
        map.put(111L, "ceshi");
        map.put(222L, "ceshi");
        map.put(333L, "ceshi");
        map.put(444L, "ceshi");
        ConcurrentNavigableMap<Long, String> heapMap = map.headMap(333L, true);
        System.out.println(heapMap.size());
        System.out.println(map.size());
        heapMap.clear();
        // 会删除掉原来的那些数据
        System.out.println(heapMap.size());
        System.out.println(map.size());

        Map<String, String> map1 = new HashMap<>();
        map1.put("1", "1");
        System.out.println(map1.size());
        // 如果直接修改里面的数据的话，也是会改变的
        update(map1);
        System.out.println(map1.size());
    }

    private static void update(Map map) {
        // 这个会指向新的地址，
        map = new HashMap();
    }
}
