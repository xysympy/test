package com.yxm.test.desgin;

import java.util.stream.Stream;

/**
 * @author yxm
 * @description: 工厂模式：专门用来创建对象的类
 * 静态工厂的实现
 * @date 2022/5/12 14:48
 */
public class StaticFactoryDemo {
    public static void main(String[] args) {
        // 静态工厂创建对象，就是写死的那种
        FactoryTest.test(new StaticFactory());
    }
}

class BadShapeCreation extends RuntimeException {
    public BadShapeCreation(String msg) {
        super(msg);
    }
}

class Shape {
    private static int counter = 0;
    private int id = counter++;

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" + id + "]";
    }

    public void draw() {
        System.out.println(this + " draw");
    }

    public void erase() {
        System.out.println(this + " erase");
    }
}

class Circle extends Shape {
}

class Square extends Shape {
}

class Triangle extends Shape {
}

interface FactoryMethod {
    /**
     * 创建
     *
     * @param type 类型
     * @return {@link Shape}
     */
    Shape create(String type);
}

class StaticFactory implements FactoryMethod {

    @Override
    public Shape create(String type) {
        switch (type) {
            case "Circle":
                return new Circle();
            case "Square":
                return new Square();
            case "Triangle":
                return new Triangle();
            default:
                throw new BadShapeCreation(type);
        }
    }
}

class FactoryTest {
    public static void test(FactoryMethod factory) {
        Stream.of("Circle", "Square", "Triangle",
                "Square", "Circle", "Circle", "Triangle")
                .map(factory::create)
                .peek(Shape::draw)
                .peek(Shape::erase)
                .count(); // Terminal operation
    }
}