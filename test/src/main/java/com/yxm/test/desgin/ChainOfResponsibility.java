package com.yxm.test.desgin;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

class Result {
    boolean success;
    List<Double> line;

    Result(List<Double> data) {
        success = true;
        line = data;
    }

    Result() {
        success = false;
        line = Collections.<Double>emptyList();
    }
}

class Fail extends Result {
}

interface Algorithm {
    Result algorithm(List<Double> line);
}

class FindMinima3 {
    public static Result leastSquares(List<Double> line) {
        System.out.println("LeastSquares.algorithm");
        boolean weSucceed = false;
        if (weSucceed) {
            // Actual test/calculation here
            return new Result(Arrays.asList(1.1, 2.2));
        } else {
        // Try the next one in the chain:
            return new Fail();
        }
    }

    public static Result perturbation(List<Double> line) {
        System.out.println("Perturbation.algorithm");
        boolean weSucceed = false;
        if (weSucceed) // Actual test/calculation here
        {
            return new Result(Arrays.asList(3.3, 4.4));
        } else {
            return new Fail();
        }
    }

    public static Result bisection(List<Double> line) {
        System.out.println("Bisection.algorithm");
        boolean weSucceed = true;
        if (weSucceed) // Actual test/calculation here
        {
            return new Result(Arrays.asList(5.5, 6.6));
        } else {
            return new Fail();
        }
    }

    static List<Function<List<Double>, Result>>
            algorithms = Arrays.asList(
            FindMinima3::leastSquares,
            FindMinima3::perturbation,
            FindMinima3::bisection
    );

    public static Result minima(List<Double> line) {
        for (Function<List<Double>, Result> alg :
                algorithms) {
            Result result = alg.apply(line);
            if (result.success) {
                return result;
            }
        }
        return new Fail();
    }
}

/**
 * 责任链模式：一个列表里面存放所有的校验规则，依次对一个对象进行校验。
 * 一种是对这个对象进行校验（使用责任链里面的全部规则），出现校验错误的，直接返回不正确（过滤器，拦截器这种）
 * 是为了找到哪一个规则是能够校验传入进来的对象（spring的参数解析器这种）
 *
 * @author yxm
 * @date 2022/05/12
 */
public class ChainOfResponsibility {
    public static void main(String[] args) {
        List<Double> line = Arrays.asList(
                1.0, 2.0, 1.0, 2.0, -1.0,
                3.0, 4.0, 5.0, 4.0);
        Result result = FindMinima3.minima(line);
        if (result.success) {
            System.out.println(result.line);
        } else {
            System.out.println("No algorithm found");
        }
    }
}