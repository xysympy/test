package com.yxm.test.desgin;

import com.yxm.test.thread.Nap;

/**
 * @author yxm
 * @description: 状态模式：由于事件的发生导致对象的状态发生改变。
 * 就像是游戏一样，获得这个道具时，你就变成火焰人；获得这个道具时，你就变成冰人；吃了这个道具时，变回....
 * 常见的就是状态机
 * @date 2022/5/12 14:04
 */
public class StateDemo {
    public static void main(String[] args) {
        // 这个是类似于自动的，实际上可以采用调用，也就是外部输入的形式
        // 也就是通过触发了某一事件，然后状态发生改变
        new Washer();
    }
}

interface State {
    /**
     * 运行
     */
    void run();
}

abstract class StateMachine {
    protected State currentState;

    /**
     * 改变状态
     *
     * @return boolean
     */
    protected abstract boolean changeState();

    protected final void runAll() {
        while (changeState()) {
            currentState.run();
        }
    }
}

class Wash implements State {
    @Override
    public void run() {
        System.out.println("Washing");
        new Nap(0.5);
    }
}

class Spin implements State {
    @Override
    public void run() {
        System.out.println("Spinning");
        new Nap(0.5);
    }
}

class Rinse implements State {
    @Override
    public void run() {
        System.out.println("Rinsing");
        new Nap(0.5);
    }
}

/**
 * 垫圈
 *
 * @author Administrator
 * @date 2022/05/12
 */
class Washer extends StateMachine {
    private int i = 0;
    /**
     * The state table:
     */
    private State[] states = {
            new Wash(), new Spin(),
            new Rinse(), new Spin(),
    };

    Washer() {
        runAll();
    }

    @Override
    public boolean changeState() {
        if (i < states.length) {
            // Change the state by setting the
            // surrogate reference to a new object:
            currentState = states[i++];
            return true;
        } else {
            return false;
        }
    }
}