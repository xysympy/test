package com.yxm.test.desgin;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yxm
 * @description: 动态工厂：依靠反射来达到
 * @date 2022/5/12 14:54
 */
public class DynamicFactoryDemo {
    public static void main(String[] args) {
        FactoryTest.test(new DynamicFactory());
    }
}

class DynamicFactory implements FactoryMethod {

    Map<String, Constructor> factories = new HashMap<>();

    static Constructor load(String type) {
        System.out.println("loading " + type);
        try {
            return Class.forName("com.yxm.test.desgin." + type)
                    .getConstructor();
        } catch (ClassNotFoundException |
                NoSuchMethodException e) {
            throw new BadShapeCreation(type);
        }
    }

    @Override
    public Shape create(String type) {
        try {
            return (Shape) factories
                    .computeIfAbsent(type, DynamicFactory::load)
                    .newInstance();
        } catch (InstantiationException |
                IllegalAccessException |
                InvocationTargetException e) {
            throw new BadShapeCreation(type);
        }
    }
}