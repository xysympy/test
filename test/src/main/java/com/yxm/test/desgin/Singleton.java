package com.yxm.test.desgin;

/**
 * @author yxm
 * @description: 单例模式的实现(静态内部类)
 * 首先要了解类加载过程中的最后一个阶段：即类的初始化，类的初始化阶本质就是执行类构造器的<clinit>方法。
 *
 * <clinit>方法：这不是由程序员写的程序，而是根据代码由javac编译器生成的。它是由类里面所有的类变量的赋值动作和静态代码块组成的。
 * JVM内部会保证一个类的<clinit>方法在多线程环境下被正确的加锁同步，也就是说如果多个线程同时去进行“类的初始化”，
 * 那么只有一个线程会去执行类的<clinit>方法，其他的线程都要阻塞等待，直到这个线程执行完<clinit>方法。然后执行完<clinit>方法后，其他线程唤醒，但是不会再进入<clinit>()方法。也就是说同一个加载器下，一个类型只会初始化一次。
 * <p>
 * 那么回到这个代码中，这里的静态变量的赋值操作进行编译之后实际上就是一个<clinit>代码，当我们执行getInstance方法的时候，
 * 会导致SingleTonHolder类的加载，类加载的最后会执行类的初始化，但是即使在多线程情况下，这个类的初始化的<clinit>代码也只会被执行一次，所以他只会有一个实例。
 * <p>
 * 那么再增加一句，之所以这里变量定义的时候不需要volatile，因为只有一个线程会执行具体的类的初始化代码<clinit>，
 * 也就是即使有指令重排序，因为根本没有第二个线程给你去影响，所以无所谓。
 * @date 2022/5/11 11:12
 */
public class Singleton {

    private static final class ResourceImpl implements Resource {

        private int value;

        public ResourceImpl(int value) {
            System.out.println("value: " + value);
            this.value = value;
        }

        @Override
        public int getValue() {
            return 0;
        }

        @Override
        public void setValue(int value) {
            this.value = value;
        }
    }

    private static class ResourceHolder {
        private static ResourceImpl resource = new ResourceImpl(1);
    }

    public static ResourceImpl getResource() {
        return ResourceHolder.resource;
    }

    public static void main(String[] args) {
        // 静态内部类是懒加载的，没有调用到它，它是不会加载的
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                Resource resource = Singleton.getResource();
                System.out.println(resource);
            }).start();
        }
    }
}


interface Resource {
    /**
     * 获得价值
     *
     * @return int
     */
    int getValue();

    /**
     * 设置值
     */
    void setValue(int value);
}