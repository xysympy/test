package com.yxm.test.desgin;

class WhatIHave {
    public void g() {
    }

    public void h() {
    }
}

interface WhatIWant {
    void f();
}

/**
 * 代理适配器
 * 第一种实现方式：直接使用代理的方式来改造
 * @author yxm
 * @date 2022/05/12
 */
class ProxyAdapter implements WhatIWant {
    WhatIHave whatIHave;

    ProxyAdapter(WhatIHave wih) {
        whatIHave = wih;
    }

    @Override
    public void f() {
        // Implement behavior using
        // methods in WhatIHave:
        whatIHave.g();
        whatIHave.h();
    }
}

class WhatIUse {
    public void op(WhatIWant wiw) {
        wiw.f();
    }
}


/**
 * Approach 2: build adapter use into op():
 * 第二种方式：在第一种的基础上创建一个工具类来自动new 代理适配器类
 * @author yxm
 * @date 2022/05/12
 */
class WhatIUse2 extends WhatIUse {
    public void op(WhatIHave wih) {
        new ProxyAdapter(wih).f();
    }
}

/**
 * Approach 3: build adapter into WhatIHave:
 * 第三种方式：继承被改造的类，然后实现我们自定义的我们想要的功能的接口
 * @author yxm
 * @date 2022/05/12
 */
class WhatIHave2 extends WhatIHave implements WhatIWant {
    @Override
    public void f() {
        g();
        h();
    }
}


/**
 * Approach 4: use an inner class:
 * 第四种方式：使用内部类的形式来生成内部适配器
 * @author yxm
 * @date 2022/05/12
 */
class WhatIHave3 extends WhatIHave {
    private class InnerAdapter implements WhatIWant {
        @Override
        public void f() {
            g();
            h();
        }
    }

    public WhatIWant whatIWant() {
        return new InnerAdapter();
    }
}

/**
 * 适配器模式：手头上有一个类，但是需要的是另外一个类，所以我们需要自己生成一个适配类
 * 就类似于手头上有一个工具包，它能+1，+2.我需要的是+3，所以这个时候我实际上可以直接把两者的方法叠加在一个方法就行
 * 当然，这个例子不太好，总的来说就是提供给我的不是我想要的，然后我生成一个适配器类来
 * 基于这个提供给我的类改造成我想要的类
 *
 * @author Administrator
 * @date 2022/05/12
 */
public class Adapter {
    public static void main(String[] args) {
        WhatIUse whatIUse = new WhatIUse();
        WhatIHave whatIHave = new WhatIHave();
        WhatIWant adapt = new ProxyAdapter(whatIHave);
        whatIUse.op(adapt);
        // Approach 2:
        WhatIUse2 whatIUse2 = new WhatIUse2();
        whatIUse2.op(whatIHave);
        // Approach 3:
        WhatIHave2 whatIHave2 = new WhatIHave2();
        whatIUse.op(whatIHave2);
        // Approach 4:
        WhatIHave3 whatIHave3 = new WhatIHave3();
        whatIUse.op(whatIHave3.whatIWant());
    }
}