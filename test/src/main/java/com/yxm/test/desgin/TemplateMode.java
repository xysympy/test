package com.yxm.test.desgin;

import java.util.stream.IntStream;

/**
 * @author yxm
 * @description: 模板模式：
 * 个人理解：一般是有一个控制流程的方法，流程对应的点作为需要实现的抽象方法交给具体的子类实现
 * @date 2022/5/12 13:19
 */
public class TemplateMode {

    public static void main(String[] args) {
        new MyApp();
    }
}

abstract class ApplicationFramework {
    protected ApplicationFramework() {
        templateMethod();
    }

    /**
     * customize1
     */
    abstract void customize1();

    /**
     * customize2
     */
    abstract void customize2();

    private void templateMethod() {
        IntStream.range(0, 5).forEach(n -> {
            customize1();
            customize2();
        });
    }
}

class MyApp extends ApplicationFramework {

    @Override
    void customize1() {
        System.out.println("MyApp customize1");
    }

    @Override
    void customize2() {
        System.out.println("MyApp customize2");
    }
}