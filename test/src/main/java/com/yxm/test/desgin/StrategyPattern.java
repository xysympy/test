package com.yxm.test.desgin;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

// The common strategy base type:
class FindMinima {
    Function<List<Double>, List<Double>> algorithm;
}

// The various strategies:
class LeastSquares extends FindMinima {
    LeastSquares() {
        // Line is a sequence of points (Dummy data):
        algorithm = (line) -> Arrays.asList(1.1, 2.2);
    }
}

class Perturbation extends FindMinima {
    Perturbation() {
        algorithm = (line) -> Arrays.asList(3.3, 4.4);
    }
}

class Bisection extends FindMinima {
    Bisection() {
        algorithm = (line) -> Arrays.asList(5.5, 6.6);
    }
}

// The "Context" controls the strategy:
class MinimaSolver {
    private FindMinima strategy;

    MinimaSolver(FindMinima strat) {
        strategy = strat;
    }

    List<Double> minima(List<Double> line) {
        return strategy.algorithm.apply(line);
    }

    void changeAlgorithm(FindMinima newAlgorithm) {
        strategy = newAlgorithm;
    }
}

/**
 * 策略模式：不同的策略类对应不同的解决方法，类似于if-else，对应不同的策略。
 * 所以，如果if-else过多的话，也是可以使用策略模式来修改
 *
 * @author yxm
 * @date 2022/05/12
 */
public class StrategyPattern {
    public static void main(String[] args) {
        MinimaSolver solver =
                new MinimaSolver(new LeastSquares());
        List<Double> line = Arrays.asList(
                1.0, 2.0, 1.0, 2.0, -1.0,
                3.0, 4.0, 5.0, 4.0);
        System.out.println(solver.minima(line));
        solver.changeAlgorithm(new Bisection());
        System.out.println(solver.minima(line));
    }
}