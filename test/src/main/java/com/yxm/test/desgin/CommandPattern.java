package com.yxm.test.desgin;

import java.util.Arrays;
import java.util.List;

/**
 * 命令模式：命令模式 的主要特点是允许向一个方法或者对象传递一个想要的动作
 *  就是像下面的例子中，传入的是打印hello，打印....这些命令
 *  可以看看这个https://refactoringguru.cn/design-patterns/command/java/example
 *
 *  和策略模式的不同：策略模式是运行时确定具体是哪一个策略类执行方法
 *  而命令模式传入的就是命令对象，更多的就是在编译时就确定下来了，也就是
 *  编译器就已经确定你要调用的对象是哪一个了，而策略模式需要在运行时，就类似于多态
 * @author yxm
 * @date 2022/05/12
 */
public class CommandPattern {
  public static void main(String[] args) {
    List<Runnable> macro = Arrays.asList(
      () -> System.out.print("Hello "),
      () -> System.out.print("World! "),
      () -> System.out.print("I'm the command pattern!")
    );
    macro.forEach(Runnable::run);
  }
}