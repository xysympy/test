package com.yxm.test.desgin;

/**
 * @author yxm
 * @description: 代理模式：分为代理对象和真实对象，代理对象实现额外的逻辑，和核心逻辑没有关系的那种
 * 真实对象就专注于实现核心的逻辑
 * 实际上和桥接模式有点相似
 * on Java 8中指出两者的不同：在结构上，代理模式和桥接模式的区别很简单:代理模式只有一个实现，而桥接模式有多个实现。
 * 个人理解：代理对象更多的是代理一个对象，而桥接模式是连接两个对象的一个桥梁{@link BridgingDemo}
 * @date 2022/5/12 13:35
 */
public class ProxyDemo {
    public static void main(String[] args) {
        // 实际上代理对象并不需要实现同一个接口，但是这样会比较方便，知道被代理对象有哪些方法
        Proxy target = new Implementation();
        Proxy proxy = new ProxyBase(target);
        proxy.h();
        proxy.g();
        proxy.f();
    }
}

interface Proxy {
    /**
     * f
     */
    void f();

    /**
     * g
     */
    void g();

    /**
     * h
     */
    void h();
}

class Implementation implements Proxy {

    @Override
    public void f() {
        System.out.println("Implementation f");
    }

    @Override
    public void g() {
        System.out.println("Implementation g");
    }

    @Override
    public void h() {
        System.out.println("Implementation h");
    }
}

class ProxyBase implements Proxy {

    private final Proxy target;

    public ProxyBase(Proxy target) {
        this.target = target;
    }

    @Override
    public void f() {
        target.f();
    }

    @Override
    public void g() {
        target.g();
    }

    @Override
    public void h() {
        target.h();
    }
}