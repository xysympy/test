package com.yxm.test.desgin;

import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * @author yxm
 * @description: 多态工厂：利用Java8的lambda表达式和函数式接口来实现
 * @date 2022/5/12 14:59
 */
public class PolymorphicFactoryDemo {
    public static void main(String[] args) {
        RandomShapes shapes = new RandomShapes(Circle::new, Square::new, Triangle::new);
        Stream.generate(shapes).limit(6).peek(Shape::draw).peek(Shape::erase).count();
    }
}


interface PolymorphicFactory {
    /**
     * 创建
     *
     * @return {@link Shape}
     */
    Shape create();
}

class RandomShapes implements Supplier<Shape> {

    private final PolymorphicFactory[] factories;
    private Random rand = new Random(42);

    RandomShapes(PolymorphicFactory ...factories) {
        this.factories = factories;
    }

    @Override
    public Shape get() {
        return factories[rand.nextInt(factories.length)].create();
    }
}