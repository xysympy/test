package com.yxm.test.desgin;

class A {
    A(int x) {
    }
}

class B {
    B(long x) {
    }
}

class C {
    C(double x) {
    }
}

/**
 * Other classes that aren't exposed by the
 * facade go here ...
 * 外观模式：如果我们有一堆让人头晕的类以及交互（Interactions），
 * 而它们又不是客户端程序员必须了解的，那我们就可以为客户端程序员创建一个接口只提供那些必要的功能。
 * 类似于创建一个类，然后这个类是一个集成类，包含更多的类的功能，而那些类的功能是不需要使用者知道的
 * @author yxm
 * @date 2022/05/12
 */
public class Facade {
    static A makeA(int x) {
        return new A(x);
    }

    static B makeB(long x) {
        return new B(x);
    }

    static C makeC(double x) {
        return new C(x);
    }

    public static void main(String[] args) {
        // The client programmer gets the objects
        // by calling the static methods:
        A a = Facade.makeA(1);
        B b = Facade.makeB(1);
        C c = Facade.makeC(1.0);
    }
}