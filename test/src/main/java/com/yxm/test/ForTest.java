package com.yxm.test;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 测试for循环
 * @Author yxm
 * @Date 2021/10/28 8:54
 */
public class ForTest {
    public static void main(String[] args) {
        System.out.println("================普通for循环：");
        for (int i = 0; i < getData().size(); i++) {
            System.out.println(i);
        }
        System.out.println("================增强for循环：");
        for (Integer i : getData()) {
            System.out.println(i);
        }
        System.out.println("================Java8提供的foreach循环：");
        getData().forEach(System.out::println);

        int j = 10; int t = 0;
        for (int i = t; i < j; i++){
            j = j - 2;t = t - 1;
            System.out.println(i);
        }
    }

    private static List<Integer> getData() {
        System.out.println("getDate被调用...");
        List list = new ArrayList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        return list;
    }
}
