package com.yxm.test;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.util.StopWatch;

import java.util.UUID;

/**
 * @author yxm
 * @description: 测试使用Apache的Commons包
 * @date 2021/11/8 14:03
 */
public class ApacheCommonsTest {
    public static void main(String[] args) throws InterruptedException {
        // boolean类型数组长度为0，还有其它的也是有的
        boolean[] booleans = ArrayUtils.EMPTY_BOOLEAN_ARRAY;
        // 基本数据类型数组和非基本数据类型的数组
        // 但是String是额外的
        int[] ints = new int[10];
        Integer[] integers = ArrayUtils.toObject(ints);
        int[] primitive = ArrayUtils.toPrimitive(integers);
        // 这里面的参数不能是基本数据类型，不然会出现编译错误，而且里面必须有值
        String[] strings = ArrayUtils.toStringArray(new Integer[]{1});
        //strings = ArrayUtils.toStringArray(new int[10]);
        // 下面的代码就会把new Integer里面的为null的数据替换成""空字符串
        ArrayUtils.toStringArray(new Integer[10], "");
        // 强烈每一个秒表都给一个id，这样查看日志起来能够更加的精确
        // 至于Id 我觉得给UUID是可行的~
        StopWatch sw = new StopWatch(UUID.randomUUID().toString());

        sw.start("起床");
        Thread.sleep(1000);
        System.out.println("当前任务名称：" + sw.currentTaskName());
        sw.stop();

        sw.start("洗漱");
        Thread.sleep(2000);
        System.out.println("当前任务名称：" + sw.currentTaskName());
        sw.stop();

        sw.start("锁门");
        Thread.sleep(500);
        System.out.println("当前任务名称：" + sw.currentTaskName());
        sw.stop();

        System.out.println(sw.prettyPrint()); // 这个方法打印在我们记录日志时是非常友好的  还有百分比的分析哦
        System.out.println(sw.shortSummary());
        System.out.println(sw.currentTaskName()); // stop后它的值为null


        // 最后一个任务的相关信息
        System.out.println(sw.getLastTaskName());
        System.out.println(sw.getLastTaskInfo());

        // 任务总的耗时  如果你想获取到每个任务详情（包括它的任务名、耗时等等）可使用
        System.out.println("所有任务总耗时：" + sw.getTotalTimeMillis());
        System.out.println("任务总数：" + sw.getTaskCount());
        System.out.println("所有任务详情：" + sw.getTaskInfo()); // 拿到所有的任务


    }
}
