package com.yxm.test.annotation;

import java.lang.annotation.Annotation;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/12/8 8:50
 */
public class TestAnnotation {
    public static void main(String[] args) {
        // 貌似是不能进行直接的校验这个类上面有Component(实际上是Service注解)
        AnnotationClass annotationClass = new AnnotationClass();
        if (Service.class.isAnnotationPresent(Component.class)) {
            System.out.println("Component");
        }
        for (Annotation annotation : annotationClass.getClass().getAnnotations()) {
            System.out.println(annotation.annotationType().getName());
        }
    }
}
