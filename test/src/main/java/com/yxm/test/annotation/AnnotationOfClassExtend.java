package com.yxm.test.annotation;

import java.lang.annotation.*;
import java.lang.reflect.Method;

/**
 * @author yxm
 * @description: 关于注解在父子类之间继承
 * /@Inherited/这个注解起作用的地方是在父类上，也就是父类的类注解有这个元注解的注解的时候，子类会自动有这个注解
 * 而不需要显示的添加这个注解
 * @date 2022/4/22 10:31
 */
public class AnnotationOfClassExtend {
    public static void main(String[] args) {
        Class<AnnotationClassSon> annotationClassSonClass = AnnotationClassSon.class;
        for (Method method : annotationClassSonClass.getMethods()) {
            for (Annotation annotation : method.getAnnotations()) {
                System.out.println(annotation.toString());
            }
        }

        for (Annotation annotation : annotationClassSonClass.getAnnotations()) {
            System.out.println(annotation.toString());
        }
    }
}

@AnnotationTest
class AnnotationClassParent {

    @AnnotationTest
    public void annotationTest() {

    }
}

class AnnotationClassSon extends AnnotationClassParent {

}

/**
 * 注释测试
 *
 * @author yxm
 * @date 2022/04/22
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE, ElementType.METHOD})
@Inherited
@interface AnnotationTest {

}