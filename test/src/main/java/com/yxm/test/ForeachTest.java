package com.yxm.test;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

/**
 * @author yxm
 * @description: 内部迭代和外部迭代的时间上的比较
 *  看到网上的一拼啊文章说，流的foreach时间都消耗在了线城池的调用上，所以会显得比较慢，但是一旦业务的数据量很大的时候就会很有效果
 * @date 2021/11/5 9:12
 */
public class ForeachTest {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);

        // 传统的for循环遍历
        Instant begin = Instant.now();
        for (Integer i : list) {
            System.out.println(i);
        }
        Instant end = Instant.now();
        System.out.println("消耗的时间:" + Duration.between(begin, end).toMillis());

        // 内部迭代
        begin = Instant.now();
        list.stream().forEach(System.out::println);
        end = Instant.now();
        System.out.println("消耗的时间:" + Duration.between(begin, end).toMillis());

        // 并行流处理(缺点是没有顺序)
        begin = Instant.now();
        list.stream().filter(temp -> temp.equals(1)).forEach(System.out::println);
        end = Instant.now();
        System.out.println("消耗的时间:" + Duration.between(begin, end).toMillis());
    }
}
