package com.yxm.test;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Scanner;
import java.util.StringJoiner;

/**
 * @ClassName: ScriptEngineTest
 * @Description: 测试使用java运行脚本
 * @Author: yxm
 * @Date: 2022/7/14 17:50
 * @Version: 1.0
 **/
public class ScriptEngineTest {
    public static void main(String[] args) {
//        String str = "400+5";
        Scanner reader = new Scanner(System.in);
        while (reader.hasNext()) {
            String str = reader.nextLine();
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine js = manager.getEngineByName("js");
            try {
                System.out.println(js.eval(str));
            } catch (ScriptException e) {
                e.printStackTrace();
            }
        }
    }
}
