package com.yxm.test;

/**
 * @Description: 测试lambda表达式
 * @Author yxm
 * @Date 2021/11/4 15:10
 */
public class TestLambda {

    public static void printString(String s, Print<String> print){
        print.print(s);
    }
    public static void main(String[] args) {
        printString("test", s -> System.out.println(s));
    }
}

@FunctionalInterface
interface Print<T>{
    /**
     * 打印
     *
     * @param x x
     */
    public void print(T x);
}