package com.yxm.test.classtest;

/**
 * @Description: 测试一下继承的类和要实现的接口有一模一样的方法会发生什么
 * @Author yxm
 * @Date 2021/10/25 18:33
 */
public class InnerClassTest extends InnerClassObject implements InnerClass{

    @Override
    public void test(){
        System.out.println("test");
    }
    public static void main(String[] args) {
        // 要实现的接口和类如果有同名的方法，子类没有实现的，就会用继承的父类的方法作为接口的默认实现
        // 如果实现了，那么就是子类的实现了的那个方法
        InnerClassTest test = new InnerClassTest();
        test.test();
    }
}

// 仅仅是为了测试用，一般不会这么去写
interface InnerClass{
    void test();
}

class InnerClassObject{
    public void test(){
        System.out.println("InnerClassObject");
    }
}
