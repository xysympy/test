package com.yxm.test.classtest;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yxm
 * @description: 获取集合类的泛型对象
 * @date 2022/5/26 16:40
 */
public class ActualClassTest {

    private List<String> list = new ArrayList<>();

    public static void main(String[] args) throws NoSuchFieldException, NoSuchMethodException {

        //下边这组大括号非常重要(匿名类) 这种方式可以，但是不常用，因为这个变相是使用了继承
        // 而子类继承一个确定泛型的父类的时候是可以获取到的，下面不行，会获取到E
        List<String> list = new ArrayList<String>() {};
        System.out.println(getActualType(list,0));
        // 这种方式就不行
        List<String> list1 = new ArrayList<>();
        System.out.println(getActualType(list1, 0));
        // 属性的方式可以
        Class<ActualClassTest> aClass = ActualClassTest.class;
        Field declaredField = aClass.getDeclaredField("list");
        Type type = declaredField.getGenericType();
        System.out.println(((ParameterizedType) type).getActualTypeArguments()[0]);
        // Java 会在class文件中保留泛型的信息，只是字节码的方法体或者类结构中擦掉了泛型信息
        // 测试下获取到方法请求参数的泛型类型
        Method aClassMethod = aClass.getMethod("list", List.class);
        Type parameterType = aClassMethod.getGenericParameterTypes()[0];
        System.out.println(((ParameterizedType) parameterType).getActualTypeArguments()[0]);
    }

    public static String getActualType(Object o,int index) {
        Type clazz = o.getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType)clazz;
        return pt.getActualTypeArguments()[index].toString();
    }

    public static void list(List<String> strings) {

    }
}
