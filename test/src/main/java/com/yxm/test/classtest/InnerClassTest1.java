package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 内部类测试
 * @date 2022/3/21 11:00
 */
public class InnerClassTest1 {

    class Content {

    }

    static class StaticContent {

    }

    StaticContent getStatic() {
        return new StaticContent();
    }

    Content getContent() {
        return new Content();
    }
}
