package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 在基类（父类）构造器中调用可以重写的方法
 * @date 2022/3/18 10:44
 */
public class MethodExtend extends ParentMethod{

    private int redis = 1;

    MethodExtend() {
        System.out.println("MethodExtend");
    }

    @Override
    public void draw() {
        System.out.println("MethodExtend redis: " + redis);
    }
}

class ParentMethod {

    ParentMethod() {
        System.out.println("ParentMethod method before");
        draw();
        System.out.println("ParentMethod method after");
    }

    public void draw() {
        System.out.println("ParentMethod draw");
    }
}

class MethodExtendTest {
    public static void main(String[] args) {
        // 应该是因为父类去调用子类实现的draw方法的时候，成员变量还没有完成初始化
        // 仅仅只是完成了分配空间这样的操作，导致子类的成员变量这个时候还是0
        /**
         * 可以看看{@link ExecutionSequence}里面的main方法的子类和父类的初始化顺序注释
         * 结论就是，尽量少在构造方法中调用方法，那样可能会出现bug
         */
        MethodExtend methodExtend = new MethodExtend();
        methodExtend.draw();
    }
}