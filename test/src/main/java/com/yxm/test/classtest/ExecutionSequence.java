package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 测试下代码块的执行顺序
 * 组合和委托的区别：委托会将当前对象传入给被委托的对象中
 * @date 2022/3/17 15:04
 */
public class ExecutionSequence extends ParentExecution {
    private int i = 0;

    {
        String temp = new String("1");
        sout(temp);
        i = 1;
        // 代码块后执行
    }

    private static int j = 0;

    static {
        j = 1;
        System.out.println("ExecutionSequence static: " + j);
    }

    public ExecutionSequence() {
        System.out.println("ExecutionSequence: " + i);
    }

    @Override
    public String get() {
        return "";
    }

    private void sout(String temp) {
        System.out.println(temp);
    }

    public static void main(String[] args) {
        // 单独的类的执行顺序是常量，静态变量，普通变量，静态代码块，代码块，构造方法
        // 加上父类的话：先执行父类的静态成员变量，父类的静态代码块，子类的静态成员变量，子类的静态代码块
        // 父类的非静态成员变量，父类的代码块，父类的构造方法，子类的非成员静态变量，子类的代码块，子类的构造方法
        ExecutionSequence sequence = new ExecutionSequence();
    }

}

class ParentExecution {
    public int k = 0;

    static {
        System.out.println("ParentExecution static");
    }

    {
        System.out.println("ParentExecution: " + k);
    }

    public ParentExecution() {
        System.out.println("ParentExecution");
    }

    public CharSequence get(){
        return "";
    }

    public static void main(String[] args) {

    }
}