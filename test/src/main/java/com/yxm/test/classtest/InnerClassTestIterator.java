package com.yxm.test.classtest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yxm
 * @description: 内部类实现简易的迭代器
 * @date 2022/3/21 11:10
 */
public class InnerClassTestIterator {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        java.util.Iterator<Integer> iterator = list.iterator();
        Sequence sequence = new Sequence(10);
        for (int j = 0; j < 10; j++) {
            sequence.add(j);
        }
        Iterator innerIterator = sequence.iterator();
        while (!innerIterator.end()) {
            System.out.println(innerIterator.current());
            innerIterator.next();
        }
    }
}


interface Iterator {
    /**
     * 结束
     *
     * @return boolean
     */
    boolean end();

    /**
     * 当前
     *
     * @return int
     */
    int current();

    /**
     * 下一个
     * 表示当前是否还有数据，有的话，将指针后移
     *
     * @return boolean
     */
    boolean next();
}

class Sequence {
    private int[] nums;
    private int next = 0;

    public Sequence(int size) {
        nums = new int[size];
    }

    public void add(int value) {
        nums[next++] = value;
    }

    public Iterator iterator() {
        return new InnerIterator();
    }

    public Iterator iterator(int size) {
        return new InnerIterator(size){};
    }

    public class InnerIterator implements Iterator {

        // 不能有static方法和字段

        private int pos = 0;

        public InnerIterator() {}
        public InnerIterator(int size) {}

        @Override
        public boolean end() {
            // 内部类的局部变量是使用final来接收的，因为内部类的生命周期长于局部变量
            return pos == Sequence.this.nums.length;
        }

        @Override
        public int current() {
            return Sequence.this.nums[pos];
        }

        @Override
        public boolean next() {
            boolean flag = pos <= Sequence.this.nums.length;
            pos++;
            return flag;
        }

        /*public void add(int[] nums) {
            // 使用外部类的属性
            Sequence.this .nums= nums;
        }*/
    }

}

class SequenceExtend extends Sequence {

    public SequenceExtend(int size) {
        super(size);
    }
    class InnerIteratorExtends extends Sequence.InnerIterator {

        public InnerIteratorExtends() {
            // 必须是在这种情况下才可以不用跟下面的构造器代码那样
            // 因为这里面是隐含了指向外部类
            super();
        }
    }
}

/*
class InnerIteratorExtends extends Sequence.InnerIterator {

    public InnerIteratorExtends(Sequence sequence) {
        // 继承匿名内部类，只能这么弄
        // 普通的继承只需要直接调用super()；而内部类是依赖于外部类的，所以需要显示的指定外部类
        sequence.super();
    }
}*/
