package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 测试下this和super
 * @date 2022/4/26 9:10
 */
public class SuperThisTest {

    public static void main(String[] args) {
        This this1 = new This();
        This this2 = new This(1);
    }
}

class Super {

    public Super() {
        System.out.println("Super 调用");
    }

    public Super(int i) {
        System.out.println("Super 有参调用" + i);
    }
}

class This extends Super {
    public This() {
        // 我认为是，在调用了this之后就不会再默认的去调用super，就相当于你显式的调用了super(xx)
        // 所以，这个时候你会发现只调用了一次
        // super和this不能同时存在：
        //                       1. super和this都是只能在第一行或者叫最先开始就调用的，两个放在一起显然有一个不满足
        //                       2. 实际上是因为super会调用父类的构造，this()也会调用父类的构造，这样就会造成一个父类初始化
        //                       两次，显然是不行的
        this(1);
        System.out.println("This 无参构造函数调用");
    }

    public This(int i) {
        super(i);
        System.out.println("This 有参构造函数调用" + i);
    }
}