package com.yxm.test.classtest;

import java.io.*;

/**
 * @author yxm
 * @description: 测试使用Externalizable接口是使用
 * Serializable接口是jvm自动的实现序列化和反序列化的，也就是过程不是我们控制的
 * 而Externalizable是类似于自定义的
 * 所以transient在这儿会失效
 * @date 2022/4/26 10:24
 */
public class ExternalizeTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ExternalizeParent externalizeParent = new ExternalizeParent("test", 18);
        System.out.println(externalizeParent);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(externalizeParent);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        ExternalizeParent readObject = (ExternalizeParent) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        ExternalizeParent.x = 11;
        System.out.println(readObject);
    }
}

class ExternalizeParent implements Externalizable {
    private String name;
    private int age;
    private transient String sex;
    public static int x;

    public ExternalizeParent(String name, int age) {
        this.name = name;
        this.age = age;
        this.sex = "男";
        x = 1;
    }

    public ExternalizeParent() {
    }


    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        System.out.println("序列化,调用writeExternal");
        // transient对象对于Externalizable是可以序列化的
        out.writeObject(this.name);
        out.writeInt(this.age);
        out.writeUTF(this.sex);
        out.writeInt(x);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        System.out.println("反序列化,调用readExternal");
        // 必须和writeExternal的写入的内容一致，也就是上面是writeObject，那么下面对应的得是readObject
        // 还有就是上面写入了什么，下面的才能读取什么，你可以不读，但是要读的变量必须在此之前得写
        this.name = (String) in.readObject();
        this.age = in.readInt();
        this.sex = in.readUTF();
        x = in.readInt();
    }

    @Override
    public String toString() {
        return "ExternalizeParent{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", x=" + x +
                '}';
    }
}