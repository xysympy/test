package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 看看null值的toString方法是什么
 * @date 2022/4/24 9:35
 */
public class TestNull {
    public static void main(String[] args) {
        NullTest instance = Singleton.getInstance();
        System.out.println(instance);
    }
}

/**
 * 空测试
 *
 * @author Administrator
 * @date 2022/04/24
 */
class NullTest {

}

class Singleton {

    /**
     * 空测试
     * 使用volatile是防止指令重排序
     * 在nullTest = new NullTest()中，实际上不是一步就生成的
     * 1. 开辟一个地址空间
     * 2. 初始化NullTest
     * 3. 赋值操作
     * 1和2操作肯定是不能互换的，但是2和3可能会发生指令重排序，也就是会造成，nullTest有了一个引用
     * 但是引用的地址只是一个地址，里面的NullTest还没有进行初始化，然后就会在
     * if(nullTest == null) 中，判断为false，也就是不为null，但是里面的还没有初始化
     * 所以就会报错
     */
    private volatile static NullTest nullTest;

    public static NullTest getInstance() {
        if (nullTest == null) {
            // synchronized (nullTest) 不使用这个的原因：synchronized锁住的对象不能是未实例化的，也就是为null的
            synchronized (NullTest.class) {
                if (nullTest == null) {
                    nullTest = new NullTest();
                }
            }
        }
        return nullTest;
    }
}