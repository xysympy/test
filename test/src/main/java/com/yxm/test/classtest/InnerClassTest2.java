package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 内部类调用测试
 * @date 2022/3/21 11:02
 */
public class InnerClassTest2 {
    public static void main(String[] args) {
        // 内部类的实例化
        InnerClassTest1 test1 = new InnerClassTest1();
        InnerClassTest1.Content content = test1.new Content();
        InnerClassTest1.StaticContent staticContent = new InnerClassTest1.StaticContent();
        // 通过类中实现的方法来达到获取对应的实例化
        InnerClassTest1.Content content1 = test1.getContent();
        InnerClassTest1.StaticContent aStatic = test1.getStatic();
    }
}
