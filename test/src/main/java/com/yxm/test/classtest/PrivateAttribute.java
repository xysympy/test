package com.yxm.test.classtest;

/**
 *
 * @author yxm
 * @description:
 * @date 2022/3/17 14:07
 * 偶然到String的方法，验证下
 * @see java.lang.String#compareTo(String)
 */
public class PrivateAttribute {
    private final char[] value;

    public PrivateAttribute() {
        value = new char[16];
    }

    public int compareTo(PrivateAttribute anotherString) {
        // 大概是因为private是类内可见
        // 传进来的是这个类对应的实例，但是还是这个类，而在这个类中，private是可见的
        // 所以最后这种式子是成立的
        int len1 = this.value.length;
        int len2 = anotherString.value.length;
        return len1 - len2;
    }
}
