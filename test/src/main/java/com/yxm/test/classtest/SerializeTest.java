package com.yxm.test.classtest;

import java.io.*;

/**
 * @author yxm
 * @description: 测试使用Serialize，序列化和反序列化
 * @date 2022/4/26 9:26
 */
public class SerializeTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        SerializeParent serializeParent = new SerializeParent("test", 18);
        System.out.println(serializeParent);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(serializeParent);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArray);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        SerializeParent readObject = (SerializeParent) objectInputStream.readObject();
        // 在反序列化之前进行修改静态变量的值，发现反序列化出来的sex的值变化了，说明就是直接用的jvm里面的值，而不是哪个对象
        SerializeParent.sex = 3;
        System.out.println(readObject);
        objectInputStream.close();
        byteArrayOutputStream.close();
        objectOutputStream.close();
        byteArrayInputStream.close();
        System.out.println("----------------------------------------------------------");
        // 这里测试的是父类没有无参构造器的时候会造成序列化失败
        // 因为在ObjectInputStream中会调用到ObjectStreamClass的getSerializableConstructor
        // 也就是反序列化的时候需要用到
        // 而这个方法会返回第一个非可序列化超类的子类可访问的no-arg构造函数，如果没有找到则返回null。
        // 在这儿就是序列化son2，会去找它的父类，如果父类没有无参构造的时候就会报错
        SerializeSon2 serializeSon2 = new SerializeSon2("test2", 18);
        System.out.println(serializeSon2);
        byteArrayOutputStream = new ByteArrayOutputStream(1024);
        objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(serializeSon2);
        byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        objectInputStream = new ObjectInputStream(byteArrayInputStream);
        SerializeSon2 readObject2 = (SerializeSon2) objectInputStream.readObject();
        System.out.println(readObject2);
        // 还有就是serialVersionUID 序列化id，在反序列化的时候会进行检测这个serialVersionUID 值是否和序列化的时候的值
        // 是否相同，如果不相同的话，就会反序列化不成功，所以一般都会生成一个值，只要不变就行（int类型）
        // 还有就是没有显式的生成这个变量和值的时候，会默认的生成一个，但是是会根据是否为同一次编译来比较是否是序列化id相同
        // 简单理解就是默认情况下，同一次编译生成的序列化id才会相同，否则就不同，然后就会反序列化失败
    }
}

class SerializeParent implements Serializable {
    private String name;
    private int age;
    public static int sex;

    public SerializeParent(String name, int age) {
        this.name = name;
        this.age = age;
        sex = 1;
    }

    @Override
    public String toString() {
        return "SerializeParent{" +
                "name='" + name + '\'' +
                ", age=" + age + '\'' +
                ", sex=" + sex +
                '}';
    }
}

class SerializeParent2 {
    private String name;
    private int age;
    private static int sex;

    public SerializeParent2(String name, int age) {
        this.name = name;
        this.age = age;
        sex = 1;
    }

    @Override
    public String toString() {
        return "SerializeParent2{" +
                "name='" + name + '\'' +
                ", age=" + age + '\'' +
                ", sex=" + sex +
                '}';
    }
}

class SerializeSon2 extends SerializeParent2 implements Serializable {

    private String name;
    private int age;

    public SerializeSon2(String name, int age) {
        super(name, age);
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "SerializeSon2{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}' + super.toString();
    }
}