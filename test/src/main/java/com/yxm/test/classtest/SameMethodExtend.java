package com.yxm.test.classtest;

import java.io.IOException;

/**
 * @author yxm
 * @description: 测试：父类和接口有相同的方法，但是是重载的那种
 * 预计：得重写接口的方法，但是还是能用父类的方法
 * 结果：和预计的一样
 * 还有就是如果接口和父类或者是父抽象类都有一个只有返回值不同的方法，那么就会造成，重写谁都不对的情况
 * 如果是接口和类的方法相同，但是抛出的异常不同的话，那么子类的实现只能是不抛出异常，这个时候才算是继承了两者
 * @date 2022/3/29 17:03
 */
public class SameMethodExtend {
    public static void main(String[] args) {
        SameMethodSonClass sonClass = new SameMethodSonClass();
        System.out.println(sonClass.append("1"));
        // System.out.println(((SameMethodParentClass) new SameMethodSonClass()).append("123"));
        // 普通的子类上转至父类的时候是正常的，但是上面的已经不行了，因为它不算是继承了
        System.out.println(((CommonParent) new CommonSon()).append("123"));
    }
}

class SameMethodParentClass {

    public String append(String s){
        // 之所以不根据返回值的类型作为重载的依据
        // int max(){} float(){}
        // 然后你调用 max();谁知道你调用的是哪个，而且，重载是编译时确定调用的是哪个方法的
         return s + "SameMethodParent append()";
        // return new StringBuffer(s).append("SameMethodParent append()");
    }

    public String other(String s) {
        return s + "SameMethodParent other()";
    }
}

abstract class SameMethodParentAbstract {
    /**
     * 附加
     *
     * @param sb 某人
     * @return {@link String}
     */
    public abstract String append(StringBuffer sb);
}

interface SameMethodParentInterface {
    /**
     * 附加
     *
     * @param sb 某人
     * @return {@link String}
     */
    public String append(StringBuffer sb);
}

class SameMethodSonClass extends SameMethodParentClass implements SameMethodParentInterface{

    @Override
    public String append(StringBuffer sb){
        return sb + ("SameMethodSonClass").toString();
    }
}

class CommonParent {
    public String append(String s) {
        // 之所以不根据返回值的类型作为重载的依据
        // int max(){} float(){}
        // 然后你调用 max();谁知道你调用的是哪个，而且，重载是编译时确定调用的是哪个方法的
        return s + "SameMethodParent append()";
    }
}

class CommonSon extends CommonParent {

}