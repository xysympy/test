package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 方法的参数是数组
 * @date 2022/3/16 16:49
 */
public class ArrayParameter {

    public static void main(String[] args) {
        // arrayParam({1, 2, 3}); 编译器报错
        int[] arr = {1, 2, 3};
        arrayParam(arr);
        arrayParam(new int[]{1, 2, 3});
        Integer[] arr1 = {1, 2, 3};
        arrayParam(arr1);
        Double[] arr2 = {1D, 2D, 3D};
        arrayParam(arr2);
        arrayParam(1);
    }

    private static void arrayParam(int[] arr) {
        for (int i : arr) {
            System.out.println(i);
        }
    }

    private static void arrayParam(Object... arr) {
        for (Object i : arr) {
            System.out.println(i);
        }
    }

    private static void arrayParam(Object arr) {
        System.out.println("obj" + arr);
    }

    private static void arrayParam(Integer[] arr) {
        for (int i : arr) {
            System.out.println(i);
        }
    }
}
