package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 测试继承
 * @date 2021/11/29 10:28
 */
public class TestExtend {
    public static void main(String[] args) {
        AbstractParent abstractParent = new ExtendSon();
        abstractParent.doFilter();
    }
}

abstract class AbstractParent{
    public void doFilter(){
        System.out.println("doFilter-------start");
        doInternal();
        System.out.println("doFilter-------end");
    }

    /**
     * 给子类实现
     */
    public abstract void doInternal();
}

class ExtendSon extends AbstractParent{

    @Override
    public void doInternal() {
        System.out.println("son-----doInternal-----");
    }
}

