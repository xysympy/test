package com.yxm.test.classtest;

/**
 * @author yxm
 * @description: 测试下接口的default方法
 * @date 2022/3/18 15:15
 */
public class DefaultMethodTest implements Jim1, Jim2{
    @Override
    public void jim() {
        // 记住这个用法吧
        Jim2.super.jim();
    }

    public static void main(String[] args) {
        // new DefaultMethodTest().jim();
    }

}

abstract class Jim {
    private int anInt;
}

interface Jim1 {
    default void jim() {
        System.out.println("Jim1:jim");
    }
}

interface Jim2 {
    default void jim() {
        System.out.println("Jim2:jim");
    }
}

class MainTest {
    public static void main(String[] args) {
    }
}