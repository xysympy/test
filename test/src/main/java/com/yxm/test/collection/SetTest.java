package com.yxm.test.collection;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * @author yxm
 * @description: Set集合测试
 * @date 2022/3/22 13:25
 */
public class SetTest {
    public static void main(String[] args) {
        Set<Integer> integers = new HashSet<>();
        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            integers.add(random.nextInt(100));
        }
        System.out.println(integers);
    }
}
