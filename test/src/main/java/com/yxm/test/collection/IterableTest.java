package com.yxm.test.collection;

import java.util.Iterator;

/**
 * @author yxm
 * @description: 任何实现了iterable接口的类都能使用for-in
 * 为什么有Iterator还需要Iterable
 * 我们看到Iterator其实已经有很多处理集合元素相关的方法了，为什么还需要抽象一层呢？很多集合不直接实现Iterator接口，而是实现Iterable?
 *
 * 1.Iterator接口的核心方法next()或者hashNext()，previous()等，都是严重依赖于指针的，也就是迭代的目前的位置。
 * 如果Collection直接实现Iterator接口，那么集合对象就拥有了指针的能力，内部不同方法传递，就会让next()方法互相受到阻挠。
 * 只有一个迭代位置，互相干扰。
 * 2.Iterable 每次获取迭代器，就会返回一个从头开始的，不会和其他的迭代器相互影响。
 * 3.这样子也是解耦合的一种，有些集合不止有一个Iterator内部类，可能有两个，比如ArrayList，LinkedList，
 * 可以获取不同的Iterator执行不一样的操作。
 * @date 2022/3/22 14:08
 */
public class IterableTest {
    public static void main(String[] args) {
        IterableImpl iterableImpl = new IterableImpl();
        for (Object o : iterableImpl) {
            
        }
    }
}

class IterableImpl implements Iterable {

    @Override
    public Iterator iterator() {
        return new Iterator() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public Object next() {
                return null;
            }
        };
    }
}