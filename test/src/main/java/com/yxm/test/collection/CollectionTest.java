package com.yxm.test.collection;

import java.util.*;

/**
 * @author yxm
 * @description: 测试下Arrays和Collections
 * @date 2022/3/21 16:46
 */
public class CollectionTest {
    public static void main(String[] args) {
        // 因为这个实际上是Arrays的内部类ArrayList
        // 内部类没有重写add方法，所以在进行add的时候就会执行abstractList的add方法
        // 但是，默认的是需要重写的，没有重写的话，就会导致报错throw new UnsupportedOperationException();
        List<Integer> integers = Arrays.asList(1, 2, 3);
        // integers.add(4);
        List<Integer> integers1 = new ArrayList<>();
        Collections.addAll(integers1, 1, 2, 3, 4);
        integers1.add(5);
        Iterator<Integer> iterator = integers1.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() == 2) {
                iterator.remove();
            }
        }
        System.out.println(integers1);
    }
}
