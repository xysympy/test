package com.yxm.test.generic;

/**
 * @author yxm
 * @description: 测试使用泛型的自限定
 * 限定泛型对应的参数，也就是T为当前定义的对象
 * @date 2022/4/18 16:52
 */
public class SelfLimiting {
    public static void main(String[] args) {
        SelfTExtends selfTExtends = new SelfTExtends();
        selfTExtends.set(new SelfTExtends());
        System.out.println(selfTExtends.get());
    }
}


class SelfT<T> {
    private T element;

    public void set(T t) {
        this.element = t;
    }
    public T get(){
        return this.element;
    }
}

class SelfTExtends extends SelfT<SelfTExtends> {

}

class SelfLimit<T extends SelfLimit<T>> {
    private T element;

    public void set(T t) {
        this.element = t;
    }
    public T get(){
        return this.element;
    }
}

class SelfLimitExtends extends SelfLimit<SelfLimitExtends> {
    // 实际上转换出来就是 SelfLimit<SelfLimitExtends extends SelfLimit<SelfLimitExtends>>
    // 也就是当且仅当子类是自限定的格式的时候才满足父类定义好的泛型

    // 好处就是通过这么限定了之后，你在父类写的T就会随着子类的变化而变化，
    // 甚至于你可以不用重写父类的代码就能获取到子类的信息
}