package com.yxm.test.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author yxm
 * @description: 测试下泛型的范围
 * @date 2022/4/18 10:51
 */
public class GenericTest {
    public static void main(String[] args) {
        List<? extends Fruit> list = new ArrayList<>();
        /* 虽然上面的意思是list里面存储Fruit的继承类，但是实际上都是没有办法添加进去的
        它需要的是一个确定的类，而不是像这些这样的不确定的类，编译器会报错，但是一般是只有
        像add这些，需要传入通配符的方法会出现编译器错误
        list.add(new Fruit())
        list.add(new Apple());
        list.add(new Orange());*/
        // Fruit fruit1 = list.get(0);
        List<? extends Fruit> temp = Collections.singletonList(new Apple());
        Fruit fruit = temp.get(0);
        System.out.println(temp.contains((Apple) fruit));
        List<? super Fruit> listSuper = new ArrayList<>();
        listSuper.add(new Fruit());
        listSuper.add(new Apple());
        // listSuper.add(new GenericTest()); 无法添加，因为还是有下限的
        // Object object = listSuper.get(0);
        System.out.println(listSuper);
        // super确定的是下限，extends确定的是上限
        // 对于同一个add方法而言，确定了上限仅仅能表示这个列表里面存储的数据是什么类型或者是该类型的子类型，但是没有
        // 办法确定是哪一个具体的类型；如果确定的是下限的话，说明里面的数据最少也是这个类型或者是这个类型的子类型
        // 转换一下：? extends Fruit就是 ? 而 ? super Fruit 就是Object，一个是不确定类型，一个是类型范围很大
        // 但是对于返回而言，也就是get方法，前者是返回Fruit，而后者返回的是Object对象
        // 所以使用的方式就是，如果你是多读少写的时候就用前者，因为能确定返回值的类型，如果是多写少读的话，就用后者
    }
}

class Fruit {

}

class Apple extends Fruit {

}

class Orange extends Fruit {

}

class Scope<T extends Fruit> {

}