package com.yxm.test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: lambda测试
 * @Author yxm
 * @Date 2021/10/29 14:53
 */
public class LambdaTest {

    public static void main(String[] args) {

        //测试数据
        List<Person> personList = getData(1);
        Long start = System.currentTimeMillis();
        integerList(personList);
        Long end = System.currentTimeMillis();
        System.out.println("lambda时间：" + (end - start));
        start = System.currentTimeMillis();
        getList(personList);
        end = System.currentTimeMillis();
        System.out.println("普通方法时间：" + (end - start));
        personList = getData(10);
        start = System.currentTimeMillis();
        integerList(personList);
        end = System.currentTimeMillis();
        System.out.println("lambda时间：" + (end - start));
        start = System.currentTimeMillis();
        getList(personList);
        end = System.currentTimeMillis();
        System.out.println("普通方法时间：" + (end - start));
        personList = getData(100);
        start = System.currentTimeMillis();
        integerList(personList);
        end = System.currentTimeMillis();
        System.out.println("lambda时间：" + (end - start));
        start = System.currentTimeMillis();
        getList(personList);
        end = System.currentTimeMillis();
        System.out.println("普通方法时间：" + (end - start));
        personList = getData(1000);
        start = System.currentTimeMillis();
        integerList(personList);
        end = System.currentTimeMillis();
        System.out.println("lambda时间：" + (end - start));
        start = System.currentTimeMillis();
        getList(personList);
        end = System.currentTimeMillis();
        System.out.println("普通方法时间：" + (end - start));
        personList = getData(10000);
        start = System.currentTimeMillis();
        integerList(personList);
        end = System.currentTimeMillis();
        System.out.println("lambda时间：" + (end - start));
        start = System.currentTimeMillis();
        getList(personList);
        end = System.currentTimeMillis();
        System.out.println("普通方法时间：" + (end - start));
        personList = getData(100000);
        start = System.currentTimeMillis();
        integerList(personList);
        end = System.currentTimeMillis();
        System.out.println("lambda时间：" + (end - start));
        start = System.currentTimeMillis();
        getList(personList);
        end = System.currentTimeMillis();
        System.out.println("普通方法时间：" + (end - start));
        personList = getData(1000000);
        start = System.currentTimeMillis();
        integerList(personList);
        end = System.currentTimeMillis();
        System.out.println("lambda时间：" + (end - start));
        start = System.currentTimeMillis();
        getList(personList);
        end = System.currentTimeMillis();
        System.out.println("普通方法时间：" + (end - start));
        personList = getData(10000000);
        start = System.currentTimeMillis();
        integerList(personList);
        end = System.currentTimeMillis();
        System.out.println("lambda时间：" + (end - start));
        start = System.currentTimeMillis();
        getList(personList);
        end = System.currentTimeMillis();
        System.out.println("普通方法时间：" + (end - start));

    }

    private static class Person{
        private Integer age;
        private String name;

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     * 使用lambda表达式的方法
     *
     * @return {@link List}<{@link Integer}>
     */
    private static List<Integer> integerList(List<Person> personList){
        return personList.stream().map(Person::getAge).collect(Collectors.toList());
    }

    /**
     * 使用普通的方法添加数据
     *
     * @param personList 人列表
     * @return {@link List}<{@link Integer}>
     */
    private static List<Integer> getList(List<Person> personList){
        List<Integer> result = new ArrayList<>();
        for (Person person : personList) {
            result.add(person.getAge());
        }
        return result;
    }

    /**
     * 获取数据
     *
     * @param num 全国矿工工会
     * @return {@link List}<{@link Person}>
     */
    private static List<Person> getData(int num){
        List<Person> personList = new ArrayList<>();
        for(int i = 0; i < num; i++){
            Person person = new Person();
            person.setAge(i);
            person.setName("person:" + i);
            personList.add(person);
        }
        return personList;
    }

}