package com.yxm.test.freemarker;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 批量生成数据库表语句
 *
 * @author yxm
 * @date 2022/06/16
 */
public class Init {
    private final static int totalTable = 100;

    public static void main(String[] args) throws IOException, TemplateException {
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File("C:\\Users\\Administrator\\IdeaProjects\\f2s\\f2s-sql\\src\\main\\resources"));
        cfg.setDefaultEncoding("utf-8");
        Template template = cfg.getTemplate("sql2.ftlh");
        List<Integer> data = new ArrayList<>(totalTable);
        for (int i = 0; i < totalTable; i++) {
            data.add(i);
        }
        Map<String, Object> model = new HashMap<>(1);
        model.put("data", data);
        template.process(model, new FileWriter("C:\\Users\\Administrator\\IdeaProjects\\f2s\\f2s-sql\\src\\main\\resources\\create2.sql"));
    }
}