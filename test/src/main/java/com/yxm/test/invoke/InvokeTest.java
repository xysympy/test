package com.yxm.test.invoke;

import java.lang.reflect.Field;

/**
 * @author yxm
 * @description: 测试下反射
 * @date 2022/3/29 14:58
 */
public class InvokeTest {
    public static void main(String[] args) {
        for (Field declaredField : InvokeSon.class.getDeclaredFields()) {
            // 只是为了证明，父类的私有变量是没有办法通过反射获取到的
            System.out.println(declaredField.getName());
        }
    }
}
class InvokeParent {
    private String username;
    private String password;
}

class InvokeSon extends InvokeParent {
    private String name;
    private String pass;
}