package com.yxm.test;

import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;

/**
 * @author yxm
 * @description: 测试使用
 * @date 2021/11/5 10:27
 */
public class Test {
    public static void main(String[] args) throws Exception {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        Method add = list.getClass().getMethod("add", Object.class);
        add.invoke(list, "a");
        add.invoke(list, 2);
        add.invoke(list, "b");
        //请在此处补全代码

        System.out.println(list); //期望输出为：[1, a, 2, b]
        Field id = Son.class.getField("id");
        for (Field field : Son.class.getDeclaredFields()) {
            System.out.println(field.getName());
        }
        // this$0就是内部类指向外部类的一个属性
        System.out.println("---------------------------------------------------");
        for (Field field : Son.class.getFields()) {
            System.out.println(field.getName());
        }
        System.out.println(id.getType());

        System.out.println("**************************************************");
        Map<String, String> map = new HashMap<>();
        System.out.println(map.put("a", "a"));
        System.out.println(map.putIfAbsent("b", "b"));
        System.out.println(map.putIfAbsent("b", "b"));
        System.out.println("-------------------------------------------------");
        PrintStream out = System.out;
        out.println("我");
        //out.close();
        out.println("爱");
        out.println("中");
        out.println("国");
        System.out.println("-------------------------------------------------");
        Map<String, String> map1 = new HashMap<>();
        System.out.println(map1.put("a", "a"));
        System.out.println(map1.putIfAbsent("b", "b"));
        System.out.println(map1.putIfAbsent("b", "c"));
        //put的用法
        System.out.println(map1.put("c", "c"));
        System.out.println(map1.put("d", "d"));
        System.out.println(map1.put("c", "d"));
        System.out.println(map1.put("c", "d"));
        System.out.println("================普通for循环：");
        for (int i = 0; i < getData().size(); i++) {
            System.out.println(i);
        }
        System.out.println("================增强for循环：");
        for (Integer i : getData()) {
            System.out.println(i);
        }
        System.out.println("================Java8提供的foreach循环：");
        getData().forEach(System.out::println);
        System.out.println("==============================================");
        int[] src = new int[]{1, 2, 3, 4, 5};
        int[] dest = new int[5];
        System.arraycopy(src, 0, dest, 0, 3);
        System.out.println(Arrays.toString(dest));

        // 实际上Arrays.asList是返回一个内部类，不能强制转换成ArrayList
        //ArrayList<Integer> integers = (ArrayList<Integer>) Arrays.asList(1, 2, 3, 4);

        //定义自己的线程池
        ForkJoinPool pool = new ForkJoinPool(10);

        String str = "my name is fangshixiang";
        pool.execute(() -> Stream.of(str.split(" ")).parallel().peek(x -> System.out.println(Thread.currentThread().getName() + "___" + x))
                .map(x -> x.length()).count());
        pool.shutdown(); //关闭线程池（一般都不需要关的）

        new Thread(() -> {
            {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("run");
            }
        }).start();
        //下面代码是为了让main线程不要退出，因为如果退出太早，上面不会有东西输出的
        //提示一点 wait外面必须有synchronized关键字，因为没有会报错的
        /*synchronized (pool) {
            try {
                pool.wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }*/
    }

    private static List<Integer> getData() {
        System.out.println("getDate被调用...");
        List list = new ArrayList<Integer>();
        list.add(10);
        list.add(20);
        list.add(30);
        return list;
    }

    class Son extends BaseEntity<Integer> {
        public String id;
        public String name;
        final String fin;

        Son(String fin) {
            super(fin);
            this.fin = fin;
        }
    }

    class BaseEntity<PK extends Number> {
        public PK id;
        final String test;

        BaseEntity(String test) {
            this.test = test;
        }
    }

}
