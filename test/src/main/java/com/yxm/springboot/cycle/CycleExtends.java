package com.yxm.springboot.cycle;


import org.springframework.stereotype.Component;

/**
 * @author yxm
 * @description:
 * @date 2022/5/7 17:24
 */
@Component
public class CycleExtends extends Cycle {

    public CycleExtends(CycleService2 cycleService2) {
        super(cycleService2);
    }
}
