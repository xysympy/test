package com.yxm.springboot.cycle;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author yxm
 * @description: 经过实践：SpringBoot2.6.x开始就开始将默认不支持循环依赖解决了
 *                      但是2.5.x还是支持的，所以将当前的版本切换成2.6.x之后
 *                      再重新build编译后，就会出现无法解决循环依赖了
 * @date 2022/5/7 17:23
 */

public abstract class Cycle implements ApplicationContextAware {

    private final CycleService2 cycleService2;
    private static ApplicationContext applicationContext;
    // private Cycle cycle = applicationContext.getBean(Cycle.class);

    protected Cycle(CycleService2 cycleService2) {
        this.cycleService2 = cycleService2;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        Cycle.applicationContext = applicationContext;
    }

    /**
     * 之后尽量避免循环依赖的产生，如果需要用到当前对象的，或者成员变量会出现A->B B->A这种现象的话
     * 就将循环依赖出现的变量变成下面这种获取方式，采用局部变量的方式来解决
     * 或者是配置下全局配置，也就是开启循环依赖解决
     *  spring.main.allow-circular-references: true
     * @param tClass t类
     * @return {@link T}
     */
    public static <T> T getBean(Class<T> tClass) {
        return applicationContext.getBean(tClass);
    }
}
