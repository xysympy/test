package com.yxm.springboot.cycle;

import org.springframework.stereotype.Component;

/**
 * @author yxm
 * @description: 测试循环依赖
 * @date 2022/5/7 17:14
 */
@Component
public class CycleService2 {
    private final CycleService1 cycleService1;

    public CycleService2(CycleService1 cycleService1) {
        this.cycleService1 = cycleService1;
    }
}
