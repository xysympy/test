package com.yxm.springboot.test.service.impl;

import com.yxm.springboot.test.service.ManyConstructorService;
import com.yxm.springboot.test.service.ManyConstructorService2;
import com.yxm.springboot.test.service.ManyConstructorService3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author yxm
 * @description: 实现类
 * @date 2022/3/21 10:21
 */
@Service
public class ManyConstructorServiceImpl implements ManyConstructorService {

    private ManyConstructorService manyConstructorService;
    private ManyConstructorService2 manyConstructorService2;
    private ManyConstructorService3 manyConstructorService3;

    /*@Autowired
    构造器注入的循环依赖没有办法解决
    public ManyConstructorServiceImpl(ManyConstructorService manyConstructorService
            , ManyConstructorService2 manyConstructorService2
            , ManyConstructorService3 manyConstructorService3) {
        this.manyConstructorService = manyConstructorService;
        this.manyConstructorService2 = manyConstructorService2;
        this.manyConstructorService3 = manyConstructorService3;
    }*/

    @Autowired(required = false)
    public ManyConstructorServiceImpl(ManyConstructorService2 manyConstructorService2
            , ManyConstructorService3 manyConstructorService3) {
        // 如果上面的两个类都有的话，那么优先是这个，也就是匹配度最高
        this.manyConstructorService2 = manyConstructorService2;
        this.manyConstructorService3 = manyConstructorService3;
    }

    @Autowired(required = false)
    public ManyConstructorServiceImpl(ManyConstructorService3 manyConstructorService3) {
        this.manyConstructorService3 = manyConstructorService3;
    }

    @Autowired(required = false)
    public ManyConstructorServiceImpl() {
        // 如果都没有autowired注解的话，那么优先是无参
        // 如果有autowired注解，并且required=false的话，那么优先的还是匹配度高的，无参的匹配度并没有有参的
        // 并且有传入参数的bean对象匹配度高
        // 貌似只能是全部都是required=false的时候才行，其中一个为true就会报错
    }
}
