package com.yxm.springboot.test.service.impl;

import com.yxm.springboot.test.service.ManyConstructorService3;
import org.springframework.stereotype.Service;

/**
 * @author yxm
 * @description: 实现类
 * @date 2022/3/21 10:21
 */
@Service
public class ManyConstructorServiceImpl3 implements ManyConstructorService3 {
}
