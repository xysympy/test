package com.yxm.springboot.test.domain.dto;

import lombok.Data;

@Data
public class SdcLogsAppApprovalDeviceDelDto {

    /**
     * 应用实例ID
     */
    private String appInstId;

    /**
     * 测试设备id数组
     */
    private String[] deviceId;
}