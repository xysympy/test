package com.yxm.springboot.test.service;

import com.yxm.test.annotation.Service;

/**
 * @author yxm
 * @description: 多个构造方法
 * @date 2022/3/21 10:20
 */
@Service
// 注解放在上面虽然是可以的，但是并没有用，不会进行自动注入，也获取不到
public interface ManyConstructorService2 {
}
