package com.yxm.springboot.test.controller;

import com.yxm.springboot.test.domain.dto.SdcAppInstListDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/24 20:13
 */
@RestController
public class TestDtoController {

    @PostMapping("/dto")
    public void dto(SdcAppInstListDto dto){
        System.out.println(dto);
    }
}
