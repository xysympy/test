package com.yxm.springboot.test.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/23 14:45
 */
@RestController
public class TestDispatcherController {

    private final HttpServletRequest httpServletRequest;
    private final HttpServletResponse httpServletResponse;

    public TestDispatcherController(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        this.httpServletRequest = httpServletRequest;
        this.httpServletResponse = httpServletResponse;
    }

    @RequestMapping("test")
    public void failure() throws ServletException, IOException {
        httpServletRequest.getRequestDispatcher("/test.html").forward(httpServletRequest, httpServletResponse);
    }
}
