package com.yxm.springboot.test.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author yxm
 * @description: 测试下上传文件
 * @date 2022/3/22 15:38
 */
@RestController
@RequestMapping("/upload")
public class UploadController {

    @PostMapping("/test")
    public String test(@RequestParam MultipartFile file) {
        File dest = new File("resource/upload" + file.getOriginalFilename());
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
        return "success";
    }
}
