package com.yxm.springboot.test.controller;

import com.yxm.springboot.test.domain.dto.SdcLogsAppApprovalDeviceDelDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/24 14:52
 */
@RestController
public class TestArrayController {

    @PostMapping("/array")
    public void test(HttpServletRequest request){
        Map<String, String[]> parameterMap = request.getParameterMap();
        parameterMap.forEach((key, value) ->{
            System.out.println("key: " + key + ",value: " + value[0]);
        });
    }

    @PostMapping("/array1")
    public void test1(SdcLogsAppApprovalDeviceDelDto dto){
        System.out.println(dto.getDeviceId()[1]);
    }
}
