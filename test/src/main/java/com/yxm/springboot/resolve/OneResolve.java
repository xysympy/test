package com.yxm.springboot.resolve;

import com.yxm.springboot.transcation.service.TestService;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/18 13:14
 */
@Component
public class OneResolve implements HandlerMethodArgumentResolver {

    @Resource
    private TestService testService;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return true;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        return null;
    }
}
