package com.yxm.springboot.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yxm
 * @description: 测试使用拦截器情况
 * @date 2021/11/10 9:20
 */
@Slf4j
public class OneInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("one interceptor preHandle");
        int i = 1 / 0;
        return true;
    }
}
