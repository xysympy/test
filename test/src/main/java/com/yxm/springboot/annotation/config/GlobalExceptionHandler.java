package com.yxm.springboot.annotation.config;

import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Iterator;
import java.util.StringJoiner;

/**
 * @author yxm
 * @description:
 * @date 2022/2/26 13:33
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {BindException.class})
    public Object bindExceptionHandler(BindException bindException) {
        StringJoiner stringJoiner = new StringJoiner(";");
        Iterator<FieldError> errorIterator = bindException.getFieldErrors().iterator();
        while (errorIterator.hasNext()) {
            FieldError error = errorIterator.next();
            stringJoiner.add(error.getField() + error.getDefaultMessage());
        }
        return stringJoiner.toString();
    }
}
