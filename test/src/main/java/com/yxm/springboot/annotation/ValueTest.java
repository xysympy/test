package com.yxm.springboot.annotation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @ClassName: ValueTest
 * @Description: 测试一下@Value注解
 * @Author: yxm
 * @Date: 2022/1/16 9:46
 * @Version: 1.0
 **/
@Component
public class ValueTest {

    private static String name;

    @Value("${test.name:test_name}")
    public void setName(String nameTest) {
        name = nameTest;
    }

    public static String getValueName(){
        return name;
    }
}
