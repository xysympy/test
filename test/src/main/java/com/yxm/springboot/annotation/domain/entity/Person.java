package com.yxm.springboot.annotation.domain.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.util.List;

/**
 * @author yxm
 * @description: 实体类
 * @date 2022/2/26 11:07
 */
@Data
public class Person {
    /**
     * 名字
     */
    @NotNull(message = "not null")
    @NotBlank(message = "not blank")
    private String name;
    /**
     * 年龄
     */
    @NotNull
    @Max(150)
    @Min(0)
    private Integer age;
    /**
     * 爱好
     */
    @NotNull
    @Size(min = 0, max = 100)
    private List<@NotBlank String> hobbies;
}
