package com.yxm.springboot.annotation.controller;

import com.yxm.springboot.annotation.domain.entity.Person;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author yxm
 * @description: 测试Validated注解
 * @date 2022/2/25 17:27
 */
@RestController
@RequestMapping("/validated")
public class ValidatedTest {

    @PostMapping("/test")
    public Boolean test(@RequestBody @Validated Person person) {
        System.out.println(person);
        return Boolean.TRUE;
    }
}
