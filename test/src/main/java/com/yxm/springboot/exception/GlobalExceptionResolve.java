package com.yxm.springboot.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author yxm
 * @description: 全局异常捕获
 * @date 2021/11/10 9:36
 */
@Slf4j
//@RestControllerAdvice
public class GlobalExceptionResolve {

    @ExceptionHandler(Exception.class)
    public String testException(Exception e) {
        log.info("testException");
        //int i = 1 / 0;
        return "testException";
    }
}
