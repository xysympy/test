package com.yxm.springboot.dependency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author yxm
 * @description: 测试循环依赖
 * @date 2022/1/11 15:55
 */
//@Component
public class CircularDependency2 {

    private final CircularDependency circularDependency;

    public CircularDependency2(CircularDependency circularDependency) {
        this.circularDependency = circularDependency;
    }
}
