package com.yxm.springboot.dependency;

import org.springframework.stereotype.Component;

/**
 * @author yxm
 * @description: 测试下循环依赖
 * @date 2022/1/11 15:54
 */
//@Component
public class CircularDependency {

    private final CircularDependency2 circularDependency2;

    public CircularDependency(CircularDependency2 circularDependency2) {
        this.circularDependency2 = circularDependency2;
    }
}
