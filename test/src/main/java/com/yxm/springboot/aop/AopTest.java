package com.yxm.springboot.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/8 17:01
 */
@Aspect
@Component
public class AopTest {

    @Pointcut("execution(* com.yxm.springboot.transcation.service.*.*(..)) ")
    public void point() {
    }
    @Before("point()")
    public void before() {
        System.out.println("this is from HelloAspect#before...");
    }
    @After("point()")
    public void after() {
        System.out.println("this is from HelloAspect#after...");
    }
    @AfterReturning("point()")
    public void afterReturning() {
        System.out.println("this is from HelloAspect#afterReturning...");
    }
    @AfterThrowing("point()")
    public void afterThrowing() {
        System.out.println("this is from HelloAspect#afterThrowing...");
    }
    // 此处需要注意：若写了@Around方法，那么最后只会执行@Around 其它的都不会执行
    // 加了这个就都会执行（proceedingJoinPoint.proceed()）
//    @Around("point()")
//    public void around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//        System.out.println("this is from HelloAspect#around...");
//        //proceedingJoinPoint.proceed();
//        //System.out.println("around after");
//    }
}
