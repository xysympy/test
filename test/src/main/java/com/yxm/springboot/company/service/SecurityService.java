package com.yxm.springboot.company.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.jwt.JWT;
import com.yxm.springboot.company.config.CommonConfig;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * @author yxm
 * @description: 校验
 * @date 2022/3/30 14:00
 */
@Service
public class SecurityService {

    private final CommonConfig commonConfig;


    public SecurityService(CommonConfig commonConfig) {
        this.commonConfig = commonConfig;
    }

    public String generateJwtToken(String account) {
        return JWT.create().setIssuer("company.standard")
                .setKey(commonConfig.getJwtKey().getBytes(StandardCharsets.UTF_8))
                .setPayload("account", account)
                .setExpiresAt(DateUtil.offsetDay(new Date(), commonConfig.getTtl()))
                .sign();
    }

    public boolean verifyJwtToken(String accessToken) {
        return JWT.of(accessToken)
                .setKey(commonConfig.getJwtKey().getBytes(StandardCharsets.UTF_8))
                .verify();
    }
}
