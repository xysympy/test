package com.yxm.springboot.company.intercept;

import com.yxm.springboot.company.dto.HeadDto;
import com.yxm.springboot.company.service.SecurityService;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yxm
 * @description: 拦截器
 * @date 2022/3/30 13:45
 */
// @Component
public class TokenInterceptor implements HandlerInterceptor {

    private final SecurityService securityService;

    public TokenInterceptor(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            if ("login".equals(handlerMethod.getMethod().getName())) {
                return true;
            }
            HeadDto headDto = HeadDto.newInstance(request);
            if (securityService.verifyJwtToken(headDto.getAuthorization())) {

            }
        }
        return false;
    }

}
