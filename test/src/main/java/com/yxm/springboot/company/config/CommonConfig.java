package com.yxm.springboot.company.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author yxm
 * @description: 配置所需的配置项
 * @date 2022/3/30 10:31
 */
@Configuration
@ConfigurationProperties(prefix = "company.standard")
public class CommonConfig {

    /**
     * RSA公钥
     */
    private String publicKey;
    /**
     * RSA私钥
     */
    private String privateKey;
    /**
     * jwt秘钥
     */
    private String jwtKey;
    /**
     * ttl
     */
    private Integer ttl;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getJwtKey() {
        return jwtKey;
    }

    public void setJwtKey(String jwtKey) {
        this.jwtKey = jwtKey;
    }

    public Integer getTtl() {
        return ttl;
    }

    public void setTtl(Integer ttl) {
        this.ttl = ttl;
    }
}
