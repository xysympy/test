package com.yxm.springboot.company.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yxm
 * @description:
 * @date 2022/3/30 14:24
 */
@RestController
@RequestMapping("/company")
public class CompanyController {

    @PostMapping("/login")
    public String login() {
        return "";
    }
}
