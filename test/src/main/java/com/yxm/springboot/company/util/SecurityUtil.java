package com.yxm.springboot.company.util;

import cn.hutool.core.codec.Base64Decoder;
import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.symmetric.AES;

/**
 * @author yxm
 * @description: 进行一些Aes加解密和RSA加解密的工具类
 * @date 2022/3/30 10:20
 */
public class SecurityUtil {

    public static String aesDecode(String aesKey, String data) {
        // 一般而言，aesKey都是经过base64之后的值
        byte[] decode = Base64Decoder.decode(aesKey);
        AES aes = new AES(Mode.ECB, Padding.PKCS5Padding, decode);
        return aes.decryptStr(data);
    }

    public static String aesEncode(String aesKey, String data) {
        byte[] decode = Base64Decoder.decode(aesKey);
        AES aes = new AES(Mode.ECB, Padding.PKCS5Padding, decode);
        // 因为直接使用new String(byte[])会产生乱码
        // 因为此时的byte[]数组是加密后的结果，如果直接转换成字符串的话，就会造成乱码
        // 还有就是存储的问题，一个字节=两个十六进制。但是我认为主要还是防止乱码的问题
        return aes.encryptHex(data);
    }

    public static String rsaDecode(String privateKey, String data) {
        RSA rsa = new RSA(privateKey, null);
        return rsa.decryptStr(data, KeyType.PrivateKey);
    }

}
