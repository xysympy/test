package com.yxm.springboot.company.dto;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yxm
 * @description: 存储部分参数
 * @date 2022/3/30 15:33
 */
public class HeadDto {
    /**
     * 秘钥
     */
    private String key;
    /**
     * 授权令牌
     */
    private String authorization;

    public String getKey() {
        return key;
    }

    public String getAuthorization() {
        return authorization;
    }

    public static HeadDto newInstance(HttpServletRequest request) {
        String key = request.getHeader("key");
        String authorization = request.getHeader("authorization");
        HeadDto headDto = new HeadDto();
        headDto.key = key;
        headDto.authorization = authorization;
        return headDto;
    }

    @Override
    public String toString() {
        return "HeadDto{" +
                "key='" + key + '\'' +
                ", authorization='" + authorization + '\'' +
                '}';
    }
}
