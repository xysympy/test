package com.yxm.springboot.company.config;

import com.yxm.springboot.company.intercept.TokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author yxm
 * @description:
 * @date 2022/3/30 14:29
 */
//@Configuration
public class BaseConfig implements WebMvcConfigurer {

    private final TokenInterceptor tokenInterceptor;

    public BaseConfig(TokenInterceptor tokenInterceptor) {
        this.tokenInterceptor = tokenInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 这个也是注册拦截器，不过对应的功能和HandlerInterceptor不太一样
        // registry.addWebRequestInterceptor()
        registry.addInterceptor(tokenInterceptor);
    }
}
