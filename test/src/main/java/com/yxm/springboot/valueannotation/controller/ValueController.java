package com.yxm.springboot.valueannotation.controller;

import com.yxm.springboot.valueannotation.service.ValueService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yxm
 * @description:
 * @date 2022/4/1 11:27
 */
@RestController
@RequestMapping("/value")
public class ValueController {

    private final ValueService valueService;

    public ValueController(ValueService valueService) {
        this.valueService = valueService;
    }

    @GetMapping("test")
    public void test() {
        valueService.print();
    }
}
