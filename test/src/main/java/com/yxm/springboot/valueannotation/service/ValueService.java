package com.yxm.springboot.valueannotation.service;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author yxm
 * @description: 测试使用@Value注解和@Bean注解的使用
 * @date 2022/4/1 11:24
 */
public class ValueService {
    @Value("${value.annotation.value:value}")
    private String value;
    @Value("${value.annotation.key:key}")
    private String key;

    public void print() {
        System.out.println(key + ": " + value);
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
