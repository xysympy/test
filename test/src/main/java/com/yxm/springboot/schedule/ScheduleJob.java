package com.yxm.springboot.schedule;

import cn.hutool.core.thread.ThreadFactoryBuilder;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author yxm
 * @description: 测试在quartzJob中添加静态变量--threadPoolExecutor
 * @date 2022/5/20 13:37
 */
public class ScheduleJob extends QuartzJobBean {

    private static ThreadPoolExecutor threadPoolExecutor;


    @PostConstruct
    public void init() {
        if (threadPoolExecutor != null) {
            return;
        }
        ThreadFactory factory = ThreadFactoryBuilder.create()
                .setNamePrefix("Scheduled-Test-Job-").build();
        threadPoolExecutor = new ThreadPoolExecutor(100, 100, 0L, TimeUnit.SECONDS
                , new LinkedBlockingQueue<>(), factory);
    }

    @PreDestroy
    public void destroy() {
        if (threadPoolExecutor != null) {
            threadPoolExecutor.shutdown();
        }
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        for (int i = 0; i < 100; i++) {
            final int temp = i;
            threadPoolExecutor.execute(() -> System.out.println(Thread.currentThread().getName() + ": " + temp));
        }
    }
}
