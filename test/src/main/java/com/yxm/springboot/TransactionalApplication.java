package com.yxm.springboot;

import com.yxm.springboot.test.service.ManyConstructorService2;
import com.yxm.springboot.transcation.service.TransactionService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author Administrator
 * @Date 2021/10/26 11:24
 */
@SpringBootApplication
//@EnableAspectJAutoProxy(exposeProxy = true)
public class TransactionalApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(TransactionalApplication.class, args);
        // ManyConstructorService2 bean = applicationContext.getBean("manyConstructorService2", ManyConstructorService2.class);
    }
}
