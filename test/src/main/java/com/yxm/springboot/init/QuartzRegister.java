package com.yxm.springboot.init;

import com.yxm.springboot.schedule.ScheduleJob;
import org.quartz.*;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

/**
 * @author yxm
 * @description: quartz定时器任务注册
 * @date 2022/5/20 14:01
 */
@Component
public class QuartzRegister implements org.springframework.boot.ApplicationRunner {

    private final Scheduler scheduler;

    public QuartzRegister(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        JobDetail jobDetail = JobBuilder.newJob(ScheduleJob.class)
                .withIdentity("schedule", "schedule")
                .build();
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity("schedule", "schedule")
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(5)).startNow()
                .build();
        scheduler.scheduleJob(jobDetail, trigger);
        JobDetail jobDetail2 = JobBuilder.newJob(ScheduleJob.class)
                .withIdentity("schedule2", "schedule2")
                .storeDurably(true).build();
        Trigger trigger2 = TriggerBuilder.newTrigger()
                .withIdentity("schedule2", "schedule2").forJob(jobDetail2)
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(10)).startNow()
                .build();
        scheduler.addJob(jobDetail2, false);
        scheduler.scheduleJob(trigger2);
    }
}
