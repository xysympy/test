package com.yxm.springboot.config;

import com.yxm.springboot.interceptor.OneInterceptor;
import com.yxm.springboot.interceptor.TwoInterceptor;
import com.yxm.springboot.transcation.service.TestService;
import com.yxm.springboot.transcation.service.impl.TestServiceImpl;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author yxm
 * @description: 添加拦截器
 * @date 2021/11/10 9:23
 */
//@Configuration
public class MvcConfig implements WebMvcConfigurer {

    private final List<HandlerMethodArgumentResolver> resolvers;

    public MvcConfig(List<HandlerMethodArgumentResolver> resolvers) {
        this.resolvers = resolvers;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getOneInterceptor());
        registry.addInterceptor(getTwoInterceptor());
    }

    //@Bean
    public OneInterceptor getOneInterceptor(){
        return new OneInterceptor();
    }

    //@Bean
    public TwoInterceptor getTwoInterceptor(){
        return new TwoInterceptor();
    }

    //@Bean
    /*public TestService testService(){
        return new TestServiceImpl();
    }*/

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.addAll(this.resolvers);
    }

    /*@Override
    这就是区别，如果是这种形式的话，就不会报错，不会产生循环依赖，但是如果不是，而是采用了另外一种，也就是上面的
    这种形式的话，就会产生循环依赖，然后报错
    我的理解是：注释的这种方式，实际上是因为创建MVConfig的时候不需要依赖注入其它的bean实例；而上面的这种是需要依赖其它的实例
    才能创建出来的，而就是在创建的过程中，它对应的依赖也在创建，所以导致了循环依赖
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(this.oneResolve());
    }

    @Bean("OneResolve")
    public OneResolve oneResolve(){
        return new OneResolve();
    }*/
}
