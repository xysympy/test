package com.yxm.springboot.config;

import com.yxm.springboot.valueannotation.service.ValueService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author yxm
 * @description: 测试@Bean注解和@Value注解的使用
 * @date 2022/4/1 11:22
 */
@Configuration
public class BeanConfig {

    @Bean
    public ValueService valueService() {
        // 下面的结果会产生覆盖
        // 猜测原因：最开始存储的对象是下面的这个对象，但是估计会进行初始化
        // 也就是把当前的这个bean获取到之后，实例化之后，会进行初始化赋值操作
        // 所以，即使你在这里面使用的是自己new的对象，它还是会进入到bean的生命周期的
        // 也就是继续，实例化，然后后面再执行初始化，aop等等这些操作
        ValueService valueService = new ValueService();
        valueService.setKey("key2");
        valueService.setValue("value2");
        return valueService;
    }
}
