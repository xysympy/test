package com.yxm.springboot.transcation.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @Description: 用户表的实体类
 * @Author yxm
 * @Date 2021/10/26 11:11
 */
@Data
public class User {
    /**
     * 用户id
     */
    @TableId
    private String userId;
    /**
     * 名字
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
}
