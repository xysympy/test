package com.yxm.springboot.transcation.service.impl;

import cn.hutool.core.util.IdUtil;
import com.yxm.springboot.transcation.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author yxm
 * @description: 测试事务是否会失效
 * @date 2021/11/18 13:06
 */
@Service
@Slf4j
public class TestServiceImpl implements TestService {

    private final JdbcTemplate jdbcTemplate;
    @Resource
    private TestServiceImpl testService;

    public TestServiceImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor = Exception.class)
    public String jdbcTest(){
        log.info("开始进行{}方法", "jdbcTest");
        String sql = "insert into `user`(user_id, name, age) values(?, ?, ?)";
        try {
            //int i = 1 / 0;
            testService.insertOne2();
            testService.insertOne1();
        }catch (Exception e){
            e.printStackTrace();
        }
        return "jdbcTest";
    }

    public boolean insertOne2(){
        String sql = "insert into `user`(user_id, name, age) values(?, ?, ?)";
        int update = jdbcTemplate.update(sql, IdUtil.randomUUID(), "test1", 12);
        return true;
    }

    //@Override
    //@Transactional(propagation= Propagation.REQUIRED, rollbackFor = Exception.class)
    private boolean insertOne1(){
        String sql = "insert into `user`(user_id, name, age) values(?, ?, ?)";
//        int i = 1 / 0;
        int update = jdbcTemplate.update(sql, IdUtil.randomUUID(), "test1", 12);
        return true;
    }
}
