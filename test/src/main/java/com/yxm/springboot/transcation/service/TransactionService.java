package com.yxm.springboot.transcation.service;

import com.yxm.springboot.transcation.dao.UserDao;
import com.yxm.springboot.transcation.entity.User;
import org.apache.ibatis.transaction.Transaction;
import org.springframework.aop.framework.AopContext;
import org.springframework.aop.support.AopUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author Administrator
 * @Date 2021/10/26 10:39
 */
@Service
public class TransactionService {

    private final UserDao userDao;
    @Resource
    private TransactionService transactionService /*= (TransactionService) AopContext.currentProxy()*/;

    public TransactionService(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * 创建
     *
     * @param user 用户
     */
    @Transactional(rollbackFor = Exception.class)
    public void create(User user){
        try {
            //transactionService.addUser(user);
        }catch (Exception e){
            e.printStackTrace();
        }
        user.setUserId(UUID.randomUUID().toString());
        userDao.insert(user);
        //System.out.println(1 / 0);
    }

    /**
     * 添加用户
     *
     * @param user 用户
     */
    @Transactional(rollbackFor = Exception.class)
    public void addUser(User user){
        userDao.insert(user);
        System.out.println(1 / 0);
    }
}
