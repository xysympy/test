package com.yxm.springboot.transcation.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxm.springboot.transcation.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author Administrator
 * @Date 2021/10/26 11:18
 */
@Mapper
public interface UserDao extends BaseMapper<User> {
}
