package com.yxm.springboot.transcation.controller;

import com.yxm.springboot.transcation.service.TestService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: 测试事务问题
 * @Author yxm
 * @Date 2021/10/26 10:38
 */
@RestController
@RequestMapping("test")
public class TestController {

    private final TestService testService;

    public TestController(TestService testService) {
        this.testService = testService;
    }

    @RequestMapping("test")
    public String test(){
        return "test";
    }

    @RequestMapping("jdbcTest")
    public String jdbcTest(){
        return testService.jdbcTest();
    }
}
