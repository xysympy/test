package com.yxm.k8s;

import io.kubernetes.client.Exec;
import io.kubernetes.client.util.Streams;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExecTest {

    private final static ThreadPoolExecutor EXECUTOR = new ThreadPoolExecutor(8, 256, 1, TimeUnit.SECONDS, new ArrayBlockingQueue<>(1));

    public static void main(String[] args) {
        Exec exec = new Exec();
        Process process = null;
        try {
            process = exec.exec("testNamespace", "podName", new String[]{"ls"}, true);
            Process finalProcess = process;
            Future<?> future = EXECUTOR.submit(() -> {
                try {
                    Streams.copy(finalProcess.getInputStream(), System.out);
                } catch (IOException e) {
                    System.err.println("io: " + e);
                }
            });
            future.get(10, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.err.println(e);
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
    }
}
