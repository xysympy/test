package com.yxm.jackson.domain.entity;

import lombok.Data;

/**
 * @author yxm
 * @description: 实体类
 * @date 2022/2/25 9:29
 */
@Data
public class Person {
    /**
     * 名字
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 爱好
     */
    private String[] hobbies;
}
