package com.yxm.jackson.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yxm.jackson.domain.entity.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yxm
 * @description: 测试Jackson序列化
 * @date 2022/2/25 9:30
 */
public class SerializeTest {
    public static void main(String[] args) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String personStr = "{\n" +
                "    \"name\":\"111\",\n" +
                "    \"age\":1,\n" +
                "    \"hobbies\":\"123\"\n" +
                "}";
        //Person person = mapper.readValue(personStr, Person.class);

        List<List<Integer>> lists = new ArrayList<>();

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        List<Integer> temp = new ArrayList<>(list);
        test(lists, list);
        System.out.println(temp);
        for (List<Integer> integers : lists) {
            for (Integer integer : integers) {
                System.out.print(integer + ", ");
            }
            System.out.println();
        }

        Person person1 = new Person();
        person1.setAge(1);
        person1.setName("1");
        Person person2 = new Person();
        person2.setAge(2);
        person2.setName("2");
        Person person3 = new Person();
        person3.setAge(3);
        person3.setName("3");
        List<Person> personList = new ArrayList<>();
        personList.add(person1);
        personList.add(person2);
        personList.add(person3);
        System.out.println(personList);
        List<Person> personTemp = new ArrayList<>(personList);
        personList.remove(1);
        System.out.println(personList);
        System.out.println(personList);
        System.out.println(personTemp);

    }

    private static void test(List<List<Integer>> lists, List<Integer> list) {
        List<Integer> temp = new ArrayList<>(list);
        lists.add(temp);
        list.remove(1);
        System.out.println(list);
        System.out.println(temp);
    }
}
