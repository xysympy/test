package com.yxm.springboot;

import com.yxm.springboot.cycle.Cycle;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author yxm
 * @description: 测试解决循环依赖
 * @date 2022/5/9 10:32
 */
@SpringBootTest(classes = {TransactionalApplication.class})
public class CycleTest {

    @Test
    void cycleTest() {
        System.out.println(Cycle.getBean(Cycle.class));
    }
}
