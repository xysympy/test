package com.yxm.springboot;

import cn.hutool.core.thread.ThreadFactoryBuilder;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yxm.test.mybatis.plus.MybaitsApplication;
import com.yxm.test.mybatis.plus.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2022/5/6 10:21
 */
@SpringBootTest(classes = {MybaitsApplication.class})
public class ThreadPollTest {

    private static final LambdaQueryWrapper<User> queryWrapper;

    static {
        queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getDeleteState, false)
                .eq(User::getDeleteTime, new Date());
    }

    @Test
    public void test() {
        ThreadFactory threadFactory = ThreadFactoryBuilder.create()
                .setNamePrefix("Statistics-Datasource-thread").build();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(100, 100, 0L, TimeUnit.SECONDS
                , new LinkedBlockingQueue<>(), threadFactory);
        /*User user = User.builder()
                .age(11).deleteState(0).deleteTime(new Date()).name("test")
                .build();*/

        for (int i = 0; i < 100; i++) {
            final int temp = i;
            threadPoolExecutor.execute(() -> {
                Map<String, Object> map = new HashMap<>(1);
                map.put("num", temp);
                // System.out.println(queryWrapper.getExpression().getSqlSegment());
                // System.out.println(user);
                System.out.println(queryWrapper);
                System.out.println(queryWrapper.getExpression().getSqlSegment());
            });
        }
    }
}
