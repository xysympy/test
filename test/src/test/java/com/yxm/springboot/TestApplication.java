package com.yxm.springboot;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.yxm.springboot.annotation.ValueTest;
import com.yxm.springboot.transcation.entity.User;
import com.yxm.springboot.transcation.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Map;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author Administrator
 * @Date 2021/10/26 11:27
 */
@SpringBootTest
public class TestApplication {

    @Autowired
    private TransactionService transactionService;

    @Test
    public void test(){
        User user = new User();
        user.setName("test");
        user.setAge(1);
        transactionService.create(user);
    }

    @Test
    public void testJackson() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String data = "{\"request\":{\"deviceId\":\"123\"}}";
        // 由于request对应的value实际上是object，不能强制转换成String
        Map strings = objectMapper.readValue("{\"request\":{\"deviceId\":\"123\"}}", new TypeReference<Map<String, String>>() {});
        /*Object o = objectMapper.readValue(data, Map.class).get("request");
        if (!(o instanceof String)) {
            System.out.println(objectMapper.writeValueAsString(o));
        }*/
        System.out.println(strings);
    }

    /**
     * 测试@Value注解
     */
    @Test
    void testValue(){
        System.out.println(ValueTest.getValueName());
    }
}
