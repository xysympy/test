package com.yxm.springboot;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2022/4/12 15:58
 */
public class LogTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(LogTest.class);

    @Test
    public void testLog() {
        try {
            int i = 1 / 0;
        } catch (Exception e) {
            LOGGER.error("exception: ", e);
        }
    }
}
