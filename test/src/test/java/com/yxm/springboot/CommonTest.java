package com.yxm.springboot;

import cn.hutool.core.codec.Base64Encoder;
import cn.hutool.crypto.asymmetric.RSA;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.http.HttpRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yxm.springboot.company.dto.HeadDto;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * @author yxm
 * @description: 不需要关联SpringBoot的测试
 * @date 2022/3/30 11:01
 */
public class CommonTest {

    @Test
    public void generateRsaKey() {
        RSA rsa = new RSA();
        byte[] publicKey = rsa.getPublicKey().getEncoded();
        System.out.println("publicKey bytes: " + Arrays.toString(publicKey));
        System.out.println("publicKey base64 encode: " + Base64Encoder.encode(publicKey));
        byte[] privateKey = rsa.getPrivateKey().getEncoded();
        System.out.println("privateKey bytes: " + Arrays.toString(privateKey));
        System.out.println("privateKey base64 encode: " + Base64Encoder.encode(privateKey));
    }

    @Test
    public void generateAesKey() {
        AES aes = new AES();
        byte[] aesKey = aes.getSecretKey().getEncoded();
        System.out.println("aes key: " + Arrays.toString(aesKey));
        System.out.println("aes Key base64 encode: " + Base64Encoder.encode(aesKey));
    }

    @Test
    public void testJackson() {
        HeadDto dto = new HeadDto();
        try {
            System.out.println(new ObjectMapper().writeValueAsString(dto));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            System.out.println(new ObjectMapper()
                    // 反序列化和set没有什么关系,但是和get有关系,上面的序列化也是一样的
                    .readValue("{\"key\":\"key\", \"authorization\":\"authorization\"}"
                            , HeadDto.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void loginTest() {
        String body = HttpRequest.post("localhost:8889/company/login")
                .execute()
                .body();
        System.out.println(body);
    }
}
