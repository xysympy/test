package com.yxm.springboot;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONUtil;
import com.yxm.springboot.annotation.domain.entity.Person;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

/**
 * @author yxm
 * @description: 测试一下@Validated注解
 * @date 2022/2/26 11:15
 */
@SpringBootTest
public class ValidatedTest {

    @Resource
    private ValidatorFactory validatorFactory;

    @Test
    public void testValidatedAnnotation() {
        Person person = new Person();
        person.setAge(1);
        //person.setName("test");
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("1");
        System.out.println(list);
        person.setHobbies(list);
        String body = HttpRequest.post("http://localhost:8889/validated/test")
                .body(JSONUtil.toJsonStr(person))
                .execute()
                .body();
        System.out.println(body);
    }

    @Test
    public void validator() {
        Validator validator = this.validatorFactory.getValidator();
        Person person = new Person();
        person.setAge(1);
        //person.setName("test");
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("1");
        person.setHobbies(list);
        Set<ConstraintViolation<Person>> validate = validator.validate(person, new Class[0]);
        StringJoiner stringJoiner = new StringJoiner(";");
        for (ConstraintViolation<Person> violation : validate) {
            stringJoiner.add(violation.getPropertyPath().toString() + violation.getMessage());
        }
        System.out.println(stringJoiner.toString());
    }
}
