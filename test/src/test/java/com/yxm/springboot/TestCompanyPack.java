package com.yxm.springboot;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yxm.springboot.company.config.CommonConfig;
import com.yxm.springboot.company.dto.HeadDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author yxm
 * @description: 测试下company包下的类
 * @date 2022/3/30 10:56
 */
@SpringBootTest
public class TestCompanyPack {
    @Resource
    private CommonConfig commonConfig;

    @Test
    public void testConfig() {
        // @ConfigurationProperties注解是通过set方法添加进去的，而是是那种setTest1
        // 那么对应的就是test1才能注入数据
        System.out.println(commonConfig.getPrivateKey());
        System.out.println(commonConfig.getPublicKey());
        // System.out.println(commonConfig.publicKey);
        System.out.println(commonConfig.getJwtKey());
        System.out.println(commonConfig.getTtl());
    }
}
