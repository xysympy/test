package com.yxm.file.system.util;

import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.*;

import java.io.*;

/**
 * @author yxm
 * @description: PDF加水印工具类
 * @date 2022/1/15 13:14
 */
public class PdfUtil {

    /**
     * 添加水印
     *
     * @param inputStream   输入流
     * @param waterMarkName 水印名称
     * @return {@link OutputStream}
     */
    public static OutputStream addWaterMark(InputStream inputStream, String waterMarkName){
        try {
            // 原PDF文件
            PdfReader reader = new PdfReader(inputStream);
            // 输出的PDF文件内容
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            PdfStamper stamper = new PdfStamper(reader, arrayOutputStream);

            // 字体 来源于 itext-asian JAR包
            BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", true);

            PdfGState gs = new PdfGState();
            // 设置透明度
            gs.setFillOpacity(0.1f);
            gs.setStrokeOpacity(0.4f);
            int totalPage = reader.getNumberOfPages() + 1;
            for (int i = 1; i < totalPage; i++) {
                // 内容上层
			    /*PdfContentByte content = stamper.getOverContent(i);*/
                // 内容下层
                PdfContentByte content = stamper.getUnderContent(i);

                content.beginText();
                // 字体添加透明度
                content.setGState(gs);
                // 添加字体大小等
                content.setFontAndSize(baseFont, 50);
                // 添加范围
                content.setTextMatrix(70, 200);
                // 具体位置 内容 旋转多少度 共360度
                content.showTextAligned(Element.ALIGN_CENTER, waterMarkName, 300, 350, 45);

                content.endText();
            }
            // 关闭
            stamper.close();
            reader.close();
            return arrayOutputStream;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
