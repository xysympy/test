package com.yxm.file.system.domain.dto;

/**
 * @author yxm
 * @description: 上传文件请求实体类
 * @date 2022/1/15 15:28
 */
public class UploadDto {
    /**
     * 水印
     */
    private String waterMark;

    public String getWaterMark() {
        return waterMark;
    }

    public void setWaterMark(String waterMark) {
        this.waterMark = waterMark;
    }
}
