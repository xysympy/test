package com.yxm.file.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;


/**
 * @author yxm
 * @description: 主启动类
 * @date 2022/1/15 13:22
 */
@SpringBootApplication
@EnableLdapRepositories
public class FileSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(FileSystemApplication.class, args);
        //PdfUtil.addWaterMark("E:/test/pdf/test.pdf", "E:/test/pdf/test1.pdf");
    }
}
