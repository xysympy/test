package com.yxm.file.system.ldap.domain.repository;

import com.yxm.file.system.ldap.domain.entity.Person;
import org.springframework.data.repository.CrudRepository;

import javax.naming.Name;

/**
 * @author yxm
 * @description:
 * @date 2022/1/21 9:33
 */
public interface PersonRepository extends CrudRepository<Person, Name> {
}
