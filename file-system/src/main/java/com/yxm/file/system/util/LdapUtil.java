package com.yxm.file.system.util;

import com.yxm.file.system.ldap.domain.entity.Group;
import com.yxm.file.system.ldap.domain.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.ldap.LdapProperties;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yxm
 * @description: 封装ldap操作
 * @date 2022/1/22 14:25
 */
@Component
public class LdapUtil {

    private static LdapProperties ldapProperties;

    private static LdapTemplate ldapTemplate;

    @Autowired
    private void setLdapTemplate(LdapTemplate ldapTemplateDemo){
        ldapTemplate = ldapTemplateDemo;
    }
    @Autowired
    private void setLdapProperties(LdapProperties ldapPropertiesDemo){
        ldapProperties = ldapPropertiesDemo;
    }

    /**
     * 查询所有
     *
     * @return {@link List}<{@link Person}>
     */
    public List<Person> queryAll(){
        return ldapTemplate.findAll(Person.class);
    }

    /**
     * 根据组名查询其组所有的用户
     *
     * @param groupName 组名称
     * @return {@link List}<{@link Person}>
     */
    public List<Person> queryAllByGroup(String groupName){
        Name base = LdapNameBuilder.newInstance()
                .add("ou", groupName).build();
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        return ldapTemplate.findAll(base, searchControls, Person.class);
    }

    public void createGroup(String groupName, String description){
        Name id = LdapNameBuilder.newInstance()
                .add("ou", groupName).build();
        DirContextOperations ctx = new DirContextAdapter();
        ctx.setDn(id);
        ctx.setAttributeValue("objectClass", "organizationalUnit");
        ctx.setAttributeValue("ou", groupName);
        ctx.setAttributeValue("description", description);
        ldapTemplate.bind(ctx);
    }

    public void deleteGroup(String groupName) {
        Name id = LdapNameBuilder.newInstance()
                .add("ou", groupName).build();
        ldapTemplate.unbind(id);
    }

    public void modifyGroup(String groupName) throws NamingException {
        Name id = LdapNameBuilder.newInstance()
                .add("ou", groupName).build();
        /*DirContextAdapter ctx = new DirContextAdapter();
        ctx.setDn(id);
        ctx.setUpdateMode(true);*/
        // ctx.setAttributeValue("objectClass", "organizationalUnit");
        ModificationItem modificationItem = new ModificationItem(DirContext.REPLACE_ATTRIBUTE
                , new BasicAttribute("description", "description"));
        List<ModificationItem> itemList = new ArrayList<>();
        itemList.add(modificationItem);
        ldapTemplate.modifyAttributes(id, itemList.toArray(new ModificationItem[itemList.size()]));
        // ldapTemplate.modifyAttributes(id, itemList.toArray(new ModificationItem[itemList.size()]))未实现
    }

}
