package com.yxm.file.system.controller;

import com.yxm.file.system.domain.dto.UploadDto;
import com.yxm.file.system.util.FileUtil;
import com.yxm.file.system.util.PdfUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * @author yxm
 * @description: 文件上传测试
 * @date 2022/1/15 15:15
 */
@RestController
public class FileController {

    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile[] files, @RequestParam String waterMark) throws Exception {
        OutputStream outputStream = null;
        for (MultipartFile file : files) {
            outputStream = PdfUtil.addWaterMark(file.getInputStream(), waterMark);
            FileUtil.sshSftp(((ByteArrayOutputStream) outputStream).toByteArray(), file.getOriginalFilename() + "test2.pdf");
        }
        if (outputStream != null) {
            outputStream.close();
        }
        return "success";
    }
}
