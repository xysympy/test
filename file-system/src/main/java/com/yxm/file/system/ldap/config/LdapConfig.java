package com.yxm.file.system.ldap.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yxm
 * @description: ldap连接配置类
 * @date 2022/1/20 14:26
 */
@Configuration
public class LdapConfig {

    @Value("${my.ldap.url:ldap://192.168.79.141:389}")
    private String url;
    @Value("${my.ldap.base:dc=yxm,dc=com}")
    private String base;
    @Value("${my.ldap.principal:cn=Manager,dc=yxm,dc=com}")
    private String principal;
    @Value("${my.ldap.password:xysympy}")
    private String password;

    @Bean
    public LdapContextSource ldapContextSource(){
        LdapContextSource ldapContextSource = new LdapContextSource();
        ldapContextSource.setUrl(this.url);
        ldapContextSource.setBase(this.base);
        ldapContextSource.setUserDn(this.principal);
        ldapContextSource.setPassword(this.password);
        Map<String, Object> config = new HashMap<>(1);
        // 解决乱码
        config.put("java.naming.ldap.attributes.binary", "objectGUID");
        ldapContextSource.setPooled(true);
        ldapContextSource.setBaseEnvironmentProperties(config);
        return ldapContextSource;
    }

    @Bean
    public LdapTemplate ldapTemplate(){
        LdapTemplate ldapTemplate = new LdapTemplate();
        ldapTemplate.setContextSource(ldapContextSource());
        return ldapTemplate;
    }

}
