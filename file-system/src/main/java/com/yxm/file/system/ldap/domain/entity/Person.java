package com.yxm.file.system.ldap.domain.entity;

import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import javax.naming.Name;
import java.util.Objects;

/**
 * @author yxm
 * @description: 测试使用ldap用户
 * @date 2022/1/21 9:26
 */
@Entry(objectClasses = "inetOrgPerson")
public final class Person {
    @Id
    private Name id;
    @DnAttribute(value = "uid", index = 0)
    private String uid;
    @Attribute(name = "cn")
    private String commonName;
    @Attribute(name = "sn")
    private String suerName;
    private String userPassword;
    @Attribute(name="ou")
    private String group;

    public Person() {
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", uid='" + uid + '\'' +
                ", commonName='" + commonName + '\'' +
                ", suerName='" + suerName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", group='" + group + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return Objects.equals(id, person.id) &&
                Objects.equals(uid, person.uid) &&
                Objects.equals(commonName, person.commonName) &&
                Objects.equals(suerName, person.suerName) &&
                Objects.equals(userPassword, person.userPassword) &&
                Objects.equals(group, person.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, uid, commonName, suerName, userPassword, group);
    }

    public Name getId() {
        return id;
    }

    public void setId(Name id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getSuerName() {
        return suerName;
    }

    public void setSuerName(String suerName) {
        this.suerName = suerName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
