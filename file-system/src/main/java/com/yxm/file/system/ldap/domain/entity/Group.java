package com.yxm.file.system.ldap.domain.entity;

import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import javax.naming.Name;
import java.util.Objects;

/**
 * @author yxm
 * @description:
 * @date 2022/1/23 10:24
 */
@Entry(objectClasses = "organizationalUnit")
public class Group {
    @Id
    private Name id;
    @Attribute(name="ou")
    private String group;
    private String description;

    public Name getId() {
        return id;
    }

    public void setId(Name id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group1 = (Group) o;
        return Objects.equals(id, group1.id) &&
                Objects.equals(group, group1.group) &&
                Objects.equals(description, group1.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, group, description);
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", group='" + group + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
