package com.yxm.file.system.util;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.stereotype.Service;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * ldap服务
 *
 * @author Administrator
 * @date 2022/01/22
 */
@Service
public class LdapService {

    private final LdapTemplate ldapTemplate;

    public LdapService(LdapTemplate ldapTemplate) {
        this.ldapTemplate = ldapTemplate;
    }

    public void bindUser(String name) {
        try {
            Attributes attributes = new BasicAttributes();

            attributes.put("objectclass", "user");
            attributes.put("name", name);
            attributes.put("displayname", name);
            attributes.put( "sAMAccountName", name);
            attributes.put("description", "测试用户1111111111111");
            attributes.put("gidNumber", "22345678");
            attributes.put("uidNumber", "16677777");
            ldapTemplate.bind("CN=" + name + ",CN=Users", null, attributes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createGroup(String name) throws Exception {
        try {
            Attributes attributes = new BasicAttributes();

            attributes.put("objectClass", "group");
            attributes.put("name", name);
            attributes.put("displayName", name);
            attributes.put("description", "测试组11111111111");

            ldapTemplate.bind("CN=" + name + ",CN=Users", null, attributes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void queryGroupByName(String name) {
        LdapQuery query = LdapQueryBuilder.query().where("objectClass").is("group")
                .and("name").is(name);
        List<Map<String, Object>> search = ldapTemplate.search(query, (AttributesMapper<Map<String, Object>>) (attributes) -> {
            Map<String, Object> map = new HashMap<>();
            try {
                Attribute number = attributes.get("gidNumber");
                if (number != null) {
                    String gidNumber = number.get().toString();
                    map.put("gidNumber", gidNumber);
                }
                String distinguishedName = attributes.get("distinguishedName").get().toString();
                NamingEnumeration<?> member = attributes.get("member").getAll();
                List<String> list = new ArrayList<>();
                while (member.hasMore()) {
                    String next = member.next().toString();
                    list.add(next);
                }

                map.put("distinguishedName", distinguishedName);
                map.put("member", list);
            } catch (NamingException e) {
                e.printStackTrace();
            }
            return map;
        });
        System.out.println(search);
    }

    private void delete(String name) {
        ldapTemplate.unbind("CN=" + name + ",CN=Users");
    }

    public void addMemberToGroup(String groupName, String username) {
        String groupDn = "CN=" + groupName + ",CN=Users";
        String userDn = "CN=" + username + ",CN=Users";
        String jhuser1 = "CN=jhuser1,CN=Users";

        DirContextOperations ctxUser = ldapTemplate.lookupContext(userDn);
        //DirContextOperations ctxUser1 = ldapTemplate.lookupContext(jhuser1);
        //DirContextOperations ctxGroup = ldapTemplate.lookupContext(groupDn);
        try {
            String distinguishedname = ctxUser.getStringAttribute("distinguishedName");
            //String distinguishedname1 = ctxUser1.getStringAttribute("distinguishedname");
            //ctxGroup.addAttributeValue("member", distinguishedname);
            //ctxGroup.addAttributeValue("member", distinguishedname1);
            //ldapTemplate.modifyAttributes(ctxGroup);

            List<ModificationItem> itemList = new ArrayList<>();
            ModificationItem primaryGroupIDItem = new ModificationItem(DirContext.ADD_ATTRIBUTE,
                    new BasicAttribute("member", distinguishedname));
            itemList.add(primaryGroupIDItem);
            ModificationItem[] modificationItems = itemList.toArray(new ModificationItem[itemList.size()]);
            ldapTemplate.modifyAttributes(groupDn, modificationItems);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addPrimaryGroupID(String username) {
        String userDn = "CN=" + username + ",CN=Users";

        List<ModificationItem> itemList = new ArrayList<>();
        ModificationItem gidNumberItem = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                new BasicAttribute("primaryGroupID", "1720"));
        itemList.add(gidNumberItem);
        ModificationItem[] modificationItems = itemList.toArray(new ModificationItem[itemList.size()]);
        ldapTemplate.modifyAttributes(userDn, modificationItems);
    }

    public void removeMemberToGroup(String groupName, String username) {
        String groupDn = "CN=" + groupName + ",CN=Users";
        String userDn = "CN=" + username + ",CN=Users";
        DirContextOperations ctxGroup = ldapTemplate.lookupContext(groupDn);
        DirContextOperations ctxUser = ldapTemplate.lookupContext(userDn);
        ctxGroup.removeAttributeValue("member", ctxUser.getStringAttribute("distinguishedName"));
        ldapTemplate.modifyAttributes(ctxGroup);
    }

    public void bindOu(String name) {
        String ou = "OU=sdf,OU=DD,OU=xian,OU=China";
        try {
            Attributes attributes = new BasicAttributes();

            attributes.put("objectClass", "organizationalUnit");
            attributes.put("name", name);
            attributes.put("displayName", name);
            attributes.put("description", "测试组织单位11111111111");

            ldapTemplate.bind(ou, null, attributes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSid(String groupName) {
        String groupDn = "CN=" + groupName + ",CN=Users";

        List<String> list = ldapTemplate.search(
                LdapQueryBuilder.query().where("objectClass").is("group")
                        .and("name").is(groupName),
                (AttributesMapper<String>) (attributes) -> {
                    String objectSidStr = null;
                    try {
                        Object objectSid = attributes.get("objectSid").get();
                        objectSidStr = getObjectSid((byte[]) objectSid);

                    } catch (NamingException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return objectSidStr;
                });

        System.out.print(list.toString());
    }

    private String getObjectSid(byte[] SID) {
        StringBuilder strSID = new StringBuilder("S-");
        strSID.append(SID[0]).append('-');
        StringBuilder tmpBuff = new StringBuilder();
        for (int t = 2; t <= 7; t++) {
            String hexString = Integer.toHexString(SID[t] & 0xFF);
            tmpBuff.append(hexString);
        }
        strSID.append(Long.parseLong(tmpBuff.toString(), 16));
        int count = SID[1];
        for (int i = 0; i < count; i++) {
            int currSubAuthOffset = i * 4;
            tmpBuff.setLength(0);
            tmpBuff.append(String.format("%02X%02X%02X%02X",
                    (SID[11 + currSubAuthOffset] & 0xFF),
                    (SID[10 + currSubAuthOffset] & 0xFF),
                    (SID[9 + currSubAuthOffset] & 0xFF),
                    (SID[8 + currSubAuthOffset] & 0xFF)));
            strSID.append('-').append(
                    Long.parseLong(tmpBuff.toString(), 16));

        }
        return strSID.toString();
    }
}

