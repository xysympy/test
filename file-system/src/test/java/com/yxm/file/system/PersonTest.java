package com.yxm.file.system;

import com.yxm.file.system.ldap.domain.entity.Person;
import com.yxm.file.system.ldap.domain.repository.PersonRepository;
import com.yxm.file.system.util.LdapUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.support.LdapNameBuilder;

import javax.annotation.Resource;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

/**
 * @author yxm
 * @description:
 * @date 2022/1/21 9:43
 */
@SpringBootTest
public class PersonTest {

    @Resource
    private PersonRepository personRepository;
    private final static String BASE_DN = "dc=yxm,dc=com";
    @Resource
    private LdapTemplate ldapTemplate;
    @Resource
    private LdapUtil ldapUtil;

    @Test
    public void queryAll() {
        ldapUtil.queryAll().forEach(person -> System.out.println(person));
    }

    @Test
    public void queryAllByGroup() {
        ldapUtil.queryAllByGroup("Group").forEach(person -> System.out.println(person));
    }

    @Test
    public void createGroup() {
        ldapUtil.createGroup("TESTGROUP1", "测试group生成");
    }

    @Test
    public void deleteGroup() {
        ldapUtil.deleteGroup("TESTGROUP1");
    }

    @Test
    public void modifyGroup() throws NamingException {
        ldapUtil.modifyGroup("TESTGROUP1");
    }

    @Test
    public void queryPerson() {
        // System.out.println(personRepository instanceof PersonRepository);
        //personRepository.findAll().forEach(person -> System.out.println(person));
        ldapTemplate.findAll(Person.class).forEach(person -> System.out.println(person));
    }

    /**
     * 一直会报错，但是不知道什么原因，暂时搁置
     */
    @Test
    public void addPerson() {
        Person person = new Person();
        Name id = LdapNameBuilder.newInstance()
                .add("ou", "Group")
                .add("ou", "add")
                .build();
        //person.setId(id);
        person.setUid("1112");
        person.setSuerName("111");
        person.setCommonName("222");
        //person.setGroup("123456");
        person.setUserPassword("123456");
        personRepository.save(person);
    }

    @Test
    public void create() {
        Person user = new Person();
        user.setSuerName("lyhtest");
        user.setCommonName("root");
        user.setUserPassword("ldappsw");
        user.setUid(String.valueOf(10L));

        Name dn = buildDn();
        ldapTemplate.bind(dn, null, buildAttributes(user));
    }

    /**
     * 动态创建DN
     * spring-ldap提供了：LdapNameBuilder，LdapUtils
     */
    private Name buildDn() {
        return LdapNameBuilder.newInstance()
                .add("ou", "People")
                .add("ou", "Users")
                .build();
    }

    /**
     * 配置属性
     *
     * @param user
     * @return
     */
    private Attributes buildAttributes(Person user) {
        Attributes attrs = new BasicAttributes();

        BasicAttribute objectclass = new BasicAttribute("objectclass");
        objectclass.add("top");
        objectclass.add("posixAccount");
        objectclass.add("inetOrgPerson");
        attrs.put(objectclass);

        attrs.put("userPassword", user.getUserPassword());
        attrs.put("cn", user.getSuerName() + "@" + user.getCommonName());
        attrs.put("sn", user.getSuerName() + "@" + user.getCommonName());
        attrs.put("displayName", user.getSuerName() + "@" + user.getCommonName());
        attrs.put("homeDirectory", "/root");
        attrs.put("uidNumber", user.getUid());
        attrs.put("uid", user.getSuerName());
        attrs.put("gidNumber", "0");

        return attrs;
    }
}
