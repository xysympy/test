package com.yxm.file.system;

import com.yxm.file.system.util.FileUtil;
import com.yxm.file.system.util.PdfUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;

/**
 * @author yxm
 * @description: 测试文件上传
 * @date 2022/1/15 14:50
 */
@SpringBootTest
public class FileUtilTest {

    @Test
    void test() throws Exception {
        FileUtil.sshSftp(cn.hutool.core.io.FileUtil.readBytes("E:/test/pdf/test.pdf")
                , "test.pdf");
    }

    @Test
    void testPdf() throws IOException {
        OutputStream outputStream = PdfUtil.addWaterMark(cn.hutool.core.io.FileUtil.getInputStream("E:/test/pdf/test.pdf")
                , "waterMark pdf");
        FileOutputStream fileOutputStream = new FileOutputStream("E:/test/pdf/test1.pdf");
        fileOutputStream.write(((ByteArrayOutputStream) outputStream).toByteArray());
        fileOutputStream.close();
    }
}
