package com.webflux.service;

import com.webflux.util.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

@Service
public class TestService {

    private HttpUtils httpUtils;

    private ThreadPoolTaskExecutor httpExecutor;

    @Autowired
    public void setHttpUtils(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    @PostConstruct
    public void initThreadPool() {
        ThreadPoolTaskExecutor httpExecutor = new ThreadPoolTaskExecutor();
        httpExecutor.setCorePoolSize(20);
        httpExecutor.setMaxPoolSize(20);
        httpExecutor.setKeepAliveSeconds(1);
        httpExecutor.setQueueCapacity(16);
        httpExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        httpExecutor.setThreadNamePrefix("http-request-");
        httpExecutor.initialize();
        this.httpExecutor = httpExecutor;
    }

    public String testHttps() throws ExecutionException, InterruptedException {
        CompletableFuture<String> completableFuture =
                CompletableFuture.supplyAsync(() ->
                        this.httpUtils.httpGet("https://localhost:3924/web-flux/test", new HttpHeaders(), String.class));
        return completableFuture.get();
    }

    public String testMono() throws ExecutionException, InterruptedException {
        return CompletableFuture.supplyAsync(
                        () -> this.httpUtils.httpGet("https://localhost:3924/web-flux/test", new HttpHeaders(), String.class), this.httpExecutor)
                .get();
//        return this.httpUtils.httpGet("https://localhost:3924/web-flux/test", new HttpHeaders(), String.class);
        /*final CountDownLatch latch = new CountDownLatch(1);
        AtomicReference<String> res = new AtomicReference<>();
        this.httpUtils.httpGetAsync("https://localhost:3924/web-flux/test", new HttpHeaders(), String.class)
                .subscribe(response -> {
                   res.set(response);
                   latch.countDown();
                });
        latch.await();
        return res.get();*/
    }
}
