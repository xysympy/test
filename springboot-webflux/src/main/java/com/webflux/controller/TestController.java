package com.webflux.controller;

import com.webflux.service.TestService;
import com.webflux.util.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
@RequestMapping("/web-flux")
public class TestController {

    private TestService testService;

    private HttpUtils httpUtils;

    @Autowired
    public void setTestService(TestService testService) {
        this.testService = testService;
    }

    @Autowired
    public void setHttpUtils(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    @Autowired


    @GetMapping("/test")
    public String test() {
        return "test";
    }

    @GetMapping("/test/https")
    public String testHttps() {
        try {
            String res = this.testService.testHttps();
            System.out.println("res: " + res);
            return res;
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/test/mono")
    public Mono<String> testMono() {
        try {
            return Mono.just(this.testService.testMono());
        } catch (ExecutionException | InterruptedException e) {
            log.error("mono调用接口失败，错误信息：", e);
            throw new RuntimeException(e);
        }
    }
}
