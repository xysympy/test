package com.webflux.util;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.SSLException;
import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Map;

@Component
public class HttpUtils {

    private final WebClient webClient;

    public HttpUtils() {
        try {
            SslContext sslContext = SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
            HttpClient httpClient = HttpClient.create().secure(t -> t.sslContext(sslContext)).followRedirect(true);
            this.webClient = WebClient.builder().clientConnector(new ReactorClientHttpConnector(httpClient)).build();
        } catch (SSLException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> Mono<T> httpGetAsync(String uri, HttpHeaders headers, Class<T> bean) {
        return this.webClient.get()
                .uri(uri)
                .headers(tmp -> {
                    for (Map.Entry<String, List<String>> entry : headers.entrySet()) {
                        tmp.addAll(entry.getKey(), entry.getValue());
                    }
                })
                .exchangeToMono(clientResponse -> {
                    System.out.println("headers: " + clientResponse.headers().asHttpHeaders());
                    return clientResponse.bodyToMono(bean);
                }).timeout(Duration.ofSeconds(5));
    }

    public <T> T httpGet(String uri, HttpHeaders headers, Class<T> bean) {
        return this.httpGetAsync(uri, headers, bean).block();
    }
}
