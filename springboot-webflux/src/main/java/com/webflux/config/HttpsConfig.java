package com.webflux.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
import org.springframework.boot.web.server.Ssl;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URISyntaxException;

@Configuration
public class HttpsConfig {
    // 将https重定向到http
    @Value("${server.port}")
    private int httpPort;

    @Value("${http.port}")
    private int httpsPort;

    @Value("${server.ssl.key-alias}")
    private String keyAlias;

    @Value("${server.ssl.key-password}")
    private String keyPassword;

    @Value("${server.ssl.key-store}")
    private String keyStore;

    @Value("${server.ssl.key-store-password}")
    private String keyStorePassword;

    @Value("${server.ssl.key-store-type}")
    private String keyStoreType;

    @PostConstruct
    public void startRedirectServer() {
        NettyReactiveWebServerFactory factory = new NettyReactiveWebServerFactory(this.httpsPort);
        this.enableSSL(factory);
        factory.getWebServer(
                (request, response) -> {
                    URI uri = request.getURI();
                    URI httpsUri;
                    try {
                        httpsUri = new URI("http",
                                uri.getUserInfo(),
                                uri.getHost(),
                                this.httpPort,
                                uri.getPath(),
                                uri.getQuery(),
                                uri.getFragment());
                        response.setStatusCode(HttpStatus.MOVED_PERMANENTLY);
                        response.getHeaders().setLocation(httpsUri);
                        return response.setComplete();
                    } catch (URISyntaxException e) {
                        return Mono.error(e);
                    }
                }
        ).start();
    }

    private void enableSSL(NettyReactiveWebServerFactory factory) {
        Ssl ssl = new Ssl();
        ssl.setKeyPassword(this.keyPassword);
        ssl.setKeyAlias(this.keyAlias);
        ssl.setKeyStore(this.keyStore);
        ssl.setKeyStoreType(this.keyStoreType);
        ssl.setKeyStorePassword(this.keyStorePassword);
        factory.setSsl(ssl);
    }
}
