package net.mqtt.service.proxy;

import net.mqtt.service.UserService;

public class UserServiceProxy {

    public void test() {
        UserService userService = new UserService();
        userService.test();
        System.out.println("proxy test()");
    }

}
