package net.mqtt;

import net.mqtt.service.MyService;
import net.mqtt.service.MyServiceImpl;
import net.mqtt.service.UserService;
import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.Advisor;
import org.springframework.aop.Pointcut;
import org.springframework.aop.PointcutAdvisor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.cglib.proxy.Callback;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class SpringAopApplication {
    public static void main(String[] args) {
        UserService target = new UserService();

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(UserService.class);

        // 定义代理的逻辑
        enhancer.setCallbacks(new Callback[]{new MethodInterceptor() {
            // 如果是invoke，那么o就是代理对象，如果是invokeSuper就是被代理的对象
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                System.out.println("before");
                // Object result = methodProxy.invoke(target, objects);
                Object result = methodProxy.invokeSuper(o, args);
                System.out.println("after");
                return result;
            }
        }});

        UserService userService = (UserService) enhancer.create();
        userService.test();

        //JDK 动态代理
        MyService myService = new MyServiceImpl();

        Object proxy = Proxy.newProxyInstance(MyServiceImpl.class.getClassLoader(), new Class[]{MyService.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("jdk dynamic proxy before");
                Object invoke = method.invoke(myService, args);
                System.out.println("jdk dynamic proxy after");
                return invoke;
            }
        });

        MyService myServiceProxy = (MyService) proxy;
        myServiceProxy.test();

        // spring的ProxyFactory
        ProxyFactory proxyFactory = new ProxyFactory();

        proxyFactory.setTarget(target);
        proxyFactory.addAdvice(new org.aopalliance.intercept.MethodInterceptor() {
            @Override
            public Object invoke(MethodInvocation methodInvocation) throws Throwable {
                System.out.println("spring proxy factory before");
                Object proceed = methodInvocation.proceed();
                System.out.println("spring proxy factory after");
                return proceed;
            }
        });

        UserService proxy1 = (UserService) proxyFactory.getProxy();
        proxy1.test();

        // spring ProxyFactory实现接口的动态代理
        ProxyFactory proxyFactoryInterface = new ProxyFactory();

        proxyFactoryInterface.setTarget(myService);
        proxyFactoryInterface.addAdvice(new org.aopalliance.intercept.MethodInterceptor() {
            @Override
            public Object invoke(MethodInvocation methodInvocation) throws Throwable {
                System.out.println("spring proxy factory before");
                Object proceed = methodInvocation.proceed();
                System.out.println("spring proxy factory after");
                return proceed;
            }
        });

        MyService proxy2 = (MyService) proxyFactoryInterface.getProxy();
        proxy2.test();

        // 使用spring aop的Advisor（切面技术）
        ProxyFactory proxyFactoryPointcut = new ProxyFactory();

        proxyFactoryPointcut.setTarget(myService);
        proxyFactoryPointcut.addAdvisor(new PointcutAdvisor() {
            @Override
            public Pointcut getPointcut() {
                return new StaticMethodMatcherPointcut() {
                    @Override
                    public boolean matches(Method method, Class<?> targetClass) {
                        return method.getName().equals("test");
                    }
                };
            }

            @Override
            public Advice getAdvice() {
                return new org.aopalliance.intercept.MethodInterceptor() {
                    @Override
                    public Object invoke(MethodInvocation invocation) throws Throwable {
                        System.out.println("spring proxy factory advisor before");
                        Object proceed = invocation.proceed();
                        System.out.println("spring proxy factory advisor after");
                        return proceed;
                    }
                };
            }

            @Override
            public boolean isPerInstance() {
                // 取默认值吧，根据注释，貌似这个方法暂时没有用到
                return false;
            }
        });

        MyService proxy3 = (MyService) proxyFactoryPointcut.getProxy();
        proxy3.test();
    }
}