package com.yxm.mongo.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreatedByDto {
    private String test;
}
