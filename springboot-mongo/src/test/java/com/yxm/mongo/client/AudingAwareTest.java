package com.yxm.mongo.client;

import com.yxm.mongo.client.model.CreatedByDto;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AudingAwareTest implements AuditorAware<CreatedByDto> {
    @Override
    public Optional<CreatedByDto> getCurrentAuditor() {
        return Optional.of(new CreatedByDto("testCreatedBy"));
    }
}
