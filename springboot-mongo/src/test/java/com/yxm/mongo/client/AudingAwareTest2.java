package com.yxm.mongo.client;

import com.yxm.mongo.client.model.LastModifiedByDto;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AudingAwareTest2 implements AuditorAware<LastModifiedByDto> {
    @Override
    public Optional<LastModifiedByDto> getCurrentAuditor() {
        return Optional.of(new LastModifiedByDto("testLastModifiedBy"));
    }
}
