package com.yxm.mongo.client.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.persistence.EntityListeners;

@EntityListeners(AuditingEntityListener.class)
@Document(collection = "test")
@Data
public class TestDto {
    @MongoId
    private String id;

    @Field(value = "test")
    private String testField;

    @Field(value = "create_date")
    @CreatedDate
    private long createDate;

    @Field(value = "created_by")
    @CreatedBy
    private CreatedByDto createdByDto;

    @Field(value = "last_modified_date")
    @LastModifiedDate
    private long lastModifiedDate;

    @Field(value = "last_modified_by")
    @LastModifiedBy
    private LastModifiedByDto lastModifiedByDto;
}
