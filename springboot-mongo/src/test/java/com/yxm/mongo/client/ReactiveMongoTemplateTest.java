package com.yxm.mongo.client;

import com.mongodb.client.result.UpdateResult;
import com.yxm.mongo.client.model.TestDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.config.EnableReactiveMongoAuditing;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

@SpringBootTest
@EnableReactiveMongoAuditing
public class ReactiveMongoTemplateTest {

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;


    @Test
    void test() {
        TestDto insert = reactiveMongoTemplate.insert(new TestDto()).block();
        Assertions.assertNotNull(insert);
        System.out.println(insert);
        UpdateResult updateResult = reactiveMongoTemplate.updateFirst(new Query(Criteria.where("_id").is(insert.getId())),
                new Update().set("test", "test_update"), TestDto.class).block();
        System.out.println(updateResult);
        List<TestDto> testDtoList = reactiveMongoTemplate.findAll(TestDto.class).collectList().block();
        Assertions.assertNotNull(testDtoList);
        testDtoList.forEach(System.out::println);

        TestDto findAndModify = reactiveMongoTemplate.findAndModify(new Query(Criteria.where("_id").is(insert.getId())),
                new Update().set("test", "test_find_and_modify"), TestDto.class).block();
        System.out.println(findAndModify);
        testDtoList = reactiveMongoTemplate.findAll(TestDto.class).collectList().block();
        Assertions.assertNotNull(testDtoList);
        testDtoList.forEach(System.out::println);
    }
}
