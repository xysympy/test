package com.yxm.test;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.bson.types.Binary;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class MainTest {
    public static void main(String[] args) throws JsonProcessingException, InterruptedException {
        /*BinaryTest binaryTest = new BinaryTest();
        binaryTest.setId("123");
        binaryTest.setV(new Binary(BsonBinarySubType.OLD_BINARY, "123".getBytes(StandardCharsets.UTF_8)));
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(Binary.class, new BinaryDeserializer());
        objectMapper.registerModule(simpleModule);
        String json = objectMapper.writeValueAsString(binaryTest);
        System.out.println(objectMapper.readValue(json, BinaryTest.class));*/

        /*Mono.delay(Duration.ofSeconds(10), Schedulers.parallel())
                .then(Mono.when(update()))
                .onErrorResume(throwable -> {
                    System.out.println(throwable);
                    return Mono.empty();
                })
                .doOnError(System.out::println)
                .repeat()
                .subscribe();*/

        Mono.delay(Duration.ofSeconds(10), Schedulers.parallel())
                .then(Mono.when(retry()))
                .retryWhen(Retry.backoff(5, Duration.ofSeconds(10)))
                .repeat()
                .subscribe();

        TimeUnit.SECONDS.sleep(40);
        Mono.fromFuture(create())
                .retryWhen(anotherRetry())
                .subscribe();

        TimeUnit.HOURS.sleep(1);
    }

    private static CompletableFuture<String> create() {
        return CompletableFuture.supplyAsync(() -> {
            System.out.println("completable block");
            try {
                TimeUnit.SECONDS.sleep(5);
                throw new RuntimeException("runtime exception");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private static Retry anotherRetry() {
        return Retry.backoff(Long.MAX_VALUE, Duration.ofSeconds(5))
                .scheduler(Schedulers.newParallel("new Parallel"))
                .doBeforeRetry(retrySignal -> System.out.println("before retry"));
    }

    private static Mono<Void> retry() {
        return Mono.fromRunnable(() -> {
            System.out.println("runnable run");
        }).then();
    }

    private static Mono<Void> update() {
        for (int i = 1; i < 20; i++) {
            if (i % 10 == 0) {
                // 没有做异常处理的话，会导致整一个Schedule失效
                throw new RuntimeException("runtime exception");
            }
            System.out.println("i: " + i);
        }
        return Mono.empty();
    }

    static class BinaryDeserializer extends JsonDeserializer<Binary> {

        @Override
        public Binary deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
            JsonNode node = p.getCodec().readTree(p);
            return new Binary((byte) node.get("type").asInt(), node.get("data").binaryValue());
        }
    }

    static class BinaryTest {
        private String id;

        private Binary v;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Binary getV() {
            return v;
        }

        public void setV(Binary v) {
            this.v = v;
        }

        @Override
        public String toString() {
            return "BinaryTest{" +
                    "id='" + id + '\'' +
                    ", v=" + v +
                    '}';
        }
    }
}
