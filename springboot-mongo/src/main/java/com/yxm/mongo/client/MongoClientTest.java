package com.yxm.mongo.client;

import lombok.AllArgsConstructor;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class MongoClientTest {

    private final ReactiveMongoTemplate mongoTemplate;
}
