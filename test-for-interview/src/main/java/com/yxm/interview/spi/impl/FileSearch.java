package com.yxm.interview.spi.impl;

import com.yxm.interview.spi.Search;

import java.util.List;

public class FileSearch implements Search {
    @Override
    public List<String> searchDoc(String keyword) {
        return List.of("search doc by file");
    }
}
