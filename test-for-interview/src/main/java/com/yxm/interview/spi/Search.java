package com.yxm.interview.spi;

import java.util.List;

public interface Search {
    List<String> searchDoc(String keyword);
}
