package com.yxm.interview.spi;

import java.util.ServiceLoader;

public class SpiTest {
    public static void main(String[] args) {
        ServiceLoader<Search> loader = ServiceLoader.load(Search.class);
        loader.forEach(search -> System.out.println(search.searchDoc("test")));
    }
}
