package com.yxm.interview.thissuper;

public class This extends Super {

    public This() {
        this(1);
        System.out.println("this()");
    }

    public This(int i) {
        System.out.println("this(i)");
    }
}
