package com.yxm;

public class ThreadLocalExample {

    private static final ThreadLocal<String> THREAD_LOCAL = ThreadLocal.withInitial(() -> "test");

    public static void main(String[] args) {
        // 或者在使用的时候进行set
        ThreadLocal<String> threadLocal = new ThreadLocal<>();
        threadLocal.set("test");
        System.out.println(threadLocal.get());
    }

    private static void threadLocalExample() {
        System.out.println(THREAD_LOCAL.get());
    }
}
