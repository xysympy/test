package com.yxm;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadLocalWithThreadPoolTest {
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        for (int i = 0; i < 4; i++) {
            int finalI = i;
            executorService.execute(() -> {
                Random random = new Random();
                int randomInt = random.nextInt(10000);
                System.out.println(Thread.currentThread().getName() + ", randomInt: " + randomInt);
                threadLocal.set(String.valueOf(randomInt));
                try {
                    TimeUnit.SECONDS.sleep(finalI + 1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(Thread.currentThread().getName() + ", threadLocal: " + threadLocal.get());
                System.out.println(Thread.currentThread().getName() + "sleep: " + (finalI + 1) + ", finished!");
                threadLocal = null;
                System.out.println(Thread.currentThread().getName() + ", threadLocal: " + threadLocal);
            });
        }

        TimeUnit.SECONDS.sleep(10);
        executorService.shutdown();
    }
}
