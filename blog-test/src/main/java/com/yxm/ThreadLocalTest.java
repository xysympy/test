package com.yxm;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ThreadLocalTest {

    private static ThreadLocal<String> threadLocal = ThreadLocal.withInitial(() -> new String("888"));

    public static void main(String[] args) throws InterruptedException {
        weakReferenceValue();
        internetTest();
    }


    private static void weakReferenceValue() throws InterruptedException {
        Map<WeakReference<ThreadLocal<String>>, WeakReference<String>> map = new HashMap<>();
        WeakReference<String> value = new WeakReference<>(new String("777"));
        WeakReference<ThreadLocal<String>> key = new WeakReference<>(ThreadLocal.withInitial(() -> new String("888")));
        map.put(key, value);
        System.out.println(map.get(key).get());
        System.out.println(key.get().get());
        TimeUnit.SECONDS.sleep(1);
        System.gc();
//        System.out.println(key.get().get());
        System.out.println(map.get(key).get());

        // 所以，实际上就是，ThreadLocalMap的Entry中key之所以为弱引用，是因为当ThreadLocal变量没有强引用的时候，能自动回收
        // 并且，重新进行set等操作，会清除这个Entry，而value是强引用则是因为如果为弱引用，那么但凡有一次GC，这个值就没了
    }

    private static void internetTest() throws InterruptedException {
        Map<WeakReference<Integer>, WeakReference<Integer>> map = new HashMap<>(8);
        WeakReference<Integer> key = new WeakReference<>(666);
        WeakReference<Integer> value = new WeakReference<>(777);
        map.put(key,value);
        System.out.println("put success");
        Thread.sleep(1000);
        System.gc();
        System.out.println("get " + map.get(key).get());
    }
}
