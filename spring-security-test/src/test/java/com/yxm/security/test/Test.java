package com.yxm.security.test;

import com.sun.org.apache.xml.internal.security.algorithms.SignatureAlgorithm;
import com.yxm.security.test.api.user.dao.UserDao;
import com.yxm.security.test.api.user.domain.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;

/**
 * @author yxm
 * @description: 测试一些方法
 * @date 2021/11/22 16:44
 */
@SpringBootTest
@Slf4j
public class Test {

    @Resource
    private UserDao userDao;

    @org.junit.jupiter.api.Test
    public void test(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode("123456");
        log.info("编译后的密码{}, 是否匹配{}", encode, encoder.matches("123456", encode));
    }

    @org.junit.jupiter.api.Test
    public void mapperTest(){
        User user = userDao.queryUser("test1");
        System.out.println(user);
    }

    @org.junit.jupiter.api.Test
    public void jwtToken(){
        /*String token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis()+tokenEcpiration))
                .signWith(SignatureAlgorithm.HS512, tokenSignKey).compressWith(CompressionCodecs.GZIP).compact();*/
    }
}
