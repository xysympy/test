package com.yxm.security.test.api.user.service.impl;

import com.yxm.security.test.api.user.dao.UserDao;
import com.yxm.security.test.api.user.domain.entity.User;
import com.yxm.security.test.api.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/22 20:08
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.queryUser(username);
        log.info("用户：" + user);
        if(user == null){
            throw new UsernameNotFoundException("用户未找到");
        }
        return user;
    }
}
