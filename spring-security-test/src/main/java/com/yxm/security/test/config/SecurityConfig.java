package com.yxm.security.test.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yxm.security.test.api.user.domain.entity.User;
import com.yxm.security.test.api.user.service.UserService;
import org.apache.tomcat.util.http.ResponseUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/23 8:55
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final PersistentTokenRepository tokenRepository;
    private final UserService userService;

    public SecurityConfig(PersistentTokenRepository tokenRepository, UserService userService) {
        this.tokenRepository = tokenRepository;
        this.userService = userService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //super.configure(http);
        http.formLogin()//form表单提交
                .loginPage("/login")  // 登录页面
                .successForwardUrl("/success")  // 登录成功后的接口
                .failureForwardUrl("/failure") // 登录失败时的接口
                .loginProcessingUrl("/load") // 登录处理接口(即使你有这个接口，还是不走你写的，拦截器拦截，登录成功了就直接到设置页面了)
                .usernameParameter("username") // 设置username的name为username(默认就是)
                .passwordParameter("password") // 和username一样
                .and()
                .authorizeRequests()
                // 得放行login，不然就是无限的重定向
                .antMatchers("/login", "/failure.html")
                .permitAll() // 表示这些允许通过
                //.antMatchers("/admin").hasRole("admin") // 有该角色，不用加ROLE_前缀，会自动添加
                //.antMatchers("/normal_user").hasAnyRole("admin", "normal_user") // 包含其中的任意一个角色都行
                //.antMatchers("/admin").hasAuthority("admin") // 有该权限，不需要加前缀，因为没有前缀
                //.antMatchers("/normal_user").hasAnyAuthority("admin", "normal_user") // 有其中的任意一个权限都行
                .anyRequest() // 其它的请求
                .authenticated() // 得认证
                .and();
                //.csrf().disable();
        // 添加记住我设置
        http.rememberMe()
                //.tokenValiditySeconds(10) // 单位是秒
                .tokenRepository(tokenRepository)
                .userDetailsService(userService);
        // 登出设置  默认是/logout  ,可以通过logoutUrl设置登出的路径
        http.logout().logoutUrl("/logout1").logoutSuccessUrl("/login").permitAll();
        http.addFilter(new TokenLoginFilter());
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}

class TokenLoginFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res)
            throws AuthenticationException {
        try {
            User user = new ObjectMapper().readValue(req.getInputStream(),
                    User.class);
            return authenticationManager.authenticate(new
                    UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), new
                    ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

