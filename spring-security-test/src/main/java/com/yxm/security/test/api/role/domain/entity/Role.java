package com.yxm.security.test.api.role.domain.entity;

import lombok.Data;

/**
 * @author yxm
 * @description: 角色表实体类
 * @date 2021/11/22 18:46
 */
@Data
public class Role {
    /**
     * id
     */
    private Integer id;
    /**
     * 角色名
     */
    private String roleName;
}
