package com.yxm.security.test.api.user.dao;

import com.yxm.security.test.api.user.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/22 18:54
 */
@Mapper
public interface UserDao {
    /**
     * 查询用户
     *
     * @param name 的名字
     * @return {@link User}
     */
    public User queryUser(@Param("name") String name);
}
