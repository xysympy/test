package com.yxm.security.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/26 8:55
 */
@Configuration
public class BrowserSecurityConfig {
    /**
     * 添加数据源
     */
    private final DataSource dataSource;

    public BrowserSecurityConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public PersistentTokenRepository getPersistentTokenRepository(){
        JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
        // 配置数据源
        tokenRepository.setDataSource(dataSource);
        // 启动时创建数据表 仅限第一次启动，并且没有persistent_logins数据表的前提下
        //tokenRepository.setCreateTableOnStartup(true);
        return tokenRepository;
    }

}
