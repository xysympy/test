package com.yxm.security.test.api.user.controller;

import com.yxm.security.test.api.user.service.UserService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/22 20:11
 */
@Controller
public class UserController {
    private final UserService userService;
    private final HttpServletRequest httpServletRequest;
    private final HttpServletResponse httpServletResponse;

    public UserController(UserService userService, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        this.userService = userService;
        this.httpServletRequest = httpServletRequest;
        this.httpServletResponse = httpServletResponse;
    }

    @PostMapping("load")
    public UserDetails loadUserByUsername(String username){
        System.out.println(username);
        return userService.loadUserByUsername(username);
    }

    @RequestMapping("login")
    public String login(){
        return "login";
    }

    @RequestMapping("success")
    public String success() throws ServletException, IOException {
        //httpServletRequest.getRequestDispatcher("/success.html").forward(httpServletRequest, httpServletResponse);
        //httpServletResponse.sendRedirect("/success.html");
        return "success";
    }
    @RequestMapping("failure")
    public String failure() throws ServletException, IOException {
        // 像是在一直的自己调用自己，最后导致了栈溢出(httpServletRequest)
        //httpServletRequest.getRequestDispatcher("/failure.html").forward(httpServletRequest, httpServletResponse);
        //httpServletResponse.sendRedirect("/failure.html");
        return "failure";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping("/admin")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseBody
    //@PreFilter(value = "filterObject == 'admin'") 必须是集合，下面也是一样
    //@PostFilter("filterObject.contains('ad')")
    public String admin(String admin){
        return "admin";
    }

    @Secured({"ROLE_ADMIN", "ROLE_NORMAL_USER"})
    @RequestMapping("/normal_user")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseBody
    public String normalUser(){
        return "normal_user";
    }
}
