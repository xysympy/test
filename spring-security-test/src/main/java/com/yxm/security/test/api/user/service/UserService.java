package com.yxm.security.test.api.user.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/22 19:54
 */
public interface UserService extends UserDetailsService {
}
