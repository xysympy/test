package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author yxm
 * @description: 启动类
 * @date 2022/4/6 10:44
 */
@SpringBootApplication(/*scanBasePackages = {"quartz.net.project.init.starter"} 会造成外部类先加载而当前项目的类还没有注入到容器中*/)
@MapperScan(basePackages = {"com.quartzcustom.dao"})
public class QuartzApplication {
    public static void main(String[] args) {
        // 上面如果没有添加@MapperScan的话，会导致问题，就是扫描不到@Mapper注解所对应的包
        // 主要是因为主启动类是放在com下面的，如果换成com.xx的话，就不会出问题
        // 换句话说，就是除了com.baomidou.xxx这些都是可以的，所以实际上就是因为放在了com下面导致
        // mybatis-plus的某些类提前加载了，而代价就是@Mapper会扫描不到，因为被当成有@MapperScan注解了
        // 具体看总结.md笔记
        // 放在默认包下创建QuartzApplication的话，会导致一些问题：某些类加载不到
        ConfigurableApplicationContext applicationContext = SpringApplication.run(QuartzApplication.class, args);
    }
}
