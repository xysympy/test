package com.quartzcustom.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.quartzcustom.dao.QrtzCustomScheduleJobDao;
import com.quartzcustom.entity.QrtzCustomScheduleJob;
import com.quartzcustom.service.QrtzCustomScheduleJobService;
import org.springframework.stereotype.Service;

/**
 * 自定义定时任务调度(QrtzCustomScheduleJob)表服务接口实现类
 *
 * @author yxm
 * @since 2022-04-09 09:15:33
 */
@Service("qrtzCustomScheduleJobService")
public class QrtzCustomScheduleJobServiceImpl extends ServiceImpl<QrtzCustomScheduleJobDao, QrtzCustomScheduleJob> implements QrtzCustomScheduleJobService {

}

