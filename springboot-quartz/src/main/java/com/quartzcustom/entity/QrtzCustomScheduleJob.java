package com.quartzcustom.entity;

import java.util.Date;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import lombok.Data;

/**
 * 自定义定时任务调度(QrtzCustomScheduleJob)实体类
 *
 * @author yxm
 * @since 2022-04-09 09:15:33
 */
@SuppressWarnings("serial")
@Data
public class QrtzCustomScheduleJob extends Model<QrtzCustomScheduleJob> {

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务分组
     */
    private String jobGroup;

    /**
     * 任务状态， 0禁用 1启用
     */
    private Integer jobStatus;

    /**
     * 任务cron表达式
     */
    private String cronExpression;

    /**
     * 任务描述
     */
    private String remark;

    /**
     * json参数
     */
    private String params;

    /**
     * 是否立即运行，0：否，1：是
     */
    private Integer isNowRun;

    /**
     * 生效日期
     */
    private Date startDate;

    /**
     * 失效日期
     */
    private Date endDate;

    /**
     * 执行类名
     */
    private String jobClassname;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 逻辑删除
     */
    private Integer deleteState;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.jobName;
    }
}

