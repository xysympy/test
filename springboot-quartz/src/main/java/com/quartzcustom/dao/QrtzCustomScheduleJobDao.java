package com.quartzcustom.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quartzcustom.entity.QrtzCustomScheduleJob;
import org.apache.ibatis.annotations.Mapper;


/**
 * 自定义定时任务调度(QrtzCustomScheduleJob)表数据库访问层
 *
 * @author yxm
 * @since 2022-04-09 09:15:32
 */
@Mapper
public interface QrtzCustomScheduleJobDao extends BaseMapper<QrtzCustomScheduleJob> {

}

