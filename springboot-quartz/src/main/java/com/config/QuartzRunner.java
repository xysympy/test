package com.config;

import com.quartzcustom.service.QrtzCustomScheduleJobService;
import org.quartz.Scheduler;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @author yxm
 * @description: quartz启动时加载
 * @date 2022/4/9 9:19
 */
@Component
public class QuartzRunner implements ApplicationRunner {

    private final QrtzCustomScheduleJobService qrtzCustomScheduleJobService;
    private final Scheduler scheduler;

    public QuartzRunner(QrtzCustomScheduleJobService qrtzCustomScheduleJobService, Scheduler scheduler) {
        this.qrtzCustomScheduleJobService = qrtzCustomScheduleJobService;
        this.scheduler = scheduler;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        /*System.out.println("applicationRunner");
        scheduler.start();
        LambdaQueryWrapper<QrtzCustomScheduleJob> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(QrtzCustomScheduleJob::getIsNowRun, 1)
                .eq(QrtzCustomScheduleJob::getDeleteState, 0);
        QrtzCustomScheduleJob one = qrtzCustomScheduleJobService.getOne(queryWrapper);
        QuartzBean quartzBean = new QuartzBean();
        quartzBean.setCronExpression(one.getCronExpression());
        quartzBean.setJobClass(one.getJobClassname());
        quartzBean.setJobName(one.getJobName());
        QuartzUtils.createScheduleJob(scheduler, quartzBean);*/
    }
}
