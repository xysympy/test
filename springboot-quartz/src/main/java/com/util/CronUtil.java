package com.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CronUtil {

    /**
     * 生成指定格式日期字符
     *
     * @param date       日期
     * @param dateFormat : e.g:yyyy-MM-dd HH:mm:ss
     * @return formatTimeStr
     */
    public static String formatDateByPattern(Date date, String dateFormat) {
        dateFormat = dateFormat == null ? "yyyy-MM-dd HH:mm:ss" : dateFormat;
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return date != null ? sdf.format(date) : null;
    }

    /**
     * 生成cron表达式 ss mm HH dd MM ? yyyy
     * convert Date to cron ,eg.  "0 06 10 15 1 ? 2014"
     *
     * @param date : 时间点
     */
    public static String getCron(Date date) {
        String dateFormat = "ss mm HH dd MM ? yyyy";
        return formatDateByPattern(date, dateFormat);
    }

    /**
     * 生成cron表达式 ss mm HH dd MM ?
     * convert Date to cron ,eg.  "0 06 10 15 1 ?"
     *
     * @param date : 时间点
     * @param type : 类型 日/周/月
     */
    public static String getLoopCron(Date date, String type, Integer week, Integer day) {
        String dateFormat = "ss mm HH";
        //  dd MM ?
        String cron = formatDateByPattern(date, dateFormat);
        switch (type) {
            case "Day":
                return cron + " * * ?";
            case "Week":
                return cron + " ? * " + getCurrentWeek(week);
            case "Month":
                return cron + " " + day + " * ?";
            default:
                return "false";
        }
    }


    /**
     * 获取当前星期的字符 MON TUE WED THU FRI SAT SUN
     *
     * @param week : 周 1 2 3 4 5 6 7
     * @return 星期字符
     */
    public static String getCurrentWeek(Integer week) {
        String[] weeks = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};
        return weeks[week - 1];
    }

    public static void main(String[] args) {
        System.out.println(CronUtil.formatDateByPattern(new Date(), null));
    }
}