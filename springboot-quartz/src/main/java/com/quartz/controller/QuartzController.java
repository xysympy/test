package com.quartz.controller;

import com.quartz.domain.AddQuartzDto;
import com.quartz.domain.ViotSceneDto;
import com.quartz.service.QuartzService;
import org.quartz.SchedulerException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yxm
 * @description: 测试quartz定时器任务的增删改查
 * @date 2022/6/28 13:29
 */
@RestController
@RequestMapping("/quartz")
public class QuartzController {

    private final QuartzService quartzService;

    public QuartzController(QuartzService quartzService) {
        this.quartzService = quartzService;
    }

    @PostMapping("/add")
    private boolean addQuartz(@RequestBody AddQuartzDto dto) throws SchedulerException {
        return quartzService.addQuartz(dto);
    }

    @PostMapping("/test")
    private void test(@RequestBody ViotSceneDto dto) {
        System.out.println(dto.getSceneType());
        System.out.println(dto.getConditionList());
    }
}
