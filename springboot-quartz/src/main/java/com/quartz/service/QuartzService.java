package com.quartz.service;

import com.quartz.domain.AddQuartzDto;
import com.time.TimeBean;
import org.quartz.*;
import org.springframework.stereotype.Service;

import java.util.TimeZone;
import java.util.UUID;

/**
 * @author yxm
 * @description: service层
 * @date 2022/6/28 13:30
 */
@Service
public class QuartzService {

    private static final String GROUP_NAME = "test";
    private final Scheduler scheduler;

    public QuartzService(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public boolean addQuartz(AddQuartzDto dto) {
        String jobName = UUID.randomUUID().toString();
        try {
            saveOrUpdateJob(jobName, dto.getCronExpression());
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 保存或更新定时器任务
     *
     * @param jobName        作业名
     * @param cronExpression cron表达式
     */
    private void saveOrUpdateJob(String jobName, String cronExpression) throws SchedulerException {
        TriggerKey triggerKey = TriggerKey.triggerKey(jobName, GROUP_NAME);
        if (scheduler.checkExists(triggerKey)) {
            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(jobName, GROUP_NAME)
                    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                    .build();
            scheduler.rescheduleJob(triggerKey, trigger);
        } else {
            JobDetail jobDetail = JobBuilder.newJob(TimeBean.class)
                    .withIdentity(jobName, GROUP_NAME)
                    .build();
            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(jobName, GROUP_NAME)
                    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression).inTimeZone(TimeZone.getTimeZone("Asia/Sh")))
                    .build();
            scheduler.scheduleJob(jobDetail, trigger);
        }
    }
}
