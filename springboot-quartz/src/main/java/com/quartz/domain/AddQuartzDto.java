package com.quartz.domain;

import lombok.Data;

/**
 * @author yxm
 * @description: 添加定时任务测试
 * @date 2022/6/28 13:32
 */
@Data
public class AddQuartzDto /*implements AesDto*/ {

    /**
     * cron表达式
     */
    /*@NotNull
    @NotEmpty*/
    private String cronExpression;

}
