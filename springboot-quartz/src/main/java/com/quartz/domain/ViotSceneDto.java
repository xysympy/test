package com.quartz.domain;

import lombok.Data;

import java.util.List;

@Data
public class ViotSceneDto /*implements H5AesDto*/ {

    private String sceneId;

    private String defaultSceneId;

    private String sceneName;

    private Integer sceneType;

    private Integer conditionLogic;

    private String creatorId;

    private String groupId;

    private Integer isEnable;

    private Integer isDefault;

    private String description;

    private String iconId;

    private String colorId;

    private Integer orderNo;

    private String createTime;

    private String updateTime;

    private List<ViotSceneActionDto> actionList;

    private List<ViotSceneConditionDto> conditionList;

    private Integer page;

    private Integer limit;

}