package com.quartz.domain;

import lombok.Data;

/**
 * 场景执行动作参数封装类
 */
@Data
public class ViotSceneActionDto /*implements H5AesDto*/ {

    private String actionId;

    private Integer actionType;

    private String actionName;

    private String sceneId;

    public String actionRefId;

    private String description;

    private String createTime;

    private String updateTime;

    private Integer page;

    private Integer limit;

}
