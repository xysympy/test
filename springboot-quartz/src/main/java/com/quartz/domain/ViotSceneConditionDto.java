package com.quartz.domain;

import lombok.Data;

/**
 * 场景触发条件参数封装类
 */
@Data
public class ViotSceneConditionDto /*implements H5AesDto*/ {

    private String conditionId;

    private Integer conditionType;

    private String conditionName;

    private String sceneId;

    private String conditionRefId;

    private String description;

    private String createTime;

    private String updateTime;

    private Integer page;

    private Integer limit;

    private String originalId;

}
