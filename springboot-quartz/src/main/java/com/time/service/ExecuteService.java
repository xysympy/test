package com.time.service;

import com.config.RequestDataHelper;
import org.springframework.stereotype.Component;

/**
 * @author yxm
 * @description: 定时服务接口
 * @date 2022/4/7 16:58
 */
@Component
public class ExecuteService {

    public ExecuteService() {
        System.out.println("ExecuteService");
    }

    public void run() {
        Object data = RequestDataHelper.getRequestData("data");
        System.out.println(data);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Object data = RequestDataHelper.getRequestData("data");
                System.out.println(data);
            }
        });
        thread.start();
    }
}
