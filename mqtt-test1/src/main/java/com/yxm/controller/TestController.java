package com.yxm.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @Author Administrator
 * @Date 2021/9/26 11:24
 */
@RestController
public class TestController {
    @GetMapping("/test")
    public String test(){
        return "test";
    }

    @GetMapping("/test1")
    public void test1(HttpServletResponse response) throws IOException {
        response.sendRedirect("/test");
    }
}
