package com.diy.security.demo.handler;

import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

@Component
public class SmsAuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    public SmsAuthFailureHandler() {
        super("/failure");
    }
}
