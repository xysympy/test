package com.diy.security.demo.handler;

import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class SmsAuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    public SmsAuthSuccessHandler() {
        super("/index");
    }
}
