package com.diy.security.demo.manager;

import com.diy.security.demo.entity.SmsAuthenticationToken;
import com.diy.security.demo.util.CacheUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.thymeleaf.util.StringUtils;

@Slf4j
@Component
public class SmsAuthenticationProvider implements AuthenticationProvider {

    private final UserDetailsService userDetailsService;

    public SmsAuthenticationProvider(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        Assert.isInstanceOf(SmsAuthenticationToken.class, authentication,
                () ->  "SmsAuthenticationProvider.onlySupports Only SmsAuthenticationToken is supported");
        SmsAuthenticationToken authenticationToken = (SmsAuthenticationToken) authentication;
        String phone = (String) authenticationToken.getPrincipal();
        String verifyCode = (String) authenticationToken.getCredentials();
        UserDetails userDetails = userDetailsService.loadUserByUsername(phone);
        if (userDetails == null){
            throw new InternalAuthenticationServiceException("cannot get user info");
        }
        //验证码是否正确
        if (!StringUtils.equals(CacheUtil.getValue(phone),verifyCode)){
            throw new AuthenticationCredentialsNotFoundException("验证码错误");
        }
        return new SmsAuthenticationToken(userDetails.getAuthorities(),userDetails,verifyCode);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.isAssignableFrom(SmsAuthenticationToken.class);
    }
}
