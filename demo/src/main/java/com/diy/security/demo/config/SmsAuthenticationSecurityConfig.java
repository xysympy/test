package com.diy.security.demo.config;

import com.diy.security.demo.filter.SmsAuthenticationFilter;
import com.diy.security.demo.handler.SmsAuthFailureHandler;
import com.diy.security.demo.handler.SmsAuthSuccessHandler;
import com.diy.security.demo.manager.SmsAuthenticationProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SmsAuthenticationSecurityConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    private final SmsAuthSuccessHandler smsAuthSuccessHandler;
    private final SmsAuthFailureHandler smsAuthFailureHandler;
    private final SmsAuthenticationProvider smsAuthenticationProvider;
    private final  SmsAuthenticationSecurityConfig smsAuthenticationSecurityConfig;

    public SmsAuthenticationSecurityConfig(SmsAuthSuccessHandler smsAuthSuccessHandler, SmsAuthFailureHandler smsAuthFailureHandler
            , SmsAuthenticationProvider smsAuthenticationProvider, SmsAuthenticationSecurityConfig smsAuthenticationSecurityConfig) {
        this.smsAuthSuccessHandler = smsAuthSuccessHandler;
        this.smsAuthFailureHandler = smsAuthFailureHandler;
        this.smsAuthenticationProvider = smsAuthenticationProvider;
        this.smsAuthenticationSecurityConfig = smsAuthenticationSecurityConfig;
    }

    @Override
    public void configure(HttpSecurity builder) throws Exception {
        SmsAuthenticationFilter smsAuthenticationFilter = new SmsAuthenticationFilter();
        smsAuthenticationFilter.setAuthenticationManager(builder.getSharedObject(AuthenticationManager.class));
        smsAuthenticationFilter.setAuthenticationSuccessHandler(smsAuthSuccessHandler);
        smsAuthenticationFilter.setAuthenticationFailureHandler(smsAuthFailureHandler);

        builder.authenticationProvider(smsAuthenticationProvider);
        builder.addFilterAfter(smsAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

        builder.formLogin()
                .loginPage("/login")
                .and()
                .apply(smsAuthenticationSecurityConfig)
                .and()
                // 设置URL的授权
                .authorizeRequests()
                // 这里需要将登录页面放行
                .antMatchers("/login","/verifyCode","/smsLogin","/failure")
                .permitAll()
                // anyRequest() 所有请求   authenticated() 必须被认证
                .anyRequest()
                .authenticated()
                .and()
                // 关闭csrf
                .csrf().disable();
    }
}
