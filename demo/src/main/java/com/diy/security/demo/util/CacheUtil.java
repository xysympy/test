package com.diy.security.demo.util;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class CacheUtil {
    private static final LoadingCache<String, String> CACHE = CacheBuilder.newBuilder()
            //基于容量回收：总数量100个
            .maximumSize(100)
            //定时回收：没有写访问1分钟后失效清理
            .expireAfterWrite(1, TimeUnit.MINUTES)
            //当在缓存中未找到所需的缓存项时，会执行CacheLoader的load方法加载缓存
            .build(new CacheLoader<String, String>() {
                @Override
                public String load(String key) throws Exception {
                    log.debug("没有找到缓存: {}",key);
                    return "";
                }
            });
    public static void putValue(String key, String value){
        CACHE.put(key,value);
    }

    public static String getValue(String key){
        try {
            return CACHE.get(key);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return "";
    }
}
