package com.yxm.springsecurity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yxm.springsecurity.domain.dto.SdcAppInstListDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
class SpringsecurityApplicationTests {

    @Test
    void contextLoads() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode("123456");
        boolean matches = passwordEncoder.matches("123456", encode);
        System.out.println(matches + encode);
    }

    @Test
    void test() throws JsonProcessingException {
        //String data = "{\"limit\":\"10\",\"createTimeRange.startTime\":\"2021-10-28 18:37:55\",\"page\":\"1\",\"createTimeRange.endTime\":\"2021-10-28 18:37:55\"}";
        String data = "{\"limit\":\"10\", \"page\":\"1\", \"createTimeRange\":{\"startTime\":\"2021-10-28 18:37:55\", \"endTime\":\"2021-10-28 18:37:55\"}}";
        ObjectMapper objectMapper = new ObjectMapper();
        SdcAppInstListDto sdcAppInstListDto = objectMapper.readValue(data, new TypeReference<SdcAppInstListDto>() {
        });
        System.out.println(sdcAppInstListDto);
    }

}
