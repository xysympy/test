package com.yxm.springsecurity.domain.dto;

import lombok.Data;

@Data
public class SearchTimeDto {
    /**
     * 搜索开始时间
     */
    private String startTime;
    /**
     * 搜索的结束时间
     */
    private String endTime;
}
