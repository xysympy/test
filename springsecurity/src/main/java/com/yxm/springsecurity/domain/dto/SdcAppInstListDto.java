package com.yxm.springsecurity.domain.dto;

import lombok.Data;

@Data
public class SdcAppInstListDto{
    /**
     * 分页
     * 默认为1
     */
    private Integer page;
    /**
     * 分页限制
     * 默认为10
     */
    private Integer limit;
    /**
     * 应用实例名称
     */
    private String appInstName;

    /**
     * 所属应用ID
     */
    private String appId;

    /**
     * 应用版本号
     */
    private String appVersion;

    /**
     * 应用实例状态
     */
    private Integer appInstState;

    /**
     * 创建时间的起止时间
     */
    private SearchTimeDto createTimeRange;
}