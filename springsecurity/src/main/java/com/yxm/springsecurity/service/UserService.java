package com.yxm.springsecurity.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/24 19:04
 */
public interface UserService extends UserDetailsService {
}
