package com.yxm.springsecurity.service.impl;

import com.yxm.springsecurity.service.UserService;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/11/24 19:05
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public UserDetails loadUserByUsername(String username){
        if (!"admin".equals(username)) {
            throw new UsernameNotFoundException("用户名不存在");
        }
        System.out.println(username);
        // 因为添加了passwordEncoder，所以会采用它的matchers方法，所以需要添加一个加密后的数据
        // 原始数据是123456
        String password = "$2a$10$NIViANqA3VeVk4.G.LWPD.2kAF6uqgBnv2Adg4AZuqdKhXAugrtqu";
        // 第三个参数表示权限
        return new User(username, password, AuthorityUtils.createAuthorityList("admin"));
    }
}
