package com.yxm.juc;

import java.util.concurrent.locks.LockSupport;

/**
 * @ClassName: ParkWaitTest
 * @Description: 测试下使用wait方法和park方法
 * @Author: yxm
 * @Date: 2022/4/10 18:39
 * @Version: 1.0
 **/
public class ParkWaitTest {

    private final Object lock = new Object();

    public static void main(String[] args) {
        //wait方法是只能被synchorized的对象才能调用的，，不然是会报错的
        // 还有一点是，如果是先使用notifyAll是不会报错的
        /*ParkWaitTest parkWaitTest = new ParkWaitTest();
        parkWaitTest.waitTest(parkWaitTest);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (parkWaitTest) {
            // 唤醒线程
            System.out.println("notifyAll thread");
            parkWaitTest.notifyAll();
        }*/
        // 使用下park
        ParkWaitTest parkWaitTest = new ParkWaitTest();
        parkWaitTest.parkTest();
        // 在使用synchorized(lock)的前提下，分别测试了sleep和park
        // 两个都没有去释放锁的资源
        synchronized (parkWaitTest.lock) {
            System.out.println("拿到了lock的锁");
        }
    }

    public void waitTest(ParkWaitTest parkWaitTest) {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                // 实际上这个是匿名的内部类的this了，所以等待的和通知的就错位了
                synchronized (parkWaitTest) {
                    // 线程进入等待时间
                    System.out.println("wait before");
                    try {
                        // wait方法需要在synchorized中进行
                        parkWaitTest.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("wait after");
                }
            }

        });
        thread1.start();
    }

    public void parkTest() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("lockSupport park before");
                // 表示这个线程要用lock需要获取它对应的通行证，不然就等在这儿
                // 这个应该是算获取锁的前提了
                // LockSupport.park(lock);
                synchronized (lock) {
                    /*try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                    LockSupport.park();
                }
                System.out.println("lockSupport park after");
            }
        });
        thread.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("lockSupport unPark");
        LockSupport.unpark(thread);
    }
}
