package com.yxm.juc;

/**
 * @ClassName: test
 * @Description: 测试使用interrupt方法
 * @Author: yxm
 * @Date: 2022/4/10 18:26
 * @Version: 1.0
 **/
public class InterruptTest {
    public static void main(String[] args) {
        // 测试下interrupt的使用
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("before sleep");
                    Thread.sleep(1);
                    System.out.println("after sleep");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        // 基本上就是在开启线程的方法里面添加了了能够抛出InterruptedException的方法
        // 直接运行下面的interrupt方法都是会中断程序的
        // 如果在线程里面运行的代码是没有这类抛出InterruptedException的方法，那么还是正常的
        // interrupt方法就会变成添加一个中断的标记，然后你可以使用isInterruped方法来手动的结束线程
        thread.interrupt();
    }
}
