package com.yxm.spring.bean;

import java.lang.annotation.Annotation;

/**
 * @author yxm
 * @description: 存储bean的信息
 *      假设，getBean的时候，如果是单例的话，那么我们可能直接从单例池里面拿出来
 *      但是如果是多例的话，那就表示我们需要直接新建一个对象出来，难道重新循环要扫描的包
 *      路径进行匹配吗？所以可以存储一下信息，这样会快一点
 * @date 2021/12/6 13:56
 */
public class BeanInfo {

    /**
     * bean的名字
     */
    private String beanName;
    /**
     * 类型
     */
    private Class type;

    public Annotation getAnnotation() {
        return annotation;
    }

    /**
     * 注解
     */
    private Annotation annotation;

    public BeanInfo(String beanName, Class type, Annotation annotation) {
        this.beanName = beanName;
        this.type = type;
        this.annotation = annotation;
    }
    public String getBeanName() {
        return beanName;
    }

    public Class getType() {
        return type;
    }
}
