package com.yxm.spring.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author yxm
 * @description: 模拟注解ComponentScan
 * @date 2021/12/6 13:37
 */
@Target(value = {ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface ComponentScan {
    String[] value() default "";
}
