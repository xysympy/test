package com.yxm.spring.exception;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/12/6 13:47
 */
public class AchieveException extends RuntimeException {
    private Integer code;

    public AchieveException(Integer code) {
        this.code = code;
    }

    public AchieveException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return super.getMessage() != null && "".equals(super.getMessage()) ? super.getMessage() : this.getMessage();
    }
}
