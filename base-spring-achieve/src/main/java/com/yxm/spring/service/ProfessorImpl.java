package com.yxm.spring.service;

import com.yxm.spring.annotation.Component;
import com.yxm.spring.service.Professor;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/12/8 10:25
 */
@Component
public class ProfessorImpl implements Professor {

    @Override
    public void postInit() {
        System.out.println("初始化后");
    }
}
