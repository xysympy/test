package com.yxm.spring.service;

/**
 * @author yxm
 * @description: 实现初始化后的操作
 * @date 2021/12/8 10:23
 */
public interface Professor {
    /**
     * 初始化后
     */
    void postInit();
}
