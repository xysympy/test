package com.yxm.spring.service;

import com.yxm.spring.annotation.Component;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/12/6 13:40
 */
@Component
public class OrderService implements Service{
    @Override
    public void test() {
        System.out.println("orderService");
    }

    @Override
    public void test1() {
        System.out.println("test1");
    }
}
