package com.yxm.spring.service;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/12/8 10:23
 */
public interface Service {
    /**
     * 测试动态代理
     */
    void test();

    /**
     * test1
     */
    void test1();
}
