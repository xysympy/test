package com.yxm.spring.config;

import com.yxm.spring.annotation.ComponentScan;
import com.yxm.spring.annotation.Configuration;

/**
 * @author yxm
 * @description: 配置类
 * @date 2021/12/6 13:34
 */
@Configuration
@ComponentScan("com.yxm.spring.service")
public class AppConfig {
}
