package com.yxm.spring;

import com.yxm.spring.annotation.Component;
import com.yxm.spring.annotation.ComponentScan;
import com.yxm.spring.annotation.Configuration;
import com.yxm.spring.bean.BeanInfo;
import com.yxm.spring.exception.AchieveException;
import com.yxm.spring.service.Professor;

import java.beans.Introspector;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yxm
 * @description: 实现下基本的spring思路
 * 扫描包路径----生成对象----注入属性----初始化前----初始化----初始化后
 * @date 2021/12/6 13:30
 */
public class YxmApplication {
    /**
     * 配置类
     */
    private Class configuration;
    /**
     * 存储bean信息
     */
    private final static Map<String, BeanInfo> beanInfoMap = new ConcurrentHashMap<>();
    /**
     * 存储已经创建好的bean对象
     */
    private final static Map<String, Object> beans = new ConcurrentHashMap<>();

    /**
     * 存储bean对象初始化后需要执行的对象方法
     */
    private final static Map<String, Professor> professors = new ConcurrentHashMap<>();

    public YxmApplication(Class configuration) {
        this.configuration = configuration;
        init();
    }

    private void init() {
        // 先是扫描指定的包
        try {
            scan(this.configuration);
        } catch (ClassNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // 扫描所有的bean信息，把初始化前和初始化后的类先添加
        for (Map.Entry<String, BeanInfo> infoEntry : beanInfoMap.entrySet()) {
            BeanInfo beanInfo = infoEntry.getValue();
            if (Professor.class.isAssignableFrom(beanInfo.getType()) && !beanInfo.getType().isInterface()) {
                professors.put(infoEntry.getKey(), (Professor) createBean(beanInfo));
            }
        }
        for (Map.Entry<String, BeanInfo> infoEntry : beanInfoMap.entrySet()) {
            // 创建bean对象
            Object bean;
            BeanInfo beanInfo = infoEntry.getValue();
            if (!Professor.class.isAssignableFrom(beanInfo.getType()) && !beanInfo.getType().isInterface()) {
                if ((bean = createBean(beanInfo)) != null) {
                    beans.put(infoEntry.getKey(), bean);
                }
            }
        }
    }

    /**
     * 扫描
     *
     * @param configuration 配置
     */
    private void scan(Class configuration) throws ClassNotFoundException, UnsupportedEncodingException {
        if (!configuration.isAnnotationPresent(Configuration.class)) {
            throw new AchieveException(-1001, "没有包含该注解");
        }
        String[] packageNames = new String[]{SpringApplication.class.getPackage().getName().replaceAll(".", "/")};
        if (configuration.isAnnotationPresent(ComponentScan.class)) {
            ComponentScan annotation = (ComponentScan) configuration.getAnnotation(ComponentScan.class);
            if (!"".equals(annotation.value())) {
                packageNames = annotation.value();
            }
        }
        for (String packageName : packageNames) {
            // 存储原始的数据带.的包名
            String temp = packageName;
            System.out.println(packageName);
            packageName = packageName.replace(".", "\\");
            System.out.println(packageName);
            URL resource = this.getClass().getClassLoader().getResource(packageName);
            String path = URLDecoder.decode(resource.getPath(), "utf-8").substring(1).replace("/", "\\");
            for (File file : new File(path).listFiles()) {
                // 暂时不做文件夹的处理
                String beanName = file.getName().substring(0, file.getName().indexOf(".class"));
                Class<?> aClass = Class.forName(temp + "." + beanName);
                Annotation annotation = null;
                if (aClass.isAnnotationPresent(Component.class)) {
                    annotation = aClass.getAnnotation(Component.class);
                    String value = ((Component) annotation).value();
                    if (value == null || "".equals(value.trim())) {
                        beanName = Introspector.decapitalize(beanName);
                    } else {
                        beanName = value;
                    }
                }
                BeanInfo beanInfo = new BeanInfo(beanName, aClass, annotation);
                beanInfoMap.put(beanName, beanInfo);
            }
        }
    }

    /**
     * 创建bean
     */
    private Object createBean(BeanInfo beanInfo) {
        try {
            if (beanInfo.getAnnotation() instanceof Component) {
                if (!"scope".equals(((Component) beanInfo.getAnnotation()).scope())) {
                    // 还是可以调用有参的构造方法创建的
                    // 暂时只是调用无参构造
                    Object instance = beanInfo.getType().getConstructor().newInstance();
                    if (!Professor.class.isAssignableFrom(beanInfo.getType())) {
                        Object proxyInstance = Proxy.newProxyInstance(beanInfo.getType().getClassLoader(), beanInfo.getType().getInterfaces()
                                , (proxy, method, args) -> {
                                    Object invoke = method.invoke(instance, args);
                                    for (Map.Entry<String, Professor> entry : professors.entrySet()) {
                                        entry.getValue().postInit();
                                    }
                                    return invoke;
                                });
                        return proxyInstance;
                    }
                    return instance;
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取bean 对象
     *
     * @param beanName bean的名字
     * @return {@link Object}
     */
    public Object getBean(String beanName) {
        BeanInfo beanInfo = beanInfoMap.get(beanName);
        if (beanInfo.getAnnotation() != null) {
            if (beanInfo.getAnnotation() instanceof Component) {
                if ("scope".equals(((Component) beanInfo.getAnnotation()).scope())) {
                    return createBean(beanInfo);
                }
                return beans.get(beanName);
            }
        }
        return null;
    }
}
