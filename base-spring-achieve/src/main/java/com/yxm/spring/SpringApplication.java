package com.yxm.spring;

import com.yxm.spring.config.AppConfig;

/**
 * @author yxm
 * @description: TODO(这里用一句话描述这个类的作用)
 * @date 2021/12/6 13:41
 */
public class SpringApplication {
    public static void main(String[] args) {
        YxmApplication application = new YxmApplication(AppConfig.class);
        System.out.println(application.getBean("orderService"));
    }
}
