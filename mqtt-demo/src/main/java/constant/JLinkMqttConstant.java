package constant;

/**
 * @author yxm
 * @description: 常量类
 * @date 2022/2/11 9:04
 */
public class JLinkMqttConstant {
    /**
     * url
     */
    public static final String BROKER = "ssl://mqtt.bcloud365.net:8000";
    /**
     * clientId
     */
    public static final String CLIENT_ID = "e0534f3240274897821a126be19b6d46";
    /**
     * 用户名
     */
    public static final String USERNAME = "username";
    /**
     * 睡眠时间：发布消息后的等待时间
     */
    public static final Integer SLEEP_TIME = 5;
    /**
     * 0：最多一次 、1：最少一次 、2：只有一次
     */
    public static final Integer QOS = 2;


    /**
     * 获取token地址
     */
    public static final String URL = "https://pub-token.xmeye.net/reqToken";
    /**
     * 设备序列号
     */
    public static final String SN = "974c5177539f31fe";
    /**
     * 服务类型
     */
    public static final String SERVICE = "MQTT";
    /**
     * 用户名：通过注册开放平台获取
     */
    public static final String USER = "admin";
    /**
     * 密码：通过注册开放平台获取
     */
    public static final String PASS = "ym131421";
    /**
     * uuid：通过注册开放平台获取
     */
    public static final String UUID = "e0534f3240274897821a126be19b6d46";
    /**
     * App Key：通过注册开放平台获取
     */
    public static final String APP_KEY = "0621ef206a1d4cafbe0c5545c3882ea8";
    /**
     * App Secret：通过注册开放平台获取
     */
    public static final String SEC_KEY = "90f8bc17be2a425db6068c749dee4f5d";
    /**
     * moveCard：通过注册开放平台获取
     */
    public static final Integer MOVE_CARD = 2;
}
