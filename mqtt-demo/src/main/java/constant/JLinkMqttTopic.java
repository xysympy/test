package constant;

/**
 * @author yxm
 * @description: Topic枚举类
 * @date 2022/2/11 9:28
 */
public enum JLinkMqttTopic {
    DEVICE_STATUS("v1/status/query/", "get device status")
    , DEVICE_WAKEUP("v1/device/wakeup/", "low-power device wakeup")
    , DEVICE_BROADCAST_ADDRESS("v1/device/livestream/", "get device live broadcast address")
    , DEVICE_INFO("v1/device/getinfo/", "get device information")
    , DEVICE_LOGIN("v1/device/login/", "device login")
    , DEVICE_ABILITY("v1/device/getability/", "get device capability set")
    , DEVICE_KEEP_ALIVE("v1/device/keepalive/", "device keep alive")
    , DEVICE_GET_CONFIG("v1/device/getconfig/", "device get config")
    , DEVICE_SET_CONFIG("v1/device/setconfig/", "device set config")
    , DEVICE_OPERATION("v1/device/opdev/", "device operation")
    , DEVICE_SNAPSHOOT("v1/device/capture/", "device snapshoot")
    , DEVICE_USER("v1/device/usermanage/", "device user related");

    private final String topic;
    private final String desc;

    JLinkMqttTopic(String topic, String desc) {
        this.topic = topic;
        this.desc = desc;
    }

    public String getTopic() {
        return topic;
    }

    public String getDesc() {
        return desc;
    }
}
