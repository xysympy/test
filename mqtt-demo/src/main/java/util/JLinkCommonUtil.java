package util;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import constant.JLinkMqttConstant;

import java.nio.charset.StandardCharsets;

/**
 * @author yxm
 * @description: password等获取类
 * @date 2021/12/28 15:31
 */
public class JLinkCommonUtil {

    public static String getPassword() {
        String url = JLinkMqttConstant.URL;
        String sn = JLinkMqttConstant.SN;
        String service = JLinkMqttConstant.SERVICE;
        String user = JLinkMqttConstant.USER;
        String pass = JLinkMqttConstant.PASS;
        String uuid = JLinkMqttConstant.UUID;
        String appKey = JLinkMqttConstant.APP_KEY;
        String secKey = JLinkMqttConstant.SEC_KEY;
        Integer mc = JLinkMqttConstant.MOVE_CARD;
        String tm = getTimMillis();
        String sign = getEncryptStr(uuid, appKey, secKey, tm, mc);
        JSONObject jsonObject = new JSONObject();
        jsonObject.putOnce("sn", sn);
        jsonObject.putOnce("service", service);
        jsonObject.putOnce("uuid", uuid);
        jsonObject.putOnce("appkey", appKey);
        jsonObject.putOnce("tm", tm);
        jsonObject.putOnce("sign", sign);
        jsonObject.putOnce("user", user);
        jsonObject.putOnce("pass", pass);
        String password = HttpRequest.post(url).header(Header.CONTENT_TYPE, "application/json")
                .body(jsonObject.toString()).execute().body();
        System.out.println("接口调用结果: " + password);
        return JSONUtil.parseObj(password).getStr("token");
    }

    private static String getEncryptStr(String uuid, String appKey, String appSecret, String timeMillis, int movedCard) {
        String encryptStr = uuid + appKey + appSecret + timeMillis;
        byte[] encryptByte = encryptStr.getBytes(StandardCharsets.ISO_8859_1);
        byte[] changeByte = change(encryptStr, movedCard);
        byte[] mergeByte = mergeByte(encryptByte, changeByte);
        return SecureUtil.md5().digestHex(mergeByte);
    }

    /**
     * 简单移位
     */
    private static byte[] change(String encryptStr, int moveCard) {
        byte[] encryptByte = encryptStr.getBytes(StandardCharsets.ISO_8859_1);
        int encryptLength = encryptByte.length;
        byte temp;
        for (int i = 0; i < encryptLength; i++) {
            temp = ((i % moveCard) > ((encryptLength - i) % moveCard)) ? encryptByte[i] : encryptByte[encryptLength - (i + 1)];
            encryptByte[i] = encryptByte[encryptLength - (i + 1)];
            encryptByte[encryptLength - (i + 1)] = temp;
        }
        return encryptByte;
    }

    /**
     * 合并
     *
     * @param encryptByte
     * @param changeByte
     * @return
     */
    private static byte[] mergeByte(byte[] encryptByte, byte[] changeByte) {
        int encryptLength = encryptByte.length;
        int encryptLength2 = encryptLength * 2;
        byte[] temp = new byte[encryptLength2];
        for (int i = 0; i < encryptByte.length; i++) {
            temp[i] = encryptByte[i];
            temp[encryptLength2 - 1 - i] = changeByte[i];
        }
        return temp;
    }

    private static long timMillis;//时间戳
    private static long counter = 0L;//计数器

    /**
     * 获取计数器
     *
     * @return
     */
    private static synchronized String getCounter() {
        ++counter;
        if (counter < 10L) {
            return "000000" + String.valueOf(counter);
        } else if (counter < 100L) {
            return "00000" + String.valueOf(counter);
        } else if (counter < 1000L) {
            return "0000" + String.valueOf(counter);
        } else if (counter < 10000L) {
            return "000" + String.valueOf(counter);
        } else if (counter < 100000L) {
            return "00" + String.valueOf(counter);
        } else if (counter < 1000000L) {
            return "0" + String.valueOf(counter);
        } else if (counter < 10000000L) {
            return String.valueOf(counter);
        } else {
            counter = 1L;
            return "000000" + String.valueOf(counter);
        }
    }

    /**
     * 获取组合时间戳
     *
     * @return
     */
    private static String getTimMillis() {
        timMillis = System.currentTimeMillis();
        return getCounter() + String.valueOf(timMillis);
    }
}
