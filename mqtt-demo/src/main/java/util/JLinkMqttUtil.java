package util;

import constant.JLinkMqttConstant;
import constant.JLinkMqttTopic;
import mqtt.JLinkOnMessageCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.concurrent.TimeUnit;

/**
 * @author yxm
 * @description: mqtt连接
 * @date 2021/12/28 15:30
 */
public class JLinkMqttUtil {

    private static String password;
    private static MqttClient client;


    private static MqttClient getClient(String password) throws MqttException {
        String broker = JLinkMqttConstant.BROKER;
        String clientId = JLinkMqttConstant.CLIENT_ID;
        MemoryPersistence persistence = new MemoryPersistence();

        MqttClient client = new MqttClient(broker, clientId, persistence);
        // MQTT 连接选项
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setUserName(JLinkMqttConstant.USERNAME);
        connOpts.setPassword(password.toCharArray());
        // 保留会话
        connOpts.setCleanSession(true);
        // 设置回调
        client.setCallback(new JLinkOnMessageCallback());
        // 建立连接
        client.connect(connOpts);
        return client;
    }

    public static void mqtt(String pubTopic, String subTopic, String content, String password) throws InterruptedException {
        int qos = JLinkMqttConstant.QOS;
        try {
            if (client == null) {
                client = getClient(password);
            }
            // 订阅
            client.subscribe(subTopic, qos);
            // 消息发布所需参数
            MqttMessage message = new MqttMessage();
            message.setPayload(content.getBytes());
            message.setQos(qos);
            client.publish(pubTopic, message);
            /*TimeUnit.SECONDS.sleep(JLinkMqttConstant.SLEEP_TIME);
            client.disconnect();
            client.close();*/
        } catch (MqttException me) {
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
        }
    }

    public static void setPassword() {
        password = JLinkCommonUtil.getPassword();
    }

    public static void queryStatus(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_STATUS.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void wakeup(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_WAKEUP.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void liveStream(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_BROADCAST_ADDRESS.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void getInfo(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_INFO.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void login(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_LOGIN.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void getAbility(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_ABILITY.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void keepAlive(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_KEEP_ALIVE.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void getConfig(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_GET_CONFIG.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void setConfig(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_SET_CONFIG.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void opDev(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_OPERATION.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void getCapture(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_SNAPSHOOT.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }

    public static void userManage(String content) throws InterruptedException {
        String pubTopic = JLinkMqttTopic.DEVICE_USER.getTopic() + password;
        String subTopic = pubTopic + "/result";
        JLinkMqttUtil.mqtt(pubTopic, subTopic, content, password);
    }
}
