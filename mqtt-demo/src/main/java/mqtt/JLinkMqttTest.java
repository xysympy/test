package mqtt;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import util.JLinkMqttUtil;

/**
 * @author yxm
 * @description: 测试运行
 * @date 2021/12/28 16:31
 */
public class JLinkMqttTest {

    public static void main(String[] args) throws InterruptedException {
        JSONObject content = new JSONObject();
        // 设置password
        JLinkMqttUtil.setPassword();
        // 获取设备状态
        JLinkMqttUtil.queryStatus(content.toString());
        // 低功耗设备唤醒
        JLinkMqttUtil.wakeup(content.toString());
        // 获取设备直播地址
        content.putOnce("mediaType", "hls");
        content.putOnce("channel", "0");
        content.putOnce("stream", "0");
        content.putOnce("protocol", "ts");
        content.putOnce("username", "admin");
        content.putOnce("devPwd", "ym131421");
        JLinkMqttUtil.liveStream(content.toString());
        //登录设备
        content.clear();
        content.putOnce("EncryptType", "MD5");
        content.putOnce("LoginType", "DVRIP-Web");
        content.putOnce("PassWord", "ym131421");
        content.putOnce("UserName", "admin");
        content.putOnce("Name", "generalinfo");
        JLinkMqttUtil.login(content.toString());
        // 获取设备信息
        content.clear();
        content.putOnce("Name", "SystemInfo");
        JLinkMqttUtil.getInfo(content.toString());
        // 获取设备能力
        content.replace("Name", "SystemFunction");
        JLinkMqttUtil.getAbility(content.toString());
        // 设备保活
        content.replace("Name", "KeepAlive");
        JLinkMqttUtil.keepAlive(content.toString());
        // 获取通道名称
        content.replace("Name", "ChannelTitle");
        JLinkMqttUtil.getConfig(content.toString());
        // 外部报警设置
        content.replace("Name", "Alarm.LocalAlarm");
        JLinkMqttUtil.getConfig(content.toString());
        // 网络基础设置
        content.replace("Name", "NetWork.NetCommon");
        JLinkMqttUtil.getConfig(content.toString());
        // 本地化设置
        content.replace("Name", "General.Location");
        JLinkMqttUtil.getConfig(content.toString());
        // 设置设备配置
        content.clear();
        content.putOnce("Name", "ChannelTitle");
        JSONArray channelTitle = new JSONArray();
        channelTitle.put("cha");
        content.putOnce("ChannelTitle", channelTitle);
        JLinkMqttUtil.setConfig(content.toString());
        // 设备保活
        content.clear();
        content.putOnce("Name", "KeepAlive");
        JLinkMqttUtil.keepAlive(content.toString());
        //设备操作
        content.clear();
        content.putOnce("Name", "OPVersionList");
        JLinkMqttUtil.opDev(content.toString());
        // 设备抓图
        content.clear();
        content.putOnce("Name", "OPSNAP");
        JSONObject opSnap = new JSONObject();
        opSnap.putOnce("Channel", 0);
        opSnap.putOnce("PicType", 0);
        content.putOnce("OPSNAP", opSnap);
        JLinkMqttUtil.getCapture(content.toString());
        // 设备用户相关
        content.clear();
        content.putOnce("Name", "USERS");
        JLinkMqttUtil.userManage(content.toString());
    }
}
